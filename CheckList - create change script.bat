@ECHO OFF
set Title=CheckList
set DeployDbFromModelBatPath=.\CheckList - deploy db from model.bat
set NHibernatePath=.\CheckList.Interfaces\bin\Debug\netcoreapp2.2\CheckList.NHibernate.config
set ChangeScriptsPath=.\CheckList.DataAccess\ChangeScripts

@ECHO OFF

echo off
call "%~dp0%DeployDbFromModelBatPath%"

echo.
echo.

echo *******************
echo %Title% - create change script 
echo *******************

set /p file=Enter jira task in template COMPONENT-NUMBER: "FIN-1541":

"%~dp0tools\OpenDbDiff\OCDB.exe" ^
-cs="%~dp0%NHibernatePath%"  -ct="%~dp0%NHibernatePath%" -jt=%file% -f="%~dp0%ChangeScriptsPath%" -p=05.10.* -rs="#DBName#-Model"

pause
