﻿@ECHO OFF
set Title=CheckList
set InterfaceProject=\CheckList.Interfaces\CheckList.Interfaces.csproj
set SchemaGeneratorPath=tools\SchemaGenerator\SchemaGeneratorConsole.dll
set DataAccessBinPath=\CheckList.DataAccess\bin\Debug\netcoreapp2.2
set ConfigurationProvider=CheckList.DataAccess.ConfigurationProvider,CheckList.DataAccess
set NHibernatePath=\CheckList.Interfaces\bin\Debug\netcoreapp2.2\CheckList.NHibernate.config

@ECHO OFF

REM if defined PostDeploymentScripts set PostDeploymentScriptsParam="-pd=%PostDeploymentScripts%"

echo *******************
echo build %title%
echo *******************

dotnet build "%~dp0%InterfaceProject%" -c Debug -v minimal


pause

echo.
echo *******************
echo Deploy %Title% Schema
echo *******************
dotnet "%~dp0%SchemaGeneratorPath%" -rn "#DBName#-Model" -o DropAndCreate -l "%~dp0%DataAccessBinPath%" -cp "%ConfigurationProvider%" -cfgFile "%~dp0%NHibernatePath%" ^
%PostDeploymentScriptsParam%

echo.

pause
