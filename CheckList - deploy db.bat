@ECHO OFF
set ProjectName=CheckList.Interfaces
set WcfServicesHostingProjectPath=.\CheckList.Interfaces
set DomainDataProjectPath=.\CheckList.DataAccess
set WcfServicesHostingProjectName=CheckList.Interfaces.csproj
set HibernateConfigFile=CheckList.NHibernate.config
set DBtype=1
set Configuration=Debug
set ScriptFolder=ChangeScripts

set WcfServicesHostingProjectFull=%~dp0%WcfServicesHostingProjectPath%\%WcfServicesHostingProjectName%
set HibernateConfigFileFull=%~dp0%WcfServicesHostingProjectPath%\bin\Debug\netcoreapp2.2\%HibernateConfigFile%
set ChangeScriptsPath=%~dp0%DomainDataProjectPath%\%ScriptFolder%


echo *******************
echo build %title%
echo *******************

dotnet build "%~dp0%WcfServicesHostingProjectFull%" -c Debug -v minimal

:DeployDBFromChangeScripts
echo *******************
echo Deploy %ProjectName% (%Configuration%, %ScriptFolder%)
echo *******************

"%~dp0tools\DBMigrator\DBMigratorConsole.exe" -s -d -f="%ChangeScriptsPath%" -cf "%HibernateConfigFileFull%" -dbType=%DBtype% -csn="MainDB" 

IF %ERRORLEVEL% == 0 GOTO EndSuccess
IF NOT %ERRORLEVEL% == 0 GOTO EndError


:EndError
pause
exit %ERRORLEVEL%

:EndSuccess
echo .





