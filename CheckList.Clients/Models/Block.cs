﻿using System.Collections.Generic;

namespace CheckList.Clients.Models
{
    /// <summary>
    /// Блока чек листа.
    /// </summary>
    public class Block
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Идентификатор блока.
        /// </summary>
        public int DicBlockId { get; set; }

        /// <summary>
        /// Название блока.
        /// </summary>
        public string DicBlockName { get; set; }

        /// <summary>
        /// Описание.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Идентификатор вышестоящей группы.
        /// </summary>
        public int? ParentBlockId { get; set; }

        /// <summary>
        /// Приоритет.
        /// </summary>
        public int Priority { get; set; }

        /// <summary>
        /// Шаблон чек листа.
        /// </summary>
        public int TemplateId { get; set; }

        /// <summary>
        /// Список элементов.
        /// </summary>
        public List<Item> Items { get; set; }

        /// <summary>
        /// Список блоков.
        /// </summary>
        public List<Block> Blocks { get; set; }

        /// <summary>
        /// Отображать список элементов.
        /// </summary>
        public bool ShowItems { get; set; }

        /// <summary>
        /// Конструктор.
        /// </summary>
        public Block()
        {
            Items = new List<Item>();
            Blocks = new List<Block>();
            ShowItems = true;
        }
    }
}
