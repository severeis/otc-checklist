﻿using System;
using System.Collections.Generic;

namespace CheckList.Clients.Models
{
    /// <summary>
    /// Чек лист.
    /// </summary>
    public class CheckList
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        public CheckList()
        {
            Items = new List<Item>();
            Blocks = new List<Block>();
        }

        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Дата создания.
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Идентификатор заказа.
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// Шаблон чек листа.
        /// </summary>
        public int TemplateId { get; set; }

        /// <summary>
        /// Название шаблона чек листа.
        /// </summary>
        public string TemplateName { get; set; }

        /// <summary>
        /// Список элементов.
        /// </summary>
        public List<Item> Items { get; set; }

        /// <summary>
        /// Список блоков.
        /// </summary>
        public List<Block> Blocks { get; set; }
    }
}
