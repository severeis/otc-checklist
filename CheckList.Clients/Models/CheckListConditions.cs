﻿namespace CheckList.Clients.Models
{
    /// <summary>
    /// Идентификаторы типов условий отбора чеклистов.
    /// </summary>
    public static class CheckListConditions
    {
        /// <summary>
        /// Отбор по параметрам тендера.
        /// </summary>
        public const int Tender = 1;

        /// <summary>
        /// Отбор по параметрам плана закупок.
        /// </summary>
        public const int TradePlan = 2;
    }
}
