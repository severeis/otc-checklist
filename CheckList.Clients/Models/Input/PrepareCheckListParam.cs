﻿using System;

namespace CheckList.Clients.Models.Input
{
    /// <summary>
    /// Параметры для поиска шаблона чек листа.
    /// </summary>
    public class PrepareCheckListParam
    {
        /// <summary>
        /// Идентификатор типа условий для отбора.
        /// </summary>
        public int? ConditionId { get; set; }

        /// <summary>
        /// Условия отбора по тендеру.
        /// </summary>
        public TenderConditionParam TenderCondition { get; set; }

        /// <summary>
        /// Условия отбора по плану закупки.
        /// </summary>
        public TradePlanConditionParam TradePlanCondition { get; set; }

        /// <summary>
        /// Идентификатор способа закупки.
        /// </summary>
        [Obsolete("use TenderCondition.PurchaseMethod")]
        public int? PurchaseMethod { get; set; }

        /// <summary>
        /// Идентификатор закона по которому проводится тендер.
        /// </summary>
        [Obsolete("use TenderCondition.FederalLawType")]
        public int? TenderLawType { get; set; }

        /// <summary>
        /// Идентификатор площадки.
        /// </summary>
        [Obsolete("use TenderCondition.MarketPlaceId")]
        public int? MarketPlaceId { get; set; }
    }
}
