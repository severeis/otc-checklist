﻿namespace CheckList.Clients.Models.Input
{
    /// <summary>
    /// Условия отбора по тендеру.
    /// </summary>
    public class TenderConditionParam
    {
        /// <summary>
        /// Идентификатор способа закупки.
        /// </summary>
        public int? PurchaseMethod { get; set; }

        /// <summary>
        /// Идентификатор закона по которому проводится тендер.
        /// </summary>
        public int? FederalLawType { get; set; }

        /// <summary>
        /// Идентификатор площадки.
        /// </summary>
        public int? MarketPlaceId { get; set; }
    }
}
