﻿namespace CheckList.Clients.Models.Input
{
    /// <summary>
    /// Условия отбора по плану закупки.
    /// </summary>
    public class TradePlanConditionParam
    {
        /// <summary>
        /// Идентификатор закона.
        /// </summary>
        public int? FederalLawType { get; set; }
    }
}
