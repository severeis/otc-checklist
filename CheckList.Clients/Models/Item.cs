﻿using System;

namespace CheckList.Clients.Models
{
    /// <summary>
    /// Элемент чек листа.
    /// </summary>
    public class Item
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Идентификатор чек листа.
        /// </summary>
        public int? CheckListId { get; set; }

        /// <summary>
        /// Элемент шаблона чек листа.
        /// </summary>
        public int? TemplateItemId { get; set; }

        /// <summary>
        /// Дата завершения.
        /// </summary>
        public DateTime? CheckDate { get; set; }

        /// <summary>
        /// Идентификатор элемента шаблона чек листа.
        /// </summary>
        public int? DicItemId { get; set; }

        /// <summary>
        /// Название элемента шаблона чек листа.
        /// </summary>
        public string DicItemName { get; set; }

        /// <summary>
        /// Описание.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Шаблон блока чек листа.
        /// </summary>
        public int? TemplateBlockId { get; set; }

        /// <summary>
        /// Приоритет.
        /// </summary>
        public int Priority { get; set; }

        /// <summary>
        /// Шаблон чек листа.
        /// </summary>
        public int? TemplateId { get; set; }

        /// <summary>
        /// Отображать описание элемента.
        /// </summary>
        public bool ShowDescription { get; set; }

        /// <summary>
        /// Выполнено.
        /// </summary>
        public bool Checked
        {
            get
            {
                return CheckDate.HasValue;
            }
        }
    }
}
