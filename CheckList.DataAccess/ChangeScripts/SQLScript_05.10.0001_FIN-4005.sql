﻿
if not exists 
(
    select so1.id from sysobjects so1 
    where so1.type in ('U', 'TT') and so1.name = 'DicPurchaseType'
)
begin
   CREATE TABLE [dbo].[DicPurchaseType]
(
	[Id] [int] IDENTITY (1,1) NOT NULL,
	[RowVersion] [int] NOT NULL,
	[Name] [nvarchar] (255) COLLATE Cyrillic_General_CI_AI NOT NULL,
	CONSTRAINT [PK_DicPurchaseType] PRIMARY KEY CLUSTERED
	(
		[Id] ASC
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY  = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


end

if not exists 
(
    select so1.id from sysobjects so1 
    where so1.type in ('U', 'TT') and so1.name = 'DicItem'
)
begin
   CREATE TABLE [dbo].[DicItem]
(
	[Id] [int] IDENTITY (1,1) NOT NULL,
	[RowVersion] [int] NOT NULL,
	[Name] [nvarchar] (255) COLLATE Cyrillic_General_CI_AI NOT NULL,
	CONSTRAINT [PK_DicItem] PRIMARY KEY CLUSTERED
	(
		[Id] ASC
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY  = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


end

if not exists 
(
    select so1.id from sysobjects so1 
    where so1.type in ('U', 'TT') and so1.name = 'DicEtp'
)
begin
   CREATE TABLE [dbo].[DicEtp]
(
	[Id] [int] IDENTITY (1,1) NOT NULL,
	[RowVersion] [int] NOT NULL,
	[Name] [nvarchar] (255) COLLATE Cyrillic_General_CI_AI NOT NULL,
	CONSTRAINT [PK_DicEtp] PRIMARY KEY CLUSTERED
	(
		[Id] ASC
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY  = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


end

if  exists 
(
    select so1.id from syscolumns so1 
    join sysobjects so2 on so1.id=so2.id
    where so1.name = 'ExecuteDate' and so2.Name = 'SchemaChangeLog'
)
begin
   ALTER TABLE [dbo].[SchemaChangeLog] ALTER COLUMN [ExecuteDate] [datetime2] NOT NULL

end


if not exists 
(
    select so1.id from sysobjects so1 
    where so1.type in ('U', 'TT') and so1.name = 'DicCondition'
)
begin
   CREATE TABLE [dbo].[DicCondition]
(
	[Id] [int] IDENTITY (1,1) NOT NULL,
	[RowVersion] [int] NOT NULL,
	[Name] [nvarchar] (255) COLLATE Cyrillic_General_CI_AI NOT NULL,
	[Code] [nvarchar] (255) COLLATE Cyrillic_General_CI_AI NULL,
	CONSTRAINT [PK_DicCondition] PRIMARY KEY CLUSTERED
	(
		[Id] ASC
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY  = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


end

if not exists 
(
    select so1.id from sysobjects so1 
    where so1.type in ('U', 'TT') and so1.name = 'DicBlock'
)
begin
   CREATE TABLE [dbo].[DicBlock]
(
	[Id] [int] IDENTITY (1,1) NOT NULL,
	[RowVersion] [int] NOT NULL,
	[Name] [nvarchar] (255) COLLATE Cyrillic_General_CI_AI NOT NULL,
	CONSTRAINT [PK_DicBlock] PRIMARY KEY CLUSTERED
	(
		[Id] ASC
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY  = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


end

if not exists 
(
    select so1.id from sysobjects so1 
    where so1.type in ('U', 'TT') and so1.name = 'CheckList'
)
begin
   CREATE TABLE [dbo].[CheckList]
(
	[Id] [int] IDENTITY (1,1) NOT NULL,
	[RowVersion] [int] NOT NULL,
	[CreateDate] [datetime2] NOT NULL,
	[OrderId] [int] NOT NULL,
	[Template_Id] [int] NOT NULL,
	CONSTRAINT [PK_CheckList] PRIMARY KEY CLUSTERED
	(
		[Id] ASC
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY  = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

end

if not exists 
(
    select so1.id from sysobjects so1 
    where so1.type in ('U', 'TT') and so1.name = 'Template'
)
begin
   CREATE TABLE [dbo].[Template]
(
	[Id] [int] IDENTITY (1,1) NOT NULL,
	[RowVersion] [int] NOT NULL,
	[Name] [nvarchar] (255) COLLATE Cyrillic_General_CI_AI NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreateDate] [datetime2] NOT NULL,
	[DicCondition_Id] [int] NOT NULL,
	CONSTRAINT [PK_Template] PRIMARY KEY CLUSTERED
	(
		[Id] ASC
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY  = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


if not exists 
(
    select so1.id from sysobjects so1 
    join sysobjects so2 on so1.parent_obj =so2.id
    where so1.xtype = 'F' and so1.name = 'FK_Template_DicCondition__DicCondition_Id' and so2.Name = 'Template'
)
begin
   ALTER TABLE [dbo].[Template] ADD CONSTRAINT [FK_Template_DicCondition__DicCondition_Id] FOREIGN KEY
	(
		[DicCondition_Id]
	)
	REFERENCES [dbo].[DicCondition]
	(
		[Id]
	)
end

if not exists 
(
    select so1.id from sysobjects so1 
    join sysobjects so2 on so1.parent_obj =so2.id
    where so1.xtype = 'F' and so1.name = 'FK_CheckList_Template__Template_Id' and so2.Name = 'CheckList'
)
begin
   ALTER TABLE [dbo].[CheckList] ADD CONSTRAINT [FK_CheckList_Template__Template_Id] FOREIGN KEY
	(
		[Template_Id]
	)
	REFERENCES [dbo].[Template]
	(
		[Id]
	)
end

end

if not exists 
(
    select so1.id from sysobjects so1 
    where so1.type in ('U', 'TT') and so1.name = 'Item'
)
begin
   CREATE TABLE [dbo].[Item]
(
	[Id] [int] IDENTITY (1,1) NOT NULL,
	[RowVersion] [int] NOT NULL,
	[CheckDate] [datetime2] NULL,
	[CheckList_Id] [int] NOT NULL,
	[TemplateItem_Id] [int] NOT NULL,
	CONSTRAINT [PK_Item] PRIMARY KEY CLUSTERED
	(
		[Id] ASC
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY  = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


if not exists 
(
    select so1.id from sysobjects so1 
    join sysobjects so2 on so1.parent_obj =so2.id
    where so1.xtype = 'F' and so1.name = 'FK_Item_CheckList__CheckList_Id' and so2.Name = 'Item'
)
begin
   ALTER TABLE [dbo].[Item] ADD CONSTRAINT [FK_Item_CheckList__CheckList_Id] FOREIGN KEY
	(
		[CheckList_Id]
	)
	REFERENCES [dbo].[CheckList]
	(
		[Id]
	)
end


end

if not exists 
(
    select so1.id from sysobjects so1 
    where so1.type in ('U', 'TT') and so1.name = 'FederalLawType'
)
begin
   CREATE TABLE [dbo].[FederalLawType]
(
	[Id] [int] IDENTITY (1,1) NOT NULL,
	[RowVersion] [int] NOT NULL,
	[Name] [nvarchar] (255) COLLATE Cyrillic_General_CI_AI NOT NULL,
	CONSTRAINT [PK_FederalLawType] PRIMARY KEY CLUSTERED
	(
		[Id] ASC
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY  = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


end

if not exists 
(
    select so1.id from sysobjects so1 
    where so1.type in ('U', 'TT') and so1.name = 'TenderCondition'
)
begin
   CREATE TABLE [dbo].[TenderCondition]
(
	[Id] [int] IDENTITY (1,1) NOT NULL,
	[RowVersion] [int] NOT NULL,
	[DicPurchaseType_Id] [int] NULL,
	[FederalLawType_Id] [int] NULL,
	[DicEtp_Id] [int] NOT NULL,
	[Template_Id] [int] NOT NULL,
	CONSTRAINT [PK_TenderCondition] PRIMARY KEY CLUSTERED
	(
		[Id] ASC
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY  = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


if not exists 
(
    select so1.id from sysobjects so1 
    join sysobjects so2 on so1.parent_obj =so2.id
    where so1.xtype = 'F' and so1.name = 'FK_TenderCondition_DicEtp__DicEtp_Id' and so2.Name = 'TenderCondition'
)
begin
   ALTER TABLE [dbo].[TenderCondition] ADD CONSTRAINT [FK_TenderCondition_DicEtp__DicEtp_Id] FOREIGN KEY
	(
		[DicEtp_Id]
	)
	REFERENCES [dbo].[DicEtp]
	(
		[Id]
	)
end


if not exists 
(
    select so1.id from sysobjects so1 
    join sysobjects so2 on so1.parent_obj =so2.id
    where so1.xtype = 'F' and so1.name = 'FK_TenderCondition_DicPurchaseType__PurchaseType_Id' and so2.Name = 'TenderCondition'
)
begin
   ALTER TABLE [dbo].[TenderCondition] ADD CONSTRAINT [FK_TenderCondition_DicPurchaseType__PurchaseType_Id] FOREIGN KEY
	(
		[DicPurchaseType_Id]
	)
	REFERENCES [dbo].[DicPurchaseType]
	(
		[Id]
	)
end


if not exists 
(
    select so1.id from sysobjects so1 
    join sysobjects so2 on so1.parent_obj =so2.id
    where so1.xtype = 'F' and so1.name = 'FK_TenderCondition_FederalLawType__FederalLawType_Id' and so2.Name = 'TenderCondition'
)
begin
   ALTER TABLE [dbo].[TenderCondition] ADD CONSTRAINT [FK_TenderCondition_FederalLawType__FederalLawType_Id] FOREIGN KEY
	(
		[FederalLawType_Id]
	)
	REFERENCES [dbo].[FederalLawType]
	(
		[Id]
	)
end


if not exists 
(
    select so1.id from sysobjects so1 
    join sysobjects so2 on so1.parent_obj =so2.id
    where so1.xtype = 'F' and so1.name = 'FK_TenderCondition_Template__Template_Id' and so2.Name = 'TenderCondition'
)
begin
   ALTER TABLE [dbo].[TenderCondition] ADD CONSTRAINT [FK_TenderCondition_Template__Template_Id] FOREIGN KEY
	(
		[Template_Id]
	)
	REFERENCES [dbo].[Template]
	(
		[Id]
	)
end


end

if not exists 
(
    select so1.id from sysobjects so1 
    where so1.type in ('U', 'TT') and so1.name = 'TemplateItem'
)
begin
   CREATE TABLE [dbo].[TemplateItem]
(
	[Id] [int] IDENTITY (1,1) NOT NULL,
	[RowVersion] [int] NOT NULL,
	[Description] [nvarchar] (1024) COLLATE Cyrillic_General_CI_AI NULL,
	[Priority] [int] NOT NULL,
	[DicItem_Id] [int] NOT NULL,
	[TemplateBlock_Id] [int] NULL,
	[Template_Id] [int] NULL,
	CONSTRAINT [PK_TemplateItem] PRIMARY KEY CLUSTERED
	(
		[Id] ASC
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY  = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


if not exists 
(
    select so1.id from sysobjects so1 
    join sysobjects so2 on so1.parent_obj =so2.id
    where so1.xtype = 'F' and so1.name = 'FK_TemplateItem_DicItem__DicItem_Id' and so2.Name = 'TemplateItem'
)
begin
   ALTER TABLE [dbo].[TemplateItem] ADD CONSTRAINT [FK_TemplateItem_DicItem__DicItem_Id] FOREIGN KEY
	(
		[DicItem_Id]
	)
	REFERENCES [dbo].[DicItem]
	(
		[Id]
	)
end


if not exists 
(
    select so1.id from sysobjects so1 
    join sysobjects so2 on so1.parent_obj =so2.id
    where so1.xtype = 'F' and so1.name = 'FK_TemplateItem_Template__Template_Id' and so2.Name = 'TemplateItem'
)
begin
   ALTER TABLE [dbo].[TemplateItem] ADD CONSTRAINT [FK_TemplateItem_Template__Template_Id] FOREIGN KEY
	(
		[Template_Id]
	)
	REFERENCES [dbo].[Template]
	(
		[Id]
	)
end



if not exists 
(
    select so1.id from sysobjects so1 
    join sysobjects so2 on so1.parent_obj =so2.id
    where so1.xtype = 'F' and so1.name = 'FK_Item_TemplateItem__TemplateItem_Id' and so2.Name = 'Item'
)
begin
   ALTER TABLE [dbo].[Item] ADD CONSTRAINT [FK_Item_TemplateItem__TemplateItem_Id] FOREIGN KEY
	(
		[TemplateItem_Id]
	)
	REFERENCES [dbo].[TemplateItem]
	(
		[Id]
	)
end


end

if not exists 
(
    select so1.id from sysobjects so1 
    where so1.type in ('U', 'TT') and so1.name = 'TemplateBlock'
)
begin
   CREATE TABLE [dbo].[TemplateBlock]
(
	[Id] [int] IDENTITY (1,1) NOT NULL,
	[RowVersion] [int] NOT NULL,
	[Description] [nvarchar] (1024) COLLATE Cyrillic_General_CI_AI NULL,
	[ParentBlockId] [int] NULL,
	[Priority] [int] NOT NULL,
	[Template_Id] [int] NOT NULL,
	[DicBlock_Id] [int] NOT NULL,
	CONSTRAINT [PK_TemplateBlock] PRIMARY KEY CLUSTERED
	(
		[Id] ASC
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY  = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


if not exists 
(
    select so1.id from sysobjects so1 
    join sysobjects so2 on so1.parent_obj =so2.id
    where so1.xtype = 'F' and so1.name = 'FK_TemplateBlock_DicBlock__DicBlock_Id' and so2.Name = 'TemplateBlock'
)
begin
   ALTER TABLE [dbo].[TemplateBlock] ADD CONSTRAINT [FK_TemplateBlock_DicBlock__DicBlock_Id] FOREIGN KEY
	(
		[DicBlock_Id]
	)
	REFERENCES [dbo].[DicBlock]
	(
		[Id]
	)
end


if not exists 
(
    select so1.id from sysobjects so1 
    join sysobjects so2 on so1.parent_obj =so2.id
    where so1.xtype = 'F' and so1.name = 'FK_TemplateBlock_Template__Template_Id' and so2.Name = 'TemplateBlock'
)
begin
   ALTER TABLE [dbo].[TemplateBlock] ADD CONSTRAINT [FK_TemplateBlock_Template__Template_Id] FOREIGN KEY
	(
		[Template_Id]
	)
	REFERENCES [dbo].[Template]
	(
		[Id]
	)
end

if not exists 
(
    select so1.id from sysobjects so1 
    join sysobjects so2 on so1.parent_obj =so2.id
    where so1.xtype = 'F' and so1.name = 'FK_TemplateItem_TemplateBlock__TemplateBlock_Id' and so2.Name = 'TemplateItem'
)
begin
   ALTER TABLE [dbo].[TemplateItem] ADD CONSTRAINT [FK_TemplateItem_TemplateBlock__TemplateBlock_Id] FOREIGN KEY
	(
		[TemplateBlock_Id]
	)
	REFERENCES [dbo].[TemplateBlock]
	(
		[Id]
	)
end


end


