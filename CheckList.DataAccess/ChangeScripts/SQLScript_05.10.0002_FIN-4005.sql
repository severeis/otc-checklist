﻿if  exists 
(
    select so1.id from syscolumns so1 
    join sysobjects so2 on so1.id=so2.id
    where so1.name = 'Description' and so2.Name = 'TemplateItem'
)
begin
   ALTER TABLE [dbo].[TemplateItem] ALTER COLUMN [Description] [nvarchar] (max) COLLATE Cyrillic_General_CI_AI NULL

end

if  exists 
(
    select so1.id from syscolumns so1 
    join sysobjects so2 on so1.id=so2.id
    where so1.name = 'Description' and so2.Name = 'TemplateBlock'
)
begin
   ALTER TABLE [dbo].[TemplateBlock] ALTER COLUMN [Description] [nvarchar] (max) COLLATE Cyrillic_General_CI_AI NULL

end

if  exists 
(
    select so1.id from syscolumns so1 
    join sysobjects so2 on so1.id=so2.id
    where so1.name = 'Name' and so2.Name = 'Template'
)
begin
   ALTER TABLE [dbo].[Template] ALTER COLUMN [Name] [nvarchar] (1024) COLLATE Cyrillic_General_CI_AI NOT NULL

end

