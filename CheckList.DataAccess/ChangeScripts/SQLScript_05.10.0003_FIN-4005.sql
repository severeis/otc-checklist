﻿SET IDENTITY_INSERT [dbo].[DicBlock] ON

declare @DicBlock table(id int, Name nvarchar(255))

insert @DicBlock
values(1, 'Анализ и принятие решения об участии')

insert @DicBlock
values(2, 'Подготовка к подаче заявки')

insert @DicBlock
values(3, 'Подача заявки')

insert @DicBlock
values(4, 'Участие в аукционе')

insert @DicBlock
values(5, 'Заключение контракта')

merge into [dbo].[DicBlock] as target
using @DicBlock as source on target.Id = source.Id
when matched then
    update set Name = source.Name
when not matched then
    insert (Id, RowVersion, Name) values (source.Id, 1, source.Name);

SET IDENTITY_INSERT [dbo].[DicBlock] OFF
GO

/*[DicItem]*/
SET IDENTITY_INSERT [dbo].[DicItem] ON

declare @DicItem table(id int, Name nvarchar(255))

insert @DicItem
values(1, 'Проверено соответствие организации условиям допуска')

insert @DicItem
values(2, 'Сформирован план выполнения контракта')

insert @DicItem
values(3, 'Определен способ и источник финансирования работ и материалов')

insert @DicItem
values(4, 'Определена себестоимость и минимальная цена участия')

insert @DicItem
values(5, 'Определен способ и источник обеспечения исполнения контракта')

insert @DicItem
values(6, 'Есть 2 ключа ЭП с оставшимся сроком действия не менее 2 мес')

insert @DicItem
values(7, 'На ЭТП зарегистрирован и активен личный кабинет ')

insert @DicItem
values(8, 'Есть 2 зарегистрированных пользователя на ЭТП')

insert @DicItem
values(9, 'Есть 2 настроенных ПК для участия в торгах')

insert @DicItem
values(10, 'Есть основное и резервное интернет-подключение')

insert @DicItem
values(11, 'Документы компании актуальны в ЛК на ЭТП')

insert @DicItem
values(12, 'Составлен список требуемых документов для 1-й и 2-й частей заявок')

insert @DicItem
values(13, 'Документы для первой части подготовлены')

insert @DicItem
values(14, 'Документы для второй части подготовлены')

insert @DicItem
values(15, 'На спец. счете в банке есть свободные средства в объеме обспечения заявки')

insert @DicItem
values(16, 'Получены разъяснения документации')

insert @DicItem
values(17, 'Загружена заявка на площадку')

insert @DicItem
values(18, 'Заявка опубликована')

insert @DicItem
values(19, 'За 1 час до начала аукциона проверена возможность входа в ЛК ЭТП')

insert @DicItem
values(20, 'Сотрудники проинструктированы о минимальной цене участия')

insert @DicItem
values(21, 'Поданы ценовые предложения')

insert @DicItem
values(22, 'Получена БГ или собраны средства для обеспечения контракта')

insert @DicItem
values(23, 'Направлены протоколы разногласий')

insert @DicItem
values(24, 'Контракт заключен')


merge into [dbo].[DicItem] as target
using @DicItem as source on target.Id = source.Id
when matched then
    update set Name = source.Name
when not matched then
    insert (Id, RowVersion, Name) values (source.Id, 1, source.Name);

SET IDENTITY_INSERT [dbo].[DicItem] OFF

GO

/* DicCondition*/
SET IDENTITY_INSERT [dbo].[DicCondition] ON

declare @DicCondition table(id int, Name nvarchar(255))

insert @DicCondition
values(1, 'Tender')

merge into [dbo].[DicCondition] as target
using @DicCondition as source on target.Id = source.Id
when matched then
    update set Name = source.Name
when not matched then
    insert (Id, RowVersion, Name) values (source.Id, 1, source.Name);

SET IDENTITY_INSERT [dbo].[DicCondition] OFF
GO

declare @Template table(Name nvarchar(255), DicCondition_Id int)

insert @Template
values('Подача заявки для участия в ЭА', 1)


merge into [dbo].[Template] as target
using @Template as source on target.Name = source.Name
when matched then
    update set DicCondition_Id = source.DicCondition_Id, CreateDate = GetDate()
when not matched then
    insert (RowVersion, Name, IsActive, CreateDate, DicCondition_Id) values (1, source.Name, 1, GetDate(), source.DicCondition_Id);

GO

declare @dicBlockId int
declare @templateId int

select top 1 @templateId = Id 
	from [dbo].[Template]
		where Name = 'Подача заявки для участия в ЭА';

select top 1 @dicBlockId = Id	
	from [dbo].[DicBlock]
		where Name = 'Анализ и принятие решения об участии';

declare @TemplateBlock table(Description nvarchar(1024), Priority int, Template_Id int, DicBlock_Id int)

insert @TemplateBlock
values ('', 1, @templateId, @dicBlockId)

merge into [dbo].[TemplateBlock] as target
using @TemplateBlock as source on target.Template_Id = source.Template_Id and target.DicBlock_Id = source.DicBlock_Id
when matched then
    update set Description = source.Description, Priority = source.Priority
when not matched then
    insert (RowVersion, Description, Priority, Template_Id, DicBlock_Id) values (1, source.Description, source.Priority, source.Template_Id, source.DicBlock_Id);

declare @templateBlockId int;

select top 1 @templateBlockId = Id
	from [dbo].[TemplateBlock]
		where Template_Id = @templateId and DicBlock_Id = @dicBlockId;

declare @TemplateItem table (Description nvarchar(max), Priority int, DicItem_Id int)

insert @TemplateItem
values ('Проверены наличие ограничений для участия в тендере:
  * участие только для СМП,  участие только для СОНО (ст.30 44-ФЗ, может быть установлено ограничение участия-участвовать могут только организации из числа Субъектов малого предпринимательства и социально-ориентированных некоммерческих организций или преимущества-побеждает организация СМП или СОНО - оплата по контракту в течение 15 дней, участвовать могут все)- см. информационную карту аукциона. 
  * преимущества для УИС (ст.28 44-ФЗ предприятия уголовно-исполнительной системы имеют преимущества в отношении определенных ТРУ, в случае победы УИС цена контракта может быть увеличена на 15%, но не выше НМЦ, наличие преимуществ можно уточнить в информационной карте аукциона), участвовать могут все. 
  *преимущества для Оинв (ст.29 44-ФЗ организации Инвалидов имеют преимущества в отношении определенных ТРУ, в случае победы Оинв цена контракта может быть увеличена на 15%, но не выше НМЦ, наличие преимуществ можно уточнить в информационной карте аукциона), участвовать могут все,
  * преференции для определенных  товаров из ЕАЭС (Россия, Армения, Белоруссия, Казахстан, Киргизия, в соответсвии с Приказом № 126Н от 04.06.2018  (в случае победы заявки с товарами из ЕАЭС, контракт заключается по цене победителя, но не более НМЦ, в случае победы заявки с иными товарами от цены победителя удерживается 15%, наличие преимуществ можно уточнить в информационной карте аукциона), участвовать могут все,
  * заказчик может установить дополнительные требования по наличию требований лицензий, свидетельств, допусков ит.д. Установлены ограничения или нет - см. информационную карту аукциона, требования по второй части заявки.', 1, 1)
insert @TemplateItem
values ('Проанализированы требования к конечному результату
Определены необходимые для выполнения контракта ресурсы. Определено какими силами будет выполняться контракт, какие понадобятся материалы и оборудование, какие работы будут переданы субподрядчикам.
Определены сроки и последовательность выполнения контракта.', 2, 2)
insert @TemplateItem
values ('В случае отсутствия аванса по контракту или в случае, если аванса недостаточно, определен объем и источник необходимого финансирования работ и закупки материалов. Получить деньги на обеспечение и исполнение контракта можно тут: https://penenza.ru/get', 3, 3)
insert @TemplateItem
values ('Проведен расчет себестоимости и всех сопуствующих расходов (доставка, погрузка, разгрузка, налог, сборы, иные обязательные платежи).
С субподрядчиками и поставщиками материалов достигнута договоренность об условиях и цене (запрос на коммерческие предложения можно разместить на ОТС: https://tender.otc.ru/main/sso/Login.aspx?_ga=2.58122985.2126069390.1551336953-1207371832.1547640118)
С необходимыми сотрудниками\департаментами согласована минимальная цена, по которой организация готова выполнять этот контракт. Рекомендуется определять 3 цены отсечения:
 * зеленая зона - цена с приемлемым уровнем прибыльности исполнения контракта
 * желтая зона - цена с низким уровнем рентабельности, но выше себестоимости
* красная зона - цена ниже себестоимости, но уровень убытка приемлем (например для целей выхода на новый рынок или создаия сложностей конкурентам)', 4, 4)
insert @TemplateItem
values ('Рассчитаны затраты на получение банковской гарантии или изъятие денежных средств из оборота компании для перечисления заказчику. Выбран способ предоставления обеспечения исполнения контракта. В случае решения о предоставлении БГ, с банками проведены переговоры, получено предварительное одобрение. Получи банкоскую гарантию в режиме онлайн тут: https://otc.ru/finance/bg', 5, 5)


merge into [dbo].[TemplateItem] as target
using @TemplateItem as source on target.DicItem_Id = source.DicItem_Id and target.TemplateBlock_Id = @templateBlockId and target.Template_Id = @templateId
when matched then
    update set Description = source.Description, Priority = source.Priority
when not matched then
    insert (RowVersion, Description, Priority, DicItem_Id, TemplateBlock_Id, Template_Id) values (1, source.Description, source.Priority, source.DicItem_Id, @templateBlockId, @templateId);



/******/

select top 1 @dicBlockId = Id	
	from [dbo].[DicBlock]
		where Name = 'Подготовка к подаче заявки';
		
delete @TemplateBlock

insert @TemplateBlock
values ('', 2, @templateId, @dicBlockId)

merge into [dbo].[TemplateBlock] as target
using @TemplateBlock as source on target.Template_Id = source.Template_Id and target.DicBlock_Id = source.DicBlock_Id
when matched then
    update set Description = source.Description, Priority = source.Priority
when not matched then
    insert (RowVersion, Description, Priority, Template_Id, DicBlock_Id) values (1, source.Description, source.Priority, source.Template_Id, source.DicBlock_Id);

select top 1 @templateBlockId = Id
	from [dbo].[TemplateBlock]
		where Template_Id = @templateId and DicBlock_Id = @dicBlockId;

delete @TemplateItem

insert @TemplateItem
values ('Есть две 2 ключа квалифицированной электронной подписи. 
Проверена валидность каждого ключа и отсутствие его в списке отозванных. Проверить можно здесь: https://sberbank-ast.ru/TestDS.aspx
Проверен срок действия каждого ключа (Сделать это можнов браузере IE: Сервис-свойства браузера-содержание-сертфикаты. Напротив каждого сертификата ключа указан срок действия).', 1, 6)

insert @TemplateItem
values ('Если организация ранее была аккредитована на ЭТП проверить, что срок аккредитации не истек. В ЛК http://sberbank-ast.ru кликнув на ФИО в правом верхнем углу.
Если организация не аккредитована на ЭТП, подать заявку на регистрацию в ЕИС http://zakupki.gov.ru/epz/main/public/home.html , после регистрации проверить, что на ЭТП появился личный кабинет', 2, 7)

insert @TemplateItem
values ('Проверить, что 2 пользователя (каждый со своим ключом ЭП) могут зайти на ЭТП: https://login.sberbank-ast.ru/
Если ЭЦП не добавена, добавить можно по ссылке: https://sberbank-ast.ru/freeregister.aspx', 3, 8)

insert @TemplateItem
values ('На 2 ПК установлено ПО для работы с ЭЦП (КриптоПРО или VipNet). Проверить наличие КриптоПРО или VipNet можно через раздел ПК - поиск программ, указав название криптопровайдера. Проверить срок действия лицензии можно в разделе Управление лицензиями, получить криптопровайдер можно здесь: https://otc.ru/crypto
На 2 ПК установлена библиотека для работы с ЭЦП из браузера, установить можно по ссылке http://utp.sberbank-ast.ru/main/notice/700/plugin
На 2 ПК проверена возможность входа на площадку
На 2 ПК установлены по 2 ключа электронной подписи', 4, 9)

insert @TemplateItem
values ('Настроены и проверены 2 интернет-канала, основной и резервный, проверено переключение на резервный канал в случае отключения основного', 5, 10)

insert @TemplateItem
values ('Проверена актуальность документов (выписка из ЕГРЮЛ, срок полномочий директора, в решение об одобрении сделки указана актуальная сумма), проверить можно в ЛК компании на электронной площадке в разделе "Личный кабинет"', 6, 11)

insert @TemplateItem
values ('Внимательно изучена тендерная документация, в единый Excel-документ собран список всех требуемых документов, для каждого документа из списка указаны требования к содержимому, к форме.', 7, 12)

insert @TemplateItem
values ('Подготовлены все документы первой части заявки, форма и содержимое документов соответствуют требованиям', 8, 13)

insert @TemplateItem
values ('Подготовлены все документы первой части заявки, форма и содержимое документов соответствуют требованиям', 9, 14)

insert @TemplateItem
values ('Спецсчет в банке открыт,
Спецсчет в банке не заблокирован  (проверить можно в банк-клиенте),
Средства с расчетного счета перечислены на спецсчет (если денег нет, то можно взять быстро деньги взаймы тут: https://penenza.ru/),
Средства поступили на спецсчет, при этом не списались по старым долгам и не заблокировались  (проверить можно в банк-клиенте )', 10, 15)

insert @TemplateItem
values ('В случае наличия неясностей и нечетких формулировок отправлены запросы на разъяснение, получены ответы. Отправить запрос на разъяснение можно здесь: https://sberbank-ast.ru/tradezone/Supplier/ESPurchaseList.aspx Запрос направляется не позднее трех дней до окончания подачи заявок, ответ размещается на сайте ЕИС', 11, 16)


merge into [dbo].[TemplateItem] as target
using @TemplateItem as source on target.DicItem_Id = source.DicItem_Id and target.TemplateBlock_Id = @templateBlockId and target.Template_Id = @templateId
when matched then
    update set Description = source.Description, Priority = source.Priority
when not matched then
    insert (RowVersion, Description, Priority, DicItem_Id, TemplateBlock_Id, Template_Id) values (1, source.Description, source.Priority, source.DicItem_Id, @templateBlockId, @templateId);

	

/******/

select top 1 @dicBlockId = Id	
	from [dbo].[DicBlock]
		where Name = 'Подача заявки';
		
delete @TemplateBlock

insert @TemplateBlock
values ('', 3, @templateId, @dicBlockId)

merge into [dbo].[TemplateBlock] as target
using @TemplateBlock as source on target.Template_Id = source.Template_Id and target.DicBlock_Id = source.DicBlock_Id
when matched then
    update set Description = source.Description, Priority = source.Priority
when not matched then
    insert (RowVersion, Description, Priority, Template_Id, DicBlock_Id) values (1, source.Description, source.Priority, source.Template_Id, source.DicBlock_Id);

select top 1 @templateBlockId = Id
	from [dbo].[TemplateBlock]
		where Template_Id = @templateId and DicBlock_Id = @dicBlockId;

delete @TemplateItem

insert @TemplateItem
values ('В электронной форме заявки не площадке добавлена первая и вторая части заявки, указана необходимая информация. Сделать это можно в разделе "Реестр объявленных процедур", нажав на кнопку "Подать заявку".', 1, 17)

insert @TemplateItem
values ('Заявка подана. Для подачи заявки необходимо нажать на кнопку "Подписать и отправить".', 2, 18)

merge into [dbo].[TemplateItem] as target
using @TemplateItem as source on target.DicItem_Id = source.DicItem_Id and target.TemplateBlock_Id = @templateBlockId and target.Template_Id = @templateId
when matched then
    update set Description = source.Description, Priority = source.Priority
when not matched then
    insert (RowVersion, Description, Priority, DicItem_Id, TemplateBlock_Id, Template_Id) values (1, source.Description, source.Priority, source.DicItem_Id, @templateBlockId, @templateId);

		

/******/

select top 1 @dicBlockId = Id	
	from [dbo].[DicBlock]
		where Name = 'Участие в аукционе';
		
delete @TemplateBlock

insert @TemplateBlock
values ('', 4, @templateId, @dicBlockId)

merge into [dbo].[TemplateBlock] as target
using @TemplateBlock as source on target.Template_Id = source.Template_Id and target.DicBlock_Id = source.DicBlock_Id
when matched then
    update set Description = source.Description, Priority = source.Priority
when not matched then
    insert (RowVersion, Description, Priority, Template_Id, DicBlock_Id) values (1, source.Description, source.Priority, source.Template_Id, source.DicBlock_Id);

select top 1 @templateBlockId = Id
	from [dbo].[TemplateBlock]
		where Template_Id = @templateId and DicBlock_Id = @dicBlockId;

delete @TemplateItem

insert @TemplateItem
values ('выполнен вход в ЛК ЭТП по ЭЦП', 1, 19)

insert @TemplateItem
values ('До сотрудников, которые будут подавать ценовые предложения, доведена информация о минимальной цене участия. Сотрудники проинформированы о способах уведомления о ходе аукционного торга, уведомления о пересечении "пороговых" (зеленой, желтой, красной) цен и способах согласования дальнейшего снижения цены.', 2, 20)

insert @TemplateItem
values ('Ответственным сотрудником принято участие в аукционе. Поучаствовать в торгах можно из раздела" Заявки на участие в процедурах".', 3, 21)

merge into [dbo].[TemplateItem] as target
using @TemplateItem as source on target.DicItem_Id = source.DicItem_Id and target.TemplateBlock_Id = @templateBlockId and target.Template_Id = @templateId
when matched then
    update set Description = source.Description, Priority = source.Priority
when not matched then
    insert (RowVersion, Description, Priority, DicItem_Id, TemplateBlock_Id, Template_Id) values (1, source.Description, source.Priority, source.DicItem_Id, @templateBlockId, @templateId);
	

/******/

select top 1 @dicBlockId = Id	
	from [dbo].[DicBlock]
		where Name = 'Заключение контракта';
		
delete @TemplateBlock

insert @TemplateBlock
values ('', 5, @templateId, @dicBlockId)

merge into [dbo].[TemplateBlock] as target
using @TemplateBlock as source on target.Template_Id = source.Template_Id and target.DicBlock_Id = source.DicBlock_Id
when matched then
    update set Description = source.Description, Priority = source.Priority
when not matched then
    insert (RowVersion, Description, Priority, Template_Id, DicBlock_Id) values (1, source.Description, source.Priority, source.Template_Id, source.DicBlock_Id);

select top 1 @templateBlockId = Id
	from [dbo].[TemplateBlock]
		where Template_Id = @templateId and DicBlock_Id = @dicBlockId;

delete @TemplateItem

insert @TemplateItem
values ('В случае решения об обеспечении исполнения с помощью БГ отправлены заявки в банки, получены предложения, лучшее предложение акцептовано и оплачено, БГ выпущена банком, внесена в реестр, копия БГ получена', 1, 22)

insert @TemplateItem
values ('В случае наличия разногласий по тексту договора в ответ на направленный заказчиком текст через ЭТП направлен протокол разногласий', 2, 23)

insert @TemplateItem
values ('Договор подписан электронной подписью на ЭТП', 3, 24)

merge into [dbo].[TemplateItem] as target
using @TemplateItem as source on target.DicItem_Id = source.DicItem_Id and target.TemplateBlock_Id = @templateBlockId and target.Template_Id = @templateId
when matched then
    update set Description = source.Description, Priority = source.Priority
when not matched then
    insert (RowVersion, Description, Priority, DicItem_Id, TemplateBlock_Id, Template_Id) values (1, source.Description, source.Priority, source.DicItem_Id, @templateBlockId, @templateId);