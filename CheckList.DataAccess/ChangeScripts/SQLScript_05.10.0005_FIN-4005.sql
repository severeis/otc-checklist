﻿
if  exists 
(
    select so1.id from sysobjects so1 
    join sysobjects so2 on so1.parent_obj =so2.id
    where so1.xtype = 'F' and so1.name = 'FK_TenderCondition_DicEtp__DicEtp_Id' and so2.Name = 'TenderCondition'
)
begin
   ALTER TABLE [dbo].[TenderCondition] DROP CONSTRAINT [FK_TenderCondition_DicEtp__DicEtp_Id]
end

if  exists 
(
    select so1.id from syscolumns so1 
    join sysobjects so2 on so1.id=so2.id
    where so1.name = 'DicEtp_Id' and so2.Name = 'TenderCondition'
)
begin
   ALTER TABLE [dbo].[TenderCondition] ALTER COLUMN [DicEtp_Id] [int] NULL

end

if not exists 
(
    select so1.id from sysobjects so1 
    join sysobjects so2 on so1.parent_obj =so2.id
    where so1.xtype = 'F' and so1.name = 'FK_TenderCondition_DicEtp__DicEtp_Id' and so2.Name = 'TenderCondition'
)
begin
   ALTER TABLE [dbo].[TenderCondition] ADD CONSTRAINT [FK_TenderCondition_DicEtp__DicEtp_Id] FOREIGN KEY
	(
		[DicEtp_Id]
	)
	REFERENCES [dbo].[DicEtp]
	(
		[Id]
	)
end
