﻿

SET IDENTITY_INSERT [dbo].[FederalLawType] ON

declare @DicItem table(id int, Name nvarchar(255))

insert @DicItem
values(2, '223-ФЗ')

insert @DicItem
values(3, 'Коммерческие')

insert @DicItem
values(4, '44-ФЗ')

insert @DicItem
values(5, '615-ППРФ')

insert @DicItem
values(6, 'Закупки малого объема')


merge into [dbo].[FederalLawType] as target
using @DicItem as source on target.Id = source.Id
when matched then
    update set Name = source.Name
when not matched then
    insert (Id, RowVersion, Name) values (source.Id, 1, source.Name);

SET IDENTITY_INSERT [dbo].[FederalLawType] OFF

GO


SET IDENTITY_INSERT [dbo].[DicPurchaseType] ON

declare @DicItem table(id int, Name nvarchar(255))

insert @DicItem
values(1, 'Запрос предложений')
insert @DicItem
values(2, 'Конкурс')
insert @DicItem
values(3, 'Запрос котировок')
insert @DicItem
values(4, 'Аукцион')
insert @DicItem
values(5, 'Конкурентные переговоры')
insert @DicItem
values(6, 'Редукцион')
insert @DicItem
values(7, 'Закупка у единственного поставщика')
insert @DicItem
values(8, 'Предварительный отбор')
insert @DicItem
values(9, 'Запрос котировок')
insert @DicItem
values(10, 'Твердая котировка')
insert @DicItem
values(11, 'Комплексная закупка (запрос предложений)')
insert @DicItem
values(15, 'Комплексная закупка (запрос цен)')
insert @DicItem
values(16, 'Комплексная закупка (конкурс)')
insert @DicItem
values(17, 'Сбор коммерческих предложений')
insert @DicItem
values(18, 'Закупка с использованием электронного магазина')
insert @DicItem
values(21, 'Услуга размещения рекламы')
insert @DicItem
values(22, 'Конкурентный отбор')
insert @DicItem
values(23, 'Аукцион (попозиционный торг)')
insert @DicItem
values(24, 'Конкурс в электронной форме, участниками которого могут являться только субъекты малого и среднего предпринимательства')
insert @DicItem
values(25, 'Запрос предложений в электронной форме, участниками которого могут являться только субъекты малого и среднего предпринимательства')
insert @DicItem
values(26, 'Запрос котировок в электронной форме, участниками которого могут являться только субъекты малого и среднего предпринимательства')
insert @DicItem
values(27, 'Аукцион в электронной форме, участниками которого могут являться только субъекты малого и среднего предпринимательства')
insert @DicItem
values(1000, 'План закупок')

merge into [dbo].[DicPurchaseType] as target
using @DicItem as source on target.Id = source.Id
when matched then
    update set Name = source.Name
when not matched then
    insert (Id, RowVersion, Name) values (source.Id, 1, source.Name);

SET IDENTITY_INSERT [dbo].[DicPurchaseType] OFF

GO

SET IDENTITY_INSERT [dbo].[DicEtp] ON

declare @DicItem table(id int, Name nvarchar(255))

insert @DicItem
values(1, 'OTC-agro')
insert @DicItem
values(2, 'OTC-tender')
insert @DicItem
values(3, 'rts223.OTC.ru')
insert @DicItem
values(4, 'Реклама')
insert @DicItem
values(5, 'Сбербанк-АСТ')
insert @DicItem
values(6, 'ЭТП ММВБ')
insert @DicItem
values(7, 'АГЗ РТ')
insert @DicItem
values(8, 'РТС-тендер')
insert @DicItem
values(9, 'ЕЭТП')
insert @DicItem
values(10, 'Вне электронных площадок')
insert @DicItem
values(11, 'OTC-market')
insert @DicItem
values(12, 'B2B-Center')
insert @DicItem
values(13, 'ТП Фабрикант')
insert @DicItem
values(14, 'ЭТЗП ОАО РЖД')
insert @DicItem
values(15, 'ГазНефтеторг.ру')
insert @DicItem
values(16, 'ЭТП ГПБ')
insert @DicItem
values(17, 'ЭТП ТОРГИ223')
insert @DicItem
values(18, 'Прочие площадки')
insert @DicItem
values(19, 'STG')
insert @DicItem
values(20, 'Halyksauda')
insert @DicItem
values(21, 'Планирование')
insert @DicItem
values(22, 'АКД')
insert @DicItem
values(23, 'ONLINECONTRACT')
insert @DicItem
values(24, 'Роснефть')
insert @DicItem
values(25, 'ЕСТП')
insert @DicItem
values(26, 'СИБУР')
insert @DicItem
values(27, 'Тендер Про')
insert @DicItem
values(28, 'ETPRF.ru')
insert @DicItem
values(29, 'MMK')
insert @DicItem
values(30, 'ЭТП НЭП')
insert @DicItem
values(31, 'РАД')

merge into [dbo].[DicEtp] as target
using @DicItem as source on target.Id = source.Id
when matched then
    update set Name = source.Name
when not matched then
    insert (Id, RowVersion, Name) values (source.Id, 1, source.Name);

SET IDENTITY_INSERT [dbo].[DicEtp] OFF

GO