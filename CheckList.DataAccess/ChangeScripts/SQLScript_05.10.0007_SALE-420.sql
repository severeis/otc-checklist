
if not exists 
(
    select so1.id from sysobjects so1 
    where so1.type in ('U', 'TT') and so1.name = 'TradePlanCondition'
)
begin
   CREATE TABLE [dbo].[TradePlanCondition]
(
	[Id] [int] IDENTITY (1,1) NOT NULL,
	[RowVersion] [int] NOT NULL,
	[FederalLawType_Id] [int] NULL,
	[Template_Id] [int] NOT NULL,
	CONSTRAINT [PK_TradePlanCondition] PRIMARY KEY CLUSTERED
	(
		[Id] ASC
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY  = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


if not exists 
(
    select so1.id from sysobjects so1 
    join sysobjects so2 on so1.parent_obj =so2.id
    where so1.xtype = 'F' and so1.name = 'FK_TradePlanCondition_FederalLawType__FederalLawType_Id' and so2.Name = 'TradePlanCondition'
)
begin
   ALTER TABLE [dbo].[TradePlanCondition] ADD CONSTRAINT [FK_TradePlanCondition_FederalLawType__FederalLawType_Id] FOREIGN KEY
	(
		[FederalLawType_Id]
	)
	REFERENCES [dbo].[FederalLawType]
	(
		[Id]
	)
end


if not exists 
(
    select so1.id from sysobjects so1 
    join sysobjects so2 on so1.parent_obj =so2.id
    where so1.xtype = 'F' and so1.name = 'FK_TradePlanCondition_Template__Template_Id' and so2.Name = 'TradePlanCondition'
)
begin
   ALTER TABLE [dbo].[TradePlanCondition] ADD CONSTRAINT [FK_TradePlanCondition_Template__Template_Id] FOREIGN KEY
	(
		[Template_Id]
	)
	REFERENCES [dbo].[Template]
	(
		[Id]
	)
end


end
