declare @DicCondition table(id int, Name nvarchar(255))

insert @DicCondition
values(2, 'TradePlan')

SET IDENTITY_INSERT [dbo].[DicCondition] ON

merge into [dbo].[DicCondition] as target
using @DicCondition as source on target.Id = source.Id
when matched then
    update set Name = source.Name
when not matched then
    insert (Id, RowVersion, Name) values (source.Id, 1, source.Name);

SET IDENTITY_INSERT [dbo].[DicCondition] OFF
GO
