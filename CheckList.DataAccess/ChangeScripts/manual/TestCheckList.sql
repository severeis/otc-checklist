
declare @templateId int;
declare @Checkist table(Id int);
declare @checklistId int;

select top 1 @templateId = Id
	from [dbo].Template
		where Name = '������ ������ ��� ������� � ��';

insert into [dbo].[CheckList] (RowVersion, CreateDate, OrderId, Template_Id)
output inserted.Id  into @Checkist
	values (1, GetDate(), 1, @templateId);

select top 1 @checklistId = Id from @Checkist;


DECLARE @templateItemId int

DECLARE MY_CURSOR CURSOR 
  LOCAL STATIC READ_ONLY FORWARD_ONLY
FOR 
SELECT DISTINCT Id 
FROM [dbo].[TemplateItem]
	WHERE Template_Id = @templateId

OPEN MY_CURSOR
FETCH NEXT FROM MY_CURSOR INTO @templateItemId
WHILE @@FETCH_STATUS = 0
BEGIN 

	insert into [dbo].[Item] (RowVersion, CheckList_Id, TemplateItem_Id)
	values (1, @checklistId, @templateItemId);

    FETCH NEXT FROM MY_CURSOR INTO @templateItemId
END
CLOSE MY_CURSOR
DEALLOCATE MY_CURSOR