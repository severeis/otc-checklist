﻿using BaseWebAPI.Infrastructure.NHibernate;
using BaseWebAPI.Infrastructure.NHibernate.Configuration;
using NHibernate.Cfg;
using System.IO;

namespace CheckList.DataAccess
{
    public static class CheckListDB
    {
        /// <summary>
        /// Инициализация NHibernate.
        /// </summary>
        /// <param name="binFolder">Путь к папке bin.</param>
        /// <param name="configFolder">Конфигурационный файл.</param>
        /// <returns>Конфигурация Nhibernate.</returns>
        public static Configuration InitializeNHibernateWithConfigFolder(string binFolder, string configFolder)
        {
            return InitializeNHibernate(binFolder, Path.Combine(configFolder, "FinScoring.NHibernate.config"));
        }

        /// <summary>
        /// Инициализация NHibernate.
        /// </summary>
        /// <param name="binFolder">Путь к папке bin.</param>
        /// <param name="configFile">Наименование конфигурационного файла.</param>
        /// <returns>Конфигурация.</returns>
        public static Configuration InitializeNHibernate(string binFolder, string configFile)
        {
            var cfg = NHibernateInitializer.CreateConfiguration(
                configFile,
                new[] { Path.Combine(binFolder, typeof(CheckListDB).Assembly.ManifestModule.ScopeName) },
                new CheckListPersistenceModelGenerator().Generate(),
#if USE_POSTGRESQL
                new PostgreSqlNamingStrategy()
#else
                null
#endif
                );

            InitializeEnvers(cfg);

            //NHibernateSession.AddConfiguration(NHibernateSession.DefaultFactoryKey, cfg.BuildSessionFactory(), cfg, null);

            SqlDialectTransform.InitializeDialectTransform(
                cfg.Properties.ContainsKey("dialect")
                    ? cfg.Properties["dialect"]
                    : "NHibernate.Dialect.MsSql2008Dialect");

            return cfg;
        }

        /// <summary>
        /// Инициализация AUD.
        /// </summary>
        /// <param name="cfg">Конфигурация.</param>
        private static void InitializeEnvers(Configuration cfg)
        {
            var envConf = new NHibernate.Envers.Configuration.Fluent.FluentConfiguration();

            ////TODO Finance: Создание схемы для истории в SchemaGenerator
            ////cfg.Properties.Add("nhibernate.envers.default_schema", "History");
            cfg.Properties.Add("nhibernate.envers.store_data_at_delete", "True");

            cfg.IntegrateWithEnvers(envConf);
        }
    }
}
