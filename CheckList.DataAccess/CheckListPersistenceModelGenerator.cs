﻿using BaseWebAPI.Infrastructure.NHibernate.Configuration;
using CheckList.DataAccess.Mappings;
using FluentNHibernate.Automapping;

namespace CheckList.DataAccess
{
    /// <summary>
    /// Класс для запуска mapping всей БД.
    /// </summary>
    public class CheckListPersistenceModelGenerator //: IAutoPersistenceModelGenerator
    {
        /// <summary>
        /// Сгенерировать.
        /// </summary>
        /// <returns>Сгенерированная модель.</returns>
        public AutoPersistenceModel Generate()
        {
            return AutoPersistenceModelGenerator.Generate<Entities.SchemaChangeLog, SchemaChangeLogMap>();
        }
    }
}
