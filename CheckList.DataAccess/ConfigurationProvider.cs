﻿using CheckList.DataAccess.Entities;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate.Cfg;
using SchemaGeneration;
using System;
using System.Collections.Generic;
using System.Text;

namespace CheckList.DataAccess
{
    /// <summary>
    /// Конфигуратор NHibernate (для SchemaGeneratorTool).
    /// </summary>
    public class ConfigurationProvider : IConfigurationProvider
    {
        /// <summary>
        /// Получить конфигурацию.
        /// </summary>
        /// <param name="binFolder">Расположение папки bin.</param>
        /// <param name="configFile">Наименование конфигурационного файла.</param>
        /// <returns>Конфигурация.</returns>
        public Configuration GetConfiguration(string binFolder, string configFile)
        {
            return CheckListDB.InitializeNHibernate(binFolder, configFile);
        }
    }
}
