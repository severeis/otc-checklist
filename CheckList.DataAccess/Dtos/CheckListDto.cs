﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CheckList.DataAccess.Dtos
{
    /// <summary>
    /// Чек лист.
    /// </summary>
    public class CheckListDto
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        public CheckListDto()
        {
            Items = new List<ItemDto>();
        }

        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Дата создания.
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Идентификатор заказа.
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// Шаблон чек листа.
        /// </summary>
        public TemplateDto Template { get; set; }

        /// <summary>
        /// Список элементов.
        /// </summary>
        public List<ItemDto> Items { get; set; }

        /// <summary>
        /// Маппинг.
        /// </summary>
        /// <param name="entity">Entity.</param>
        public static implicit operator CheckListDto(Entities.CheckList entity)
        {
            if (entity == null)
            {
                return null;
            }

            return new CheckListDto
            {
                Id = entity.Id,
                OrderId = entity.OrderId,
                CreateDate = entity.CreateDate,
                Items = entity.Items?.Select(s => (ItemDto)s).ToList() ?? new List<ItemDto>(),
                Template = entity.Template,
            };
        }
    }
}
