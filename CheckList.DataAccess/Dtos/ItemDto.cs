﻿using System;

namespace CheckList.DataAccess.Dtos
{
    /// <summary>
    /// Элемент чек листа.
    /// </summary>
    public class ItemDto
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Идентификатор чек листа.
        /// </summary>
        public int? CheckListId { get; set; }

        /// <summary>
        /// Элемент шаблона чек листа.
        /// </summary>
        public int? TemplateItemId { get; set; }

        /// <summary>
        /// Дата завершения.
        /// </summary>
        public DateTime? CheckDate { get; set; }

        /// <summary>
        /// Маппинг.
        /// </summary>
        /// <param name="entity">Entity.</param>
        public static implicit operator ItemDto(Entities.Item entity)
        {
            if (entity == null)
            {
                return null;
            }

            return new ItemDto
            {
                Id = entity.Id,
                CheckDate = entity.CheckDate,
                CheckListId = entity.CheckList?.Id,
                TemplateItemId = entity.TemplateItem?.Id,
            };
        }
    }
}
