﻿using System.Collections.Generic;
using System.Linq;

namespace CheckList.DataAccess.Dtos
{
    /// <summary>
    /// Шаблон блока чек листа.
    /// </summary>
    public class TemplateBlockDto
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Идентификатор блока.
        /// </summary>
        public int? DicBlockId { get; set; }

        /// <summary>
        /// Название блока.
        /// </summary>
        public string DicBlockName { get; set; }

        /// <summary>
        /// Описание.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Идентификатор вышестоящей группы.
        /// </summary>
        public int? ParentBlockId { get; set; }

        /// <summary>
        /// Приоритет.
        /// </summary>
        public int Priority { get; set; }

        /// <summary>
        /// Шаблон чек листа.
        /// </summary>
        public int? TemplateId { get; set; }

        /// <summary>
        /// Список элементов.
        /// </summary>
        public List<TemplateItemDto> TemplateItems { get; set; }

        /// <summary>
        /// Маппинг.
        /// </summary>
        /// <param name="entity">Entity.</param>
        public static implicit operator TemplateBlockDto(Entities.TemplateBlock entity)
        {
            if (entity == null)
            {
                return null;
            }

            return new TemplateBlockDto
            {
                Id = entity.Id,
                Description = entity.Description,
                DicBlockId = entity.DicBlock?.Id,
                DicBlockName = entity.DicBlock?.Name,
                ParentBlockId = entity.ParentBlockId,
                Priority = entity.Priority,
                TemplateId = entity.Template?.Id,
                TemplateItems = entity.TemplateItems?.Select(s => (TemplateItemDto)s).ToList()
            };
        }
    }
}
