﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CheckList.DataAccess.Dtos
{
    /// <summary>
    /// Шаблон чек листа.
    /// </summary>
    public class TemplateDto
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        public TemplateDto()
        {
            TemplateItems = new List<TemplateItemDto>();
            TemplateBlocks = new List<TemplateBlockDto>();
        }

        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название шаблона.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Признак актуальной записи.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Дата создания.
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Список элементов.
        /// </summary>
        public List<TemplateItemDto> TemplateItems { get; set; }

        /// <summary>
        /// Список блоков.
        /// </summary>
        public List<TemplateBlockDto> TemplateBlocks { get; set; }

        /// <summary>
        /// Словарь условий.
        /// </summary>
        public int? DicConditionId { get; set; }

        /// <summary>
        /// Наименование.
        /// </summary>
        public string DicConditionName { get; set; }

        /// <summary>
        /// Маппинг.
        /// </summary>
        /// <param name="entity">Entity.</param>
        public static implicit operator TemplateDto(Entities.Template entity)
        {
            if (entity == null)
            {
                return null;
            }

            return new TemplateDto
            {
                Id = entity.Id,
                CreateDate = entity.CreateDate,
                DicConditionId = entity.DicCondition?.Id,
                DicConditionName = entity.DicCondition?.Name,
                IsActive = entity.IsActive,
                Name = entity.Name,
                TemplateBlocks = entity.TemplateBlocks?.Select(s => (TemplateBlockDto)s).ToList(),
                TemplateItems = entity.TemplateItems?.Select(s => (TemplateItemDto)s).ToList()
            };
        }
    }
}
