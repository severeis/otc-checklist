﻿namespace CheckList.DataAccess.Dtos
{
    /// <summary>
    /// Элемент шаблона чек листа.
    /// </summary>
    public class TemplateItemDto
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Идентификатор элемента шаблона чек листа.
        /// </summary>
        public int? DicItemId { get; set; }

        /// <summary>
        /// Название элемента шаблона чек листа.
        /// </summary>
        public string DicItemName { get; set; }

        /// <summary>
        /// Описание.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Шаблон блока чек листа.
        /// </summary>
        public int? TemplateBlockId { get; set; }

        /// <summary>
        /// Приоритет.
        /// </summary>
        public int Priority { get; set; }

        /// <summary>
        /// Шаблон чек листа.
        /// </summary>
        public int? TemplateId { get; set; }

        /// <summary>
        /// Маппинг.
        /// </summary>
        /// <param name="entity">Entity.</param>
        public static implicit operator TemplateItemDto(Entities.TemplateItem entity)
        {
            if (entity == null)
            {
                return null;
            }

            return new TemplateItemDto
            {
                Id = entity.Id,
                Description = entity.Description,
                DicItemId = entity.DicItem?.Id,
                DicItemName = entity.DicItem?.Name,
                Priority = entity.Priority,
                TemplateBlockId = entity.TemplateBlock?.Id,
                TemplateId = entity.Template?.Id
            };
        }
    }
}
