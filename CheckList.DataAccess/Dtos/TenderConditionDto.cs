﻿namespace CheckList.DataAccess.Dtos
{
    /// <summary>
    /// Условие для подбора шаблона тендера.
    /// </summary>
    public class TenderConditionDto
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Идентификатор способа закупки.
        /// </summary>
        public int? PurchaseTypeId { get; set; }

        /// <summary>
        /// Название способа закупки.
        /// </summary>
        public string PurchaseTypeName { get; set; }

        /// <summary>
        /// Закон по которому проводится тендер.
        /// </summary>
        public int? FederalLawTypeId { get; set; }

        /// <summary>
        /// Закон по которому проводится тендер.
        /// </summary>
        public string FederalLawTypeName { get; set; }

        /// <summary>
        /// Словарь ЭТП.
        /// </summary>
        public int? DicEtpId { get; set; }

        /// <summary>
        /// Словарь ЭТП.
        /// </summary>
        public string DicEtpName { get; set; }

        /// <summary>
        /// Шаблон чек листа.
        /// </summary>
        public int TemplateId { get; set; }

        /// <summary>
        /// Маппинг.
        /// </summary>
        /// <param name="entity">Entity.</param>
        public static implicit operator TenderConditionDto(Entities.TenderCondition entity)
        {
            if (entity == null)
            {
                return null;
            }

            return new TenderConditionDto
            {
                Id = entity.Id,
                PurchaseTypeId = entity.PurchaseType?.Id,
                PurchaseTypeName = entity.PurchaseType?.Name,
                FederalLawTypeId = entity.FederalLawType?.Id,
                FederalLawTypeName = entity.FederalLawType?.Name,
                DicEtpId = entity.DicEtp?.Id,
                DicEtpName = entity.DicEtp?.Name,
                TemplateId = entity.Template.Id
            };
        }
    }
}
