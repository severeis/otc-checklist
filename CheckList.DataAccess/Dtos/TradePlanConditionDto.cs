﻿namespace CheckList.DataAccess.Dtos
{
    /// <summary>
    /// Условие для отбора шаблона чеклиста для плана закупок.
    /// </summary>
    public class TradePlanConditionDto
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Закон по которому проводится тендер.
        /// </summary>
        public int? FederalLawTypeId { get; set; }

        /// <summary>
        /// Закон по которому проводится тендер.
        /// </summary>
        public string FederalLawTypeName { get; set; }

        /// <summary>
        /// Шаблон чек листа.
        /// </summary>
        public int TemplateId { get; set; }

        /// <summary>
        /// Маппинг.
        /// </summary>
        /// <param name="entity">Entity.</param>
        public static implicit operator TradePlanConditionDto(Entities.TradePlanCondition entity)
        {
            if (entity == null)
            {
                return null;
            }

            return new TradePlanConditionDto
            {
                Id = entity.Id,
                FederalLawTypeId = entity.FederalLawType?.Id,
                FederalLawTypeName = entity.FederalLawType?.Name,
                TemplateId = entity.Template.Id
            };
        }
    }
}
