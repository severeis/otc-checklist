﻿using BaseWebAPI.Common.Domain;
using System;
using System.Collections.Generic;

namespace CheckList.DataAccess.Entities
{
    /// <summary>
    /// Чек лист.
    /// </summary>
    public class CheckList : EntityBase
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        public CheckList()
        {
            Items = new HashSet<Item>();
        }

        /// <summary>
        /// Дата создания.
        /// </summary>
        public virtual DateTime CreateDate { get; set; }

        /// <summary>
        /// Идентификатор заказа.
        /// </summary>
        public virtual int OrderId { get; set; }

        /// <summary>
        /// Шаблон чек листа.
        /// </summary>
        public virtual Template Template { get; set; }

        /// <summary>
        /// Список элементов.
        /// </summary>
        public virtual ISet<Item> Items { get; set; }
    }
}
