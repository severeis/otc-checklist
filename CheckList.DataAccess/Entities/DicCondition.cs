﻿using BaseWebAPI.Common.Domain;

namespace CheckList.DataAccess.Entities
{
    /// <summary>
    /// Словарь условий.
    /// </summary>
    public class DicCondition : DictionaryBase
    {
        /// <summary>
        /// Code.
        /// </summary>
        public virtual string Code { get; set; }
    }
}
