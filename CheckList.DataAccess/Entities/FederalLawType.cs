﻿using BaseWebAPI.Common.Domain;

namespace CheckList.DataAccess.Entities
{
    /// <summary>
    /// Закон по которому проводится тендер.
    /// 44 - 44-ФЗ
    /// 223 - 223-ФЗ
    /// </summary>
    public class FederalLawType : EntityBase
    {
        /// <summary>
        /// Название.
        /// </summary>
        public virtual string Name { get; set; }
    }
}
