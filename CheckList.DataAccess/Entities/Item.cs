﻿using BaseWebAPI.Common.Domain;
using System;

namespace CheckList.DataAccess.Entities
{
    /// <summary>
    /// Элемент чек листа.
    /// </summary>
    public class Item : EntityBase
    {
        /// <summary>
        /// Идентификатор чек листа.
        /// </summary>
        public virtual CheckList CheckList { get; set; }

        /// <summary>
        /// Элемент шаблона чек листа.
        /// </summary>
        public virtual TemplateItem TemplateItem { get; set; }

        /// <summary>
        /// Дата завершения.
        /// </summary>
        public virtual DateTime? CheckDate { get; set; }
    }
}
