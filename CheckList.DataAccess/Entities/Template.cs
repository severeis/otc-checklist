﻿using BaseWebAPI.Common.Domain;
using System;
using System.Collections.Generic;

namespace CheckList.DataAccess.Entities
{
    /// <summary>
    /// Шаблон чек листа.
    /// </summary>
    public class Template : EntityBase
    {
        /// <summary>
        /// Название шаблона.
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Признак актуальной записи.
        /// </summary>
        public virtual bool IsActive { get; set; }

        /// <summary>
        /// Дата создания.
        /// </summary>
        public virtual DateTime CreateDate { get; set; }

        /// <summary>
        /// Список элементов.
        /// </summary>
        public virtual ISet<TemplateItem> TemplateItems { get; set; }

        /// <summary>
        /// Список блоков.
        /// </summary>
        public virtual ISet<TemplateBlock> TemplateBlocks { get; set; }

        /// <summary>
        /// Словарь условий.
        /// </summary>
        public virtual DicCondition DicCondition { get; set; }
    }
}
