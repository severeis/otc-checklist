﻿using BaseWebAPI.Common.Domain;
using System.Collections.Generic;

namespace CheckList.DataAccess.Entities
{
    /// <summary>
    /// Шаблон блока чек листа.
    /// </summary>
    public class TemplateBlock : EntityBase
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        public TemplateBlock()
        {
            TemplateItems = new HashSet<TemplateItem>();
        }

        /// <summary>
        /// Словарь блока.
        /// </summary>
        public virtual DicBlock DicBlock { get; set; }

        /// <summary>
        /// Описание.
        /// </summary>
        public virtual string Description { get; set; }

        /// <summary>
        /// Идентификатор вышестоящей группы.
        /// </summary>
        public virtual int? ParentBlockId { get; set; }

        /// <summary>
        /// Приоритет.
        /// </summary>
        public virtual int Priority { get; set; }

        /// <summary>
        /// Шаблон чек листа.
        /// </summary>
        public virtual Template Template { get; set; }

        /// <summary>
        /// Список элементов.
        /// </summary>
        public virtual ISet<TemplateItem> TemplateItems { get; set; }
    }
}
