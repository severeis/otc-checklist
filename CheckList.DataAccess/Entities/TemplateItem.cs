﻿using BaseWebAPI.Common.Domain;

namespace CheckList.DataAccess.Entities
{
    /// <summary>
    /// Элемент шаблона чек листа.
    /// </summary>
    public class TemplateItem : EntityBase
    {
        /// <summary>
        /// Словарь элемента шаблона чек листа.
        /// </summary>
        public virtual DicItem DicItem { get; set; }

        /// <summary>
        /// Описание.
        /// </summary>
        public virtual string Description { get; set; }

        /// <summary>
        /// Шаблон блока чек листа.
        /// </summary>
        public virtual TemplateBlock TemplateBlock { get; set; }

        /// <summary>
        /// Приоритет.
        /// </summary>
        public virtual int Priority { get; set; }

        /// <summary>
        /// Шаблон чек листа.
        /// </summary>
        public virtual Template Template { get; set; }
    }
}
