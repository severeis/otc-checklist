﻿using BaseWebAPI.Common.Domain;

namespace CheckList.DataAccess.Entities
{
    /// <summary>
    /// Шаблон тендера.
    /// </summary>
    public class TenderCondition : EntityBase
    {
        /// <summary>
        /// Словарь способ закупки.
        /// </summary>
        public virtual DicPurchaseType PurchaseType { get; set; }

        /// <summary>
        /// Закон по которому проводится тендер.
        /// 44 - 44-ФЗ
        /// 223 - 223-ФЗ
        /// </summary>
        public virtual FederalLawType FederalLawType { get; set; }

        /// <summary>
        /// Словарь ЭТП.
        /// </summary>
        public virtual DicEtp DicEtp { get; set; }

        /// <summary>
        /// Шаблон чек листа.
        /// </summary>
        public virtual Template Template { get; set; }
    }
}
