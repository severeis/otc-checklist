﻿using BaseWebAPI.Common.Domain;

namespace CheckList.DataAccess.Entities
{
    /// <summary>
    /// Условие для отбора шаблона чеклиста для плана закупок.
    /// </summary>
    public class TradePlanCondition : EntityBase
    {
        /// <summary>
        /// Закон по которому проводится тендер.
        /// 44 - 44-ФЗ
        /// 223 - 223-ФЗ
        /// </summary>
        public virtual FederalLawType FederalLawType { get; set; }

        /// <summary>
        /// Шаблон чек листа.
        /// </summary>
        public virtual Template Template { get; set; }
    }
}
