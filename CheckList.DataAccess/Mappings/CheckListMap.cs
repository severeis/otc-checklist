﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace CheckList.DataAccess.Mappings
{
    /// <summary>
    /// Маппинг таблицы.
    /// </summary>
    public class CheckListMap : IAutoMappingOverride<Entities.CheckList>
    {
        /// <summary>
        /// Маппинг колонок.
        /// </summary>
        public void Override(AutoMapping<Entities.CheckList> mapping)
        {
            mapping.Id(x => x.Id);
            mapping.Map(x => x.CreateDate).Not.Nullable();
            mapping.Map(x => x.OrderId).Not.Nullable();

            mapping.References(x => x.Template).Class<Entities.Template>().Cascade.None().Not.Nullable().Column("Template_Id");

            mapping.HasMany(x => x.Items).AsSet().Cascade.SaveUpdate();
        }
    }
}
