﻿using CheckList.DataAccess.Entities;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace CheckList.DataAccess.Mappings
{
    /// <summary>
    /// Маппинг таблицы.
    /// </summary>
    public class DicBlockMap : IAutoMappingOverride<DicBlock>
    {
        /// <summary>
        /// Маппинг колонок.
        /// </summary>
        public void Override(AutoMapping<DicBlock> mapping)
        {
            mapping.Id(x => x.Id);
            mapping.Map(x => x.Name).Length(255).Not.Nullable();
        }
    }
}
