﻿using CheckList.DataAccess.Entities;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace CheckList.DataAccess.Mappings
{
    /// <summary>
    /// Маппинг таблицы.
    /// </summary>
    public class DicEtpMap : IAutoMappingOverride<DicEtp>
    {
        /// <summary>
        /// Маппинг колонок.
        /// </summary>
        public void Override(AutoMapping<DicEtp> mapping)
        {
            mapping.Id(x => x.Id);
            mapping.Map(x => x.Name).Length(255).Not.Nullable();
        }
    }
}
