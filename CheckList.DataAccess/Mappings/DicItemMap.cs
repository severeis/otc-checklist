﻿using CheckList.DataAccess.Entities;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace CheckList.DataAccess.Mappings
{
    /// <summary>
    /// Маппинг таблицы.
    /// </summary>
    public class DicItemMap : IAutoMappingOverride<DicItem>
    {
        /// <summary>
        /// Маппинг колонок.
        /// </summary>
        public void Override(AutoMapping<DicItem> mapping)
        {
            mapping.Id(x => x.Id);
            mapping.Map(x => x.Name).Length(255).Not.Nullable();
        }
    }
}
