﻿using CheckList.DataAccess.Entities;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace CheckList.DataAccess.Mappings
{
    /// <summary>
    /// Маппинг таблицы.
    /// </summary>
    public class FederalLawTypeMap : IAutoMappingOverride<FederalLawType>
    {
        /// <summary>
        /// Маппинг колонок.
        /// </summary>
        public void Override(AutoMapping<FederalLawType> mapping)
        {
            mapping.Id(x => x.Id);
            mapping.Map(x => x.Name).Length(255).Not.Nullable();
        }
    }
}
