﻿using CheckList.DataAccess.Entities;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace CheckList.DataAccess.Mappings
{
    /// <summary>
    /// Маппинг таблицы.
    /// </summary>
    public class ItemMap : IAutoMappingOverride<Item>
    {
        /// <summary>
        /// Маппинг колонок.
        /// </summary>
        public void Override(AutoMapping<Item> mapping)
        {
            mapping.Id(x => x.Id);
            mapping.Map(x => x.CheckDate).Nullable();

            mapping.References<Entities.CheckList>(x => x.CheckList).Cascade.None().Not.Nullable().Column("CheckList_Id");
            mapping.References<Entities.TemplateItem>(x => x.TemplateItem).Cascade.None().Not.Nullable().Column("TemplateItem_Id");
        }
    }
}
