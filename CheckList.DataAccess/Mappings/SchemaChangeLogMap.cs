﻿using BaseWebAPI.Common.Domain.Data.Mapping;
using CheckList.DataAccess.Entities;

namespace CheckList.DataAccess.Mappings
{
    /// <summary>
    /// Mapping таблицы наката скриптов.
    /// </summary>
    public class SchemaChangeLogMap : SchemaChangeLogBaseMap<SchemaChangeLog>
    {
    }
}
