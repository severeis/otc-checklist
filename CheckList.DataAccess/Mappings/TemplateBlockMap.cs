﻿using CheckList.DataAccess.Entities;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace CheckList.DataAccess.Mappings
{
    /// <summary>
    /// Маппинг таблицы.
    /// </summary>
    public class TemplateBlockMap : IAutoMappingOverride<TemplateBlock>
    {
        /// <summary>
        /// Маппинг колонок.
        /// </summary>
        public void Override(AutoMapping<TemplateBlock> mapping)
        {
            mapping.Id(x => x.Id);

            mapping.Map(x => x.Description).Length(5000).Nullable();
            mapping.Map(x => x.ParentBlockId).Nullable();
            mapping.Map(x => x.Priority).Not.Nullable();

            mapping.References<Entities.Template>(x => x.Template).Cascade.None().Not.Nullable().Column("Template_Id");
            mapping.References<Entities.DicBlock>(x => x.DicBlock).Cascade.None().Not.Nullable().Column("DicBlock_Id");

            mapping.HasMany<Entities.TemplateItem>(x => x.TemplateItems).AsSet().Cascade.SaveUpdate();
        }
    }
}
