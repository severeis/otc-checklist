﻿using CheckList.DataAccess.Entities;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace CheckList.DataAccess.Mappings
{
    /// <summary>
    /// Маппинг таблицы.
    /// </summary>
    public class TemplateItemMap : IAutoMappingOverride<TemplateItem>
    {
        /// <summary>
        /// Маппинг колонок.
        /// </summary>
        public void Override(AutoMapping<TemplateItem> mapping)
        {
            mapping.Id(x => x.Id);
            mapping.Map(x => x.Description).Length(5000);
            mapping.Map(x => x.Priority).Not.Nullable();

            mapping.References<Entities.DicItem>(x => x.DicItem).Not.Nullable().Column("DicItem_Id");
            mapping.References<Entities.TemplateBlock>(x => x.TemplateBlock).Column("TemplateBlock_Id");
            mapping.References<Entities.Template>(x => x.Template).Column("Template_Id");
        }
    }
}
