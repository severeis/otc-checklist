﻿using CheckList.DataAccess.Entities;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace CheckList.DataAccess.Mappings
{
    /// <summary>
    /// Маппинг таблицы.
    /// </summary>
    public class TemplateMap : IAutoMappingOverride<Template>
    {
        /// <summary>
        /// Маппинг колонок.
        /// </summary>
        public void Override(AutoMapping<Template> mapping)
        {
            mapping.Id(x => x.Id);
            mapping.Map(x => x.Name).Length(1024).Not.Nullable();
            mapping.Map(x => x.IsActive).Not.Nullable();
            mapping.Map(x => x.CreateDate).Not.Nullable();

            mapping.References<Entities.DicCondition>(x => x.DicCondition).Cascade.None().Not.Nullable().Column("DicCondition_Id");

            mapping.HasMany(x => x.TemplateItems).AsSet().Cascade.SaveUpdate();
            mapping.HasMany(x => x.TemplateBlocks).AsSet().Cascade.SaveUpdate();
        }
    }
}
