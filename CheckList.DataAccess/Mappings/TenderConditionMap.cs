﻿using CheckList.DataAccess.Entities;

using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace CheckList.DataAccess.Mappings
{
    public class TenderConditionMap : IAutoMappingOverride<TenderCondition>
    {
        /// <summary>
        /// Маппинг колонок.
        /// </summary>
        public void Override(AutoMapping<TenderCondition> mapping)
        {
            mapping.Id(x => x.Id);

            mapping.References(x => x.PurchaseType).Cascade.None().Column("DicPurchaseType_Id");
            mapping.References(x => x.FederalLawType).Cascade.None().Column("FederalLawType_Id");
            mapping.References(x => x.DicEtp).Cascade.None().Nullable().Column("DicEtp_Id");
            mapping.References(x => x.Template).Cascade.None().Not.Nullable().Column("Template_Id");
        }
    }
}
