﻿using CheckList.DataAccess.Entities;

using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace CheckList.DataAccess.Mappings
{
    public class TradePlanConditionMap : IAutoMappingOverride<TradePlanCondition>
    {
        /// <summary>
        /// Маппинг колонок.
        /// </summary>
        public void Override(AutoMapping<TradePlanCondition> mapping)
        {
            mapping.Id(x => x.Id);

            mapping.References(x => x.FederalLawType).Cascade.None().Column("FederalLawType_Id");
            mapping.References(x => x.Template).Cascade.None().Not.Nullable().Column("Template_Id");
        }
    }
}
