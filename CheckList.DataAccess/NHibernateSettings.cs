﻿namespace CheckList.DataAccess
{
    /// <summary>
    /// Настройки NHibernate.
    /// </summary>
    public class NHibernateSettings
    {
        /// <summary>
        /// Папка с конфигами NHibernate.
        /// </summary>
        public string NHibernateBinFolder { get; set; }

        /// <summary>
        /// Файл конфигурации NHibernate.
        /// </summary>
        public string NHibernateConfigFile { get; set; }
    }
}
