﻿using CheckList.DataAccess.RepositoryInterfaces;
using NHibernate;
using System.Threading;
using System.Threading.Tasks;

namespace CheckList.DataAccess.Repositories
{
    /// <summary>
    /// Базовый репозиторий.
    /// </summary>
    public abstract class BaseRepository<T> : IBaseRepository<T>
    {
        /// <summary>
        /// Сессия.
        /// </summary>
        protected readonly ISession _session;

        /// <summary>
        /// Ctors.
        /// </summary>
        /// <param name="session">Сессия.</param>
        public BaseRepository(ISession session)
        {
            _session = session;
        }

        /// <summary>
        /// Either Save() or Update() the given instance, depending upon the value of its identifier property.
        /// </summary>
        /// <param name="obj">A transient instance containing new or updated state</param>
        /// <param name="cancellationToken">A cancellation token that can be used to cancel the work</param>
        /// <remarks>By default the instance is always saved. This behaviour may be adjusted by specifying 
        /// an unsaved-value attribute of the identifier property mapping</remarks>
        /// <returns>Task.</returns>
        public async Task SaveOrUpdateAsync(T obj, CancellationToken cancellationToken = default(CancellationToken))
        {
            await _session.SaveOrUpdateAsync(obj, cancellationToken);
        }
    }
}
