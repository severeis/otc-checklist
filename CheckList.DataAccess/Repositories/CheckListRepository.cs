﻿using CheckList.DataAccess.Dtos;
using CheckList.DataAccess.RepositoryInterfaces;
using NHibernate;
using NHibernate.Linq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CheckList.DataAccess.Repositories
{
    /// <summary>
    /// Репозиторий чек листов.
    /// </summary>
    public class CheckListRepository : BaseRepository<Entities.CheckList>, ICheckListRepository
    {
        /// <summary>
        /// Ctors.
        /// </summary>
        /// <param name="session">Сессия.</param>
        public CheckListRepository(ISession session) : base(session)
        {
        }

        /// <summary>
        /// Получить чек лист.
        /// </summary>
        /// <param name="id">Идентификатор чек листа.</param>
        /// <returns>Чек лист.</returns>
        public async Task<CheckListDto> GetAsync(int id)
        {
            var checkList = await _session.Query<Entities.CheckList>()
                .Where(w => w.Id == id)
                .FetchMany(f => f.Items)
                    .ThenFetch(f => f.TemplateItem)
                .ToListAsync();

            return checkList?.FirstOrDefault();
        }

        /// <summary>
        /// Получить чек лист.
        /// </summary>
        /// <param name="orderId">Идентификатор заказа.</param>
        /// <param name="templateId">Идентификатор шаблона чек листа.</param>
        /// <returns>Чек лист.</returns>
        public async Task<CheckListDto> GetByOrderAsync(int orderId, int templateId)
        {
            var checkList = await _session.Query<Entities.CheckList>()
                .Where(w => w.OrderId == orderId)
                .Where(w => w.Template.Id == templateId)
                .FetchMany(f => f.Items)
                    .ThenFetch(f => f.TemplateItem)
                .ToListAsync();

            return checkList?.FirstOrDefault();
        }

        /// <summary>
        /// Создать чек лист.
        /// </summary>
        /// <param name="dto">Чек лист.</param>
        /// <param name="templateId">Идентификатор шаблона чек листа.</param>
        /// <returns>Чек лист.</returns>
        public async Task<CheckListDto> Create(CheckListDto dto, int templateId)
        {
            var template = await _session.LoadAsync<Entities.Template>(templateId);

            var checkList = new Entities.CheckList
            {
                CreateDate = DateTime.UtcNow,
                OrderId = dto.OrderId,
                Template = template
            };

            foreach (var templateItem in template.TemplateItems)
            {
                checkList.Items.Add(
                    new Entities.Item
                    {
                        CheckList = checkList,
                        TemplateItem = templateItem
                    });
            }

            await _session.SaveAsync(checkList);

            return checkList;
        }
    }
}
