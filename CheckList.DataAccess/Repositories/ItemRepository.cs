﻿using CheckList.DataAccess.RepositoryInterfaces;
using NHibernate;
using System;
using System.Threading.Tasks;

namespace CheckList.DataAccess.Repositories
{
    public class ItemRepository : BaseRepository<Entities.Item>, IItemRepository
    {
        /// <summary>
        /// Ctors.
        /// </summary>
        /// <param name="session">Сессия.</param>
        public ItemRepository(ISession session) : base(session)
        {
        }

        /// <summary>
        /// Установить/снять дату завершения элемента чек листа.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Entities.Item> Toggle(int id)
        {
            var item = await _session.GetAsync<Entities.Item>(id);

            if (item == null)
            {
                throw new ApplicationException("Item not found");
            }

            if (item.CheckDate.HasValue)
            {
                item.CheckDate = null;
            }
            else
            {
                item.CheckDate = DateTime.UtcNow;
            }

            await _session.SaveOrUpdateAsync(item);
            await _session.FlushAsync();

            return item;
        }
    }
}
