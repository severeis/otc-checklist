﻿using CheckList.DataAccess.Dtos;
using CheckList.DataAccess.RepositoryInterfaces;
using NHibernate;
using NHibernate.Linq;
using System.Linq;
using System.Threading.Tasks;

namespace CheckList.DataAccess.Repositories
{
    /// <summary>
    /// Репозиторий шаблонов.
    /// </summary>
    public class TemplateRepository : BaseRepository<Entities.Template>, ITemplateRepository
    {
        /// <summary>
        /// Ctors.
        /// </summary>
        /// <param name="session">Сессия.</param>
        public TemplateRepository(ISession session) : base(session)
        {
        }

        /// <summary>
        /// Получить шаблон.
        /// </summary>
        /// <param name="id">Идентификатор шаблона.</param>
        /// <returns>Шаблон чек листа.</returns>
        public async Task<TemplateDto> GetAsync(int id)
        {
            var result = await _session.Query<Entities.Template>()
                .FetchMany(f => f.TemplateItems)
                .FetchMany(f => f.TemplateBlocks)
                    .ThenFetchMany(f => f.TemplateItems)
                .Where(w => w.Id == id)
                .ToListAsync();

            return result?.FirstOrDefault();
        }
    }
}
