﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using CheckList.DataAccess.Dtos;
using CheckList.DataAccess.RepositoryInterfaces;

using NHibernate;
using NHibernate.Linq;

namespace CheckList.DataAccess.Repositories
{
    /// <summary>
    /// Репозиторий шаблонов тендера.
    /// </summary>
    public class TenderConditionRepository : BaseRepository<Entities.TenderCondition>, ITenderConditionRepository
    {
        /// <summary>
        /// Ctors.
        /// </summary>
        /// <param name="session">Сессия.</param>
        public TenderConditionRepository(ISession session) : base(session)
        {
        }

        /// <summary>
        /// Получить шаблон тендера.
        /// </summary>
        /// <param name="purchaseType">Способ закупки</param>
        /// <param name="federalLawTypeId">Закон по которому проводится тендер.</param>
        /// <param name="etpId">ЭТП</param>
        /// <returns></returns>
        public async Task<IEnumerable<TenderConditionDto>> GetTenderContions(int? purchaseType, int? federalLawTypeId, int? etpId)
        {
            var query = _session.Query<Entities.TenderCondition>()
                    .Where(w => w != null)
                    .Where(w => (purchaseType != null && w.PurchaseType != null && w.PurchaseType.Id == purchaseType) || 
                        (purchaseType == null && w.PurchaseType == null))
                    .Where(w => (federalLawTypeId != null && w.FederalLawType != null && w.FederalLawType.Id == federalLawTypeId) ||
                        (federalLawTypeId == null && w.FederalLawType == null))
                    .Where(w => (etpId != null && w.DicEtp != null && w.DicEtp.Id == etpId) || (etpId == null && w.DicEtp == null))
                    .Fetch(f => f.Template);

            var result = await query.ToListAsync();

            return result.Select(s => (TenderConditionDto)s);
        }
    }
}
