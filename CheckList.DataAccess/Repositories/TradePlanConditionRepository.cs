﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using CheckList.DataAccess.Dtos;
using CheckList.DataAccess.RepositoryInterfaces;

using NHibernate;
using NHibernate.Linq;

namespace CheckList.DataAccess.Repositories
{
    /// <summary>
    /// Репозиторий условий отбора шаблона чеклистов для плана закупок.
    /// </summary>
    public class TradePlanConditionRepository : BaseRepository<Entities.TradePlanCondition>, ITradePlanConditionRepository
    {
        /// <summary>
        /// Ctor.
        /// </summary>
        /// <param name="session">Сессия.</param>
        public TradePlanConditionRepository(ISession session) : base(session)
        {
        }

        public async Task<IEnumerable<TradePlanConditionDto>> Get(int? federalLawTypeId)
        {
            var query = _session
                .Query<Entities.TradePlanCondition>()
                .Where(x => x != null);

            query = federalLawTypeId.HasValue
                ? query.Where(x => x.FederalLawType.Id == federalLawTypeId.Value)
                : query.Where(x => x.FederalLawType == null);

            var result = await query.Fetch(x => x.Template).ToListAsync();

            return result.Select(x => (TradePlanConditionDto)x);
        }
    }
}
