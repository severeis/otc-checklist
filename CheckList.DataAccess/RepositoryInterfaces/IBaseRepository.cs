﻿using System.Threading;
using System.Threading.Tasks;

namespace CheckList.DataAccess.RepositoryInterfaces
{
    public interface IBaseRepository<T>
    {
        Task SaveOrUpdateAsync(T obj, CancellationToken cancellationToken = default(CancellationToken));
    }
}