﻿using System.Threading.Tasks;

namespace CheckList.DataAccess.RepositoryInterfaces
{
    /// <summary>
    /// Репозиторий чек листов.
    /// </summary>
    public interface ICheckListRepository
    {
        /// <summary>
        /// Получить чек лист.
        /// </summary>
        /// <param name="id">Идентификатор чек листа.</param>
        /// <returns>Чек лист.</returns>
        Task<Dtos.CheckListDto> GetAsync(int id);

        /// <summary>
        /// Получить чек лист.
        /// </summary>
        /// <param name="orderId">Идентификатор заказа.</param>
        /// <param name="templateId">Идентификатор шаблона чек листа.</param>
        /// <returns>Чек лист.</returns>
        Task<Dtos.CheckListDto> GetByOrderAsync(int orderId, int templateId);

        /// <summary>
        /// Создать чек лист.
        /// </summary>
        /// <param name="dto">Чек лист.</param>
        /// <param name="templateId">Идентификатор шаблона чек листа.</param>
        /// <returns>Чек лист.</returns>
        Task<Dtos.CheckListDto> Create(Dtos.CheckListDto dto, int templateId);
    }
}
