﻿using CheckList.DataAccess.Entities;
using System.Threading.Tasks;

namespace CheckList.DataAccess.RepositoryInterfaces
{
    /// <summary>
    /// Репозиторий элементов чек листа.
    /// </summary>
    public interface IItemRepository : IBaseRepository<Item>
    {
        /// <summary>
        /// Установить/снять дату завершения элемента чек листа.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Item> Toggle(int id);
    }
}
