﻿using System.Threading.Tasks;

namespace CheckList.DataAccess.RepositoryInterfaces
{
    /// <summary>
    /// Репозиторий шаблонов.
    /// </summary>
    public interface ITemplateRepository
    {
        /// <summary>
        /// Получить шаблон.
        /// </summary>
        /// <param name="id">Идентификатор шаблона.</param>
        /// <returns>Шаблон чек листа.</returns>
        Task<Dtos.TemplateDto> GetAsync(int id);
    }
}
