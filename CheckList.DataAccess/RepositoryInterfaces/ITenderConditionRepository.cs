﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CheckList.DataAccess.RepositoryInterfaces
{
    /// <summary>
    /// Репозиторий шаблонов тендера.
    /// </summary>
    public interface ITenderConditionRepository
    {
        /// <summary>
        /// Получить шаблон тендера.
        /// </summary>
        /// <param name="purchaseType">Способ закупки</param>
        /// <param name="federalLawTypeId">Закон по которому проводится тендер.</param>
        /// <param name="etpId">ЭТП</param>
        /// <returns></returns>
        Task<IEnumerable<Dtos.TenderConditionDto>> GetTenderContions(int? purchaseType, int? federalLawTypeId, int? etpId);
    }
}
