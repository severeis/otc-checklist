﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CheckList.DataAccess.RepositoryInterfaces
{
    /// <summary>
    /// Репозиторий условий отбора шаблона чеклистов для плана закупок.
    /// </summary>
    public interface ITradePlanConditionRepository
    {
        /// <summary>
        /// Возвращает условия.
        /// </summary>
        /// <param name="federalLawTypeId">Закон.</param>
        /// <returns>Условия.</returns>
        Task<IEnumerable<Dtos.TradePlanConditionDto>> Get(int? federalLawTypeId);
    }
}
