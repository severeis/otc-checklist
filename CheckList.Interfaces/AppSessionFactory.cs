﻿using CheckList.DataAccess;
using Microsoft.Extensions.Options;
using NHibernate;
using NHibernate.Cfg;

namespace CheckList.Interfaces
{
    public class AppSessionFactory
    {
        public Configuration Configuration { get; }
        public ISessionFactory SessionFactory { get; }

        public AppSessionFactory(Microsoft.Extensions.Logging.ILoggerFactory loggerFactory,
            IOptions<NHibernateSettings> options)
        {
            var configurationProvider = new ConfigurationProvider();
            Configuration = configurationProvider.GetConfiguration(options.Value.NHibernateBinFolder, options.Value.NHibernateConfigFile);

            Configuration.SessionFactory().GenerateStatistics();

            SessionFactory = Configuration.BuildSessionFactory();
        }

        public ISession OpenSession()
        {
            return SessionFactory.OpenSession();
        }
    }
}
