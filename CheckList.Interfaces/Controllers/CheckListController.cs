﻿using CheckList.Clients.Models.Input;
using CheckList.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CheckList.Interfaces.Controllers
{
    [Route("api/check-list")]
    [ApiController]
    [ApiVersionNeutral]
    [Authorize]
    public class CheckListController : ControllerBase
    {
        /// <summary>
        /// Сервис чек лист.
        /// </summary>
        private readonly ICheckListService _checkListService;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="checkListService">Сервис чек лист.</param>
        public CheckListController(ICheckListService checkListService)
        {
            _checkListService = checkListService;
        }

        /// <summary>
        /// Получить чек лист.
        /// </summary>
        /// <param name="param">Параметры для получения элементов чек листа.</param>
        /// <returns>Чек лист.</returns>
        [HttpPost]
        [Route("get")]
        public async Task<IActionResult> GetCheckList(GetCheckListParam param)
        {
            var result = await _checkListService.GetCheckListAsync(param);
            return Ok(result);
        }

        /// <summary>
        /// Получить чек лист.
        /// </summary>
        /// <param name="param">Параметры для получения элементов чек листа.</param>
        /// <returns>Чек лист.</returns>
        [HttpPost]
        [Route("prepare")]
        [AllowAnonymous]
        public async Task<IActionResult> PrepareCheckList(PrepareCheckListParam param)
        {
            var result = await _checkListService.PrepareCheckListAsync(param);
            return Ok(result);
        }

        /// <summary>
        /// Обработать клик по элементу чек листа.
        /// </summary>
        /// <param name="id">Идентификатор элемента чек листа.</param>
        /// <returns>Элемент чек листа.</returns>
        [HttpPost]
        [Route("toggle-item/{id}")]
        public async Task<IActionResult> ToggleCheckListItem(int id)
        {
            var result = await _checkListService.ToggleCheckListItemAsync(id);
            return Ok(result);
        }
    }
}
