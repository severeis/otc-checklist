﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace CheckList.Interfaces.Controllers
{
    /// <summary>
    /// Контроллер версий.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [ApiVersionNeutral]
    public class VersionController : ControllerBase
    {
        /// <summary>
        /// Получить версию микросервиса.
        /// </summary>
        /// <returns>Чат.</returns>
        [HttpGet(), HttpGet("~/")]
        public IActionResult Get()
        {
            return Ok("5.8.0");
        }
    }
}