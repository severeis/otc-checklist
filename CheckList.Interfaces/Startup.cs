﻿using System;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;

using Anresh.Microservices.Infrastructure.Interfaces;

using CheckList.Services;
using CheckList.DataAccess;
using CheckList.Services.Configs;
using CheckList.DataAccess.Repositories;
using CheckList.Services.CheckListService;
using CheckList.Services.ConditionService;
using CheckList.DataAccess.RepositoryInterfaces;

namespace CheckList.Interfaces
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
#if DEBUG
                .AddJsonFile("bin\\Debug\\netcoreapp2.2\\commonSettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"bin\\Debug\\netcoreapp2.2\\commonSettings.{env.EnvironmentName}.json", optional: true)
#else
                .AddJsonFile("commonSettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"commonSettings.{env.EnvironmentName}.json", optional: true)
#endif
                .AddEnvironmentVariables();

            if (env.IsDevelopment())
            {
                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            }

            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<NHibernateSettings>(Configuration.GetSection("NHibernateSettings"));
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));

            services.UseMicroservice(Configuration.GetSection("CommonSettings"));

            services.AddTransient<ICheckListService, CheckListService>();
            services.AddTransient<ICheckListRepository, CheckListRepository>();
            services.AddTransient<ITemplateRepository, TemplateRepository>();
            services.AddTransient<IItemRepository, ItemRepository>();
            services.AddTransient<IConditionService, ConditionService>();
            services.AddTransient<ITenderConditionRepository, TenderConditionRepository>();
            services.AddTransient<ITradePlanConditionRepository, TradePlanConditionRepository>();

            services.AddSingleton<AppSessionFactory>();
            services.AddScoped(x => x.GetService<AppSessionFactory>().OpenSession());

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddSwagger();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IApiVersionDescriptionProvider provider)
        {
            loggerFactory.AddLog4Net();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseAuthentication();
            app.UseCors(AppBuilderExtensions.MyAllowSpecificOrigins);
            app.UseStaticFiles();
            //app.UseHttpsRedirection();
            app.UseMvc();

            app.UseSwagger(provider);
        }
    }
}