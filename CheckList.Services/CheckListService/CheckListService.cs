﻿using System;
using System.Linq;
using System.Threading.Tasks;

using CheckList.Clients.Models;
using CheckList.Clients.Models.Input;
using CheckList.DataAccess.Dtos;
using CheckList.DataAccess.RepositoryInterfaces;
using CheckList.Services.Configs;

using Microsoft.Extensions.Options;

namespace CheckList.Services.CheckListService
{
    /// <summary>
    /// Сервис чек лист.
    /// </summary>
    public class CheckListService : ICheckListService
    {
        /// <summary>
        /// Репозиторий чек листов.
        /// </summary>
        private readonly ICheckListRepository _checkListRepository;

        /// <summary>
        /// Репозиторий шаблонов.
        /// </summary>
        private readonly ITemplateRepository _templateRepository;

        /// <summary>
        /// Репозиторий элементов чек листа.
        /// </summary>
        private readonly IItemRepository _itemRepository;

        /// <summary>
        /// Настройки приложения.
        /// </summary>
        private readonly AppSettings _appSettings;

        /// <summary>
        /// Сервис определения условий.
        /// </summary>
        private readonly IConditionService _conditionService;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="checkListRepository">Репозиторий чек листов.</param>
        /// <param name="templateRepository">Репозиторий шаблонов.</param>
        /// <param name="itemRepository">Репозиторий элементов чек листа.</param>
        /// <param name="options">Конфигурация приложения.</param>
        public CheckListService(ICheckListRepository checkListRepository,
            ITemplateRepository templateRepository,
            IItemRepository itemRepository,
            IOptions<AppSettings> options,
            IConditionService conditionService)
        {
            _checkListRepository = checkListRepository;
            _templateRepository = templateRepository;
            _itemRepository = itemRepository;
            _appSettings = options.Value;
            _conditionService = conditionService;
        }

        /// <summary>
        /// Получить чек лист.
        /// </summary>
        /// <param name="param">Параметры для получения элементов чек листа.</param>
        /// <returns>Чек лист.</returns>
        public async Task<Clients.Models.CheckList> GetCheckListAsync(GetCheckListParam param)
        {
            FixObsolette(param);

            var template = await FindTemplate(param.ConditionId.Value, param.TenderCondition, param.TradePlanCondition);

            var checkList = await _checkListRepository.GetByOrderAsync(param.OrderId, template.Id);

            if (checkList == null)
            {
                var checkListDto = new DataAccess.Entities.CheckList
                {
                    CreateDate = DateTime.UtcNow,
                    OrderId = param.OrderId,
                };

                checkList = await _checkListRepository.Create(checkListDto, template.Id);
            }

            return checkList.ToModel(template);
        }

        /// <summary>
        /// Обработать клик по элементу чек листа.
        /// </summary>
        /// <param name="id">Идентификатор элемента чек листа.</param>
        /// <returns>Элемент чек листа.</returns>
        public async Task<Item> ToggleCheckListItemAsync(int id)
        {
            var item = await _itemRepository.Toggle(id);

            return ((DataAccess.Dtos.ItemDto)item).ToModel(item.TemplateItem);
        }

        /// <summary>
        /// Подготовить предварительный чек лист.
        /// </summary>
        /// <param name="param">Параметры для поиска шаблона чек листа.</param>
        /// <returns>Предварительный чек лист.</returns>
        public async Task<Clients.Models.CheckList> PrepareCheckListAsync(PrepareCheckListParam param)
        {
            FixObsolette(param);

            var template = await FindTemplate(param.ConditionId.Value, param.TenderCondition, param.TradePlanCondition);

            var preCheckList = template.ToPreCheckList();

            return ApplyShowItemsLimit(preCheckList, _appSettings.PrepareCheckListShowItems);
        }

        /// <summary>
        /// Применить ограничение отображаемых элементов чек листа.
        /// </summary>
        /// <param name="checkList">Чек лист.</param>
        /// <param name="showItemsCount">Максимальное количество элементов чек листа.</param>
        /// <returns>Чек лист с ограниченным количеством элементов чек листа.</returns>
        public Clients.Models.CheckList ApplyShowItemsLimit(Clients.Models.CheckList checkList, int showItemsCount)
        {
            var resultCheckList = new Clients.Models.CheckList();

            if (checkList == null)
            {
                return resultCheckList;
            }

            var count = 0;

            checkList.Items?.ForEach(item =>
            {
                if (count >= showItemsCount)
                {
                    return;
                }

                item.ShowDescription = count == 0;

                resultCheckList.Items.Add(item);
                count++;
            });

            if (count >= showItemsCount ||
                checkList.Blocks == null ||
                !checkList.Blocks.Any())
            {
                return resultCheckList;
            }

            foreach (var block in checkList.Blocks.OrderBy(o => o.Priority))
            {
                if (block.Items == null || !block.Items.Any())
                {
                    continue;
                }

                var resultBlock = new Block
                {
                    Description = block.Description,
                    DicBlockId = block.DicBlockId,
                    DicBlockName = block.DicBlockName,
                    Id = block.Id,
                    ParentBlockId = block.ParentBlockId,
                    Priority = block.Priority,
                    ShowItems = block.ShowItems,
                    TemplateId = block.TemplateId
                };

                resultCheckList.Blocks.Add(resultBlock);

                foreach(var item in block.Items)
                {
                    item.ShowDescription = count == 0;

                    resultBlock.Items.Add(item);
                    count++;

                    if (count >= showItemsCount)
                    {
                        return resultCheckList;
                    }
                }
            }

            return resultCheckList;
        }

        private async Task<TemplateDto> FindTemplate(
            int conditionId,
            TenderConditionParam tenderParam,
            TradePlanConditionParam tradePlanParam)
        {
            switch (conditionId)
            {
                case CheckListConditions.Tender:
                    var tenderCondition = await _conditionService.FindTenderConditionAsync(
                        tenderParam?.PurchaseMethod, tenderParam?.FederalLawType, tenderParam?.MarketPlaceId);

                    return tenderCondition != null ? await _templateRepository.GetAsync(tenderCondition.TemplateId) : null;

                case CheckListConditions.TradePlan:
                    var tradePlanCondition = await _conditionService.FindTradePlanConditionAsync(
                        tradePlanParam?.FederalLawType);

                    return tradePlanCondition != null ? await _templateRepository.GetAsync(tradePlanCondition.TemplateId) : null;

                default:
                    throw new ApplicationException($"unknown conditionId: \"{conditionId}\"");
            }
        }

        private void FixObsolette(GetCheckListParam param)
        {
            if (!param.ConditionId.HasValue)
            {
                param.ConditionId = CheckListConditions.Tender;

                param.TenderCondition = new TenderConditionParam
                {
#pragma warning disable CS0618 // Type or member is obsolete
                    PurchaseMethod = param.PurchaseMethod,
                    FederalLawType = param.TenderLawType,
                    MarketPlaceId = param.MarketPlaceId
#pragma warning restore CS0618 // Type or member is obsolete
                };
            }
        }

        private void FixObsolette(PrepareCheckListParam param)
        {
            if (!param.ConditionId.HasValue)
            {
                param.ConditionId = CheckListConditions.Tender;

                param.TenderCondition = new TenderConditionParam
                {
#pragma warning disable CS0618 // Type or member is obsolete
                    PurchaseMethod = param.PurchaseMethod,
                    FederalLawType = param.TenderLawType,
                    MarketPlaceId = param.MarketPlaceId
#pragma warning restore CS0618 // Type or member is obsolete
                };
            }
        }
    }
}
