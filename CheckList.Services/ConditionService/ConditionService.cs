﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using CheckList.DataAccess.RepositoryInterfaces;

namespace CheckList.Services.ConditionService
{
    /// <summary>
    /// Сервис определения условий.
    /// </summary>
    public class ConditionService : IConditionService
    {
        /// <summary>
        /// Репозиторий шаблонов тендера.
        /// </summary>
        private readonly ITenderConditionRepository _tenderConditionRepository;

        /// <summary>
        /// Репозиторий условий отбора шаблона чеклистов для плана закупок.
        /// </summary>
        private readonly ITradePlanConditionRepository _tradePlanConditionRepository;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="tenderConditionRepository">Репозиторий шаблонов тендера.</param>
        /// <param name="tradePlanConditionRepository">Репозиторий условий отбора шаблона чеклистов для плана закупок.</param>
        public ConditionService(ITenderConditionRepository tenderConditionRepository,
                                ITradePlanConditionRepository tradePlanConditionRepository)
        {
            _tenderConditionRepository = tenderConditionRepository;
            _tradePlanConditionRepository = tradePlanConditionRepository;
        }

        /// <summary>
        /// Возвращает подходящее условие по параметрам плана закупок.
        /// </summary>
        /// <param name="federalLawTypeId">Закон.</param>
        /// <returns>Условие.</returns>
        public async Task<DataAccess.Dtos.TradePlanConditionDto> FindTradePlanConditionAsync(int? federalLawTypeId)
        {
            var conditions = await _tradePlanConditionRepository.Get(federalLawTypeId: federalLawTypeId);

            var count = conditions?.Count();

            if (count == 1)
            {
                return conditions.First();
            }
            else if (count > 1)
            {
                throw new ApplicationException("Найдено несколько условий для подбора шаблона");
            }

            return null;
        }

        /// <summary>
        /// Найти подходящее условие для подбора шаблона тендера.
        /// </summary>
        /// <param name="purchaseType">Способ закупки</param>
        /// <param name="federalLawTypeId">Закон по которому проводится тендер.</param>
        /// <param name="etpId">ЭТП</param>
        /// <returns>Условие для подбора шаблона тендера.</returns>
        public async Task<DataAccess.Dtos.TenderConditionDto> FindTenderConditionAsync(
            int? purchaseType,
            int? federalLawTypeId,
            int? etpId)
        {
            DataAccess.Dtos.TenderConditionDto tenderCondition = null;

            // Попытка найти с полным запросом.
            if (purchaseType.HasValue && federalLawTypeId.HasValue && etpId.HasValue)
            {
                tenderCondition = await TryFindTenderConditions(
                    purchaseType: purchaseType,
                    federalLawTypeId: federalLawTypeId,
                    etpId: etpId);

                if (tenderCondition != null)
                {
                    return tenderCondition;
                }
            }

            // Попытка найти без способа закупки.
            if (federalLawTypeId.HasValue && purchaseType.HasValue)
            {
                tenderCondition = await TryFindTenderConditions(
                    purchaseType: purchaseType,
                    federalLawTypeId: federalLawTypeId,
                    etpId: null);

                if (tenderCondition != null)
                {
                    return tenderCondition;
                }
            }

            // Попытка найти только по закону проведения тендера.
            tenderCondition = await TryFindTenderConditions(
                purchaseType: null,
                federalLawTypeId: federalLawTypeId,
                etpId: null);

            return tenderCondition;
        }

        private async Task<DataAccess.Dtos.TenderConditionDto> TryFindTenderConditions(
            int? purchaseType,
            int? federalLawTypeId,
            int? etpId)
        {
            var tenderConditions = await _tenderConditionRepository.GetTenderContions(
                purchaseType: purchaseType,
                federalLawTypeId: federalLawTypeId,
                etpId: etpId);

            if (tenderConditions?.Count() == 1)
            {
                return tenderConditions.First();
            }
            else if (tenderConditions?.Count() > 1)
            {
                throw new ApplicationException("Найдено несколько условий для подбора шаблона");
            }

            return null;
        }

        /// <summary>
        /// Получить идентфикаторы условий.
        /// </summary>
        /// <param name="num">Сумма идентификаторов условий.</param>
        /// <returns>Список идентификаторов условий.</returns>
        public IEnumerable<int> GetActiveConditions(int num)
        {
            var progression = new List<int>();

            if (num == default(int))
            {
                return progression;
            }

            progression.Add(1);

            if (num == 1)
            {
                return progression;
            }

            var currentSum = 2;
            var prevSum = 1;

            // Заполняем числа прогрессии.
            for (int i = 2; i <= num; i++)
            {
                var tempCur = currentSum;
                currentSum = currentSum * prevSum;
                prevSum = tempCur;

                if (currentSum > num)
                {
                    break;
                }

                progression.Add(currentSum);
            }

            var resultCount = progression.Count - 1;

            var checkSum = 0;

            var result = new List<int>();

            // Сумма чисел последовательности должна быть равна входящему числу.
            for (int i = resultCount; i >= 0; i--)
            {
                checkSum = checkSum + progression[i];

                if (checkSum == num)
                {
                    result.Add(progression[i]);
                    break;
                }
                else if (checkSum > num)
                {
                    checkSum = checkSum - progression[i];
                }
                else
                {
                    result.Add(progression[i]);
                }
            }

            return result.OrderBy(o => o);
        }
    }
}
