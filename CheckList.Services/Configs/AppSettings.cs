﻿namespace CheckList.Services.Configs
{
    /// <summary>
    /// Конфигурация приложения.
    /// </summary>
    public class AppSettings
    {
        /// <summary>
        /// Количество элементов отображаемых при просмотре предварительного чек листа.
        /// </summary>
        public int PrepareCheckListShowItems { get; set; }
    }
}
