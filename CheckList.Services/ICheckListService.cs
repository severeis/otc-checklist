﻿using CheckList.Clients.Models.Input;
using System.Threading.Tasks;

namespace CheckList.Services
{
    /// <summary>
    /// Сервис чек лист.
    /// </summary>
    public interface ICheckListService
    {
        /// <summary>
        /// Получить чек лист.
        /// </summary>
        /// <param name="param">Параметры для получения элементов чек листа.</param>
        /// <returns>Чек лист.</returns>
        Task<Clients.Models.CheckList> GetCheckListAsync(GetCheckListParam param);

        /// <summary>
        /// Обработать клик по элементу чек листа.
        /// </summary>
        /// <param name="id">Идентификатор элемента чек листа.</param>
        /// <returns>Элемент чек листа.</returns>
        Task<Clients.Models.Item> ToggleCheckListItemAsync(int id);

        /// <summary>
        /// Подготовить предварительный чек лист.
        /// </summary>
        /// <param name="param">Параметры для поиска шаблона чек листа.</param>
        /// <returns>Предварительный чек лист.</returns>
        Task<Clients.Models.CheckList> PrepareCheckListAsync(PrepareCheckListParam param);

        /// <summary>
        /// Применить ограничение отображаемых элементов чек листа.
        /// </summary>
        /// <param name="checkList">Чек лист.</param>
        /// <param name="showItemsCount">Максимальное количество элементов чек листа.</param>
        /// <returns>Чек лист с ограниченным количеством элементов чек листа.</returns>
        Clients.Models.CheckList ApplyShowItemsLimit(Clients.Models.CheckList checkList, int showItemsCount);
    }
}
