﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CheckList.Services
{
    /// <summary>
    /// Сервис определения условий.
    /// </summary>
    public interface IConditionService
    {
        /// <summary>
        /// Получить идентфикаторы условий.
        /// </summary>
        /// <param name="num">Сумма идентификаторов условий.</param>
        /// <returns>Список идентификаторов условий.</returns>
        IEnumerable<int> GetActiveConditions(int num);

        /// <summary>
        /// Найти подходящее условие для подбора шаблона тендера.
        /// </summary>
        /// <param name="purchaseType">Способ закупки</param>
        /// <param name="federalLawTypeId">Закон по которому проводится тендер.</param>
        /// <param name="etpId">ЭТП</param>
        /// <returns>Условие для подбора шаблона тендера.</returns>
        Task<DataAccess.Dtos.TenderConditionDto> FindTenderConditionAsync(
            int? purchaseType,
            int? federalLawTypeId,
            int? etpId);

        /// <summary>
        /// Возвращает подходящее условие по параметрам плана закупок.
        /// </summary>
        /// <param name="federalLawTypeId">Закон.</param>
        /// <returns>Условие.</returns>
        Task<DataAccess.Dtos.TradePlanConditionDto> FindTradePlanConditionAsync(int? federalLawTypeId);
    }
}