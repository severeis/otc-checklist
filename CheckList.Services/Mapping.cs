﻿using System.Collections.Generic;
using System.Linq;

namespace CheckList.Services
{
    /// <summary>
    /// Маппинг.
    /// </summary>
    public static class Mapping
    {
        /// <summary>
        /// Dto to model.
        /// </summary>
        /// <param name="dto">CheckListDto.</param>
        /// <returns>CheckList.</returns>
        public static Clients.Models.CheckList ToModel(this DataAccess.Dtos.CheckListDto dto, DataAccess.Dtos.TemplateDto templateDto)
        {
            if (dto == null)
            {
                return null;
            }

            var result = new Clients.Models.CheckList
            {
                Id = dto.Id,
                CreateDate = dto.CreateDate,
                OrderId = dto.OrderId,
                TemplateId = dto.Template.Id,
                TemplateName = dto.Template.Name
            };

            if (templateDto == null)
            {
                return result;
            }

            var items = dto.Items?
                .Select(s => s.ToModel(templateDto.TemplateItems?.FirstOrDefault(f => f.Id == s.TemplateItemId)))
                .ToList();

            result.Items = items?.Where(w => !w.TemplateBlockId.HasValue).OrderBy(o => o.Priority).ToList();

            var blockItems = items?.Where(w => w.TemplateBlockId.HasValue).ToList();

            var parentBlocks = templateDto.TemplateBlocks.Where(w => !w.ParentBlockId.HasValue).OrderBy(o => o.Priority).ToList();
            var childBlocks = templateDto.TemplateBlocks?.Where(w => w.ParentBlockId.HasValue).OrderBy(o => o.Priority).ToList();

            foreach (var block in parentBlocks)
            {
                result.Blocks.Add(FillBlock(block, childBlocks, blockItems));
            }

            return result;
        }

        public static Clients.Models.CheckList ToPreCheckList(this DataAccess.Dtos.TemplateDto templateDto)
        {
            if (templateDto == null)
            {
                return null;
            }

            var result = new Clients.Models.CheckList
            {
                TemplateId = templateDto.Id,
                TemplateName = templateDto.Name
            };

            if (templateDto == null)
            {
                return result;
            }

            result.Items = templateDto.TemplateItems?.Where(w => !w.TemplateBlockId.HasValue)
                .OrderBy(o => o.Priority)
                .Select(ToPreModel)
                .ToList();

            var blockItems = templateDto.TemplateItems?.Where(w => w.TemplateBlockId.HasValue)
                .Select(ToPreModel)
                .ToList();

            var parentBlocks = templateDto.TemplateBlocks.Where(w => !w.ParentBlockId.HasValue).OrderBy(o => o.Priority).ToList();
            var childBlocks = templateDto.TemplateBlocks?.Where(w => w.ParentBlockId.HasValue).OrderBy(o => o.Priority).ToList();

            foreach (var block in parentBlocks)
            {
                result.Blocks.Add(FillBlock(block, childBlocks, blockItems));
            }

            return result;
        }

        /// <summary>
        /// Рекурсивно заполнить блоки.
        /// </summary>
        /// <param name="templateBlockDto">Шаблон блока чек листа.</param>
        /// <param name="blockDtos">Список шаблон блоков чек листа.</param>
        /// <param name="items"></param>
        /// <returns>Элементы чек листа.</returns>
        private static Clients.Models.Block FillBlock(DataAccess.Dtos.TemplateBlockDto templateBlockDto,
            List<DataAccess.Dtos.TemplateBlockDto> blockDtos,
            List<Clients.Models.Item> items)
        {
            var block = new Clients.Models.Block
            {
                Id = templateBlockDto.Id,
                Description = templateBlockDto.Description,
                DicBlockId = templateBlockDto.DicBlockId.Value,
                DicBlockName = templateBlockDto.DicBlockName,
                ParentBlockId = templateBlockDto.ParentBlockId,
                Priority = templateBlockDto.Priority,
                TemplateId = templateBlockDto.TemplateId.Value
            };

            if (items?.Any(a => a.TemplateBlockId.HasValue) == true)
            {
                block.Items = items?.Where(w => w.TemplateBlockId == templateBlockDto.Id).OrderBy(o => o.Priority).ToList();
                items.RemoveAll(w => w.TemplateBlockId == templateBlockDto.Id);
            }

            var childBlocks = blockDtos.Where(w => w.ParentBlockId == block.Id).OrderBy(o => o.Priority).ToList();
            blockDtos.RemoveAll(w => w.ParentBlockId == block.Id);

            foreach (var item in childBlocks)
            {
                block.Blocks.Add(FillBlock(item, blockDtos, items));
            }

            return block;
        }

        /// <summary>
        /// Dto to model.
        /// </summary>
        /// <param name="dto">ItemDto.</param>
        /// <returns>Item.</returns>
        public static Clients.Models.Item ToModel(this DataAccess.Dtos.ItemDto dto, DataAccess.Dtos.TemplateItemDto templateItemDto)
        {
            if (dto == null)
            {
                return null;
            }

            return new Clients.Models.Item
            {
                Id = dto.Id,
                CheckDate = dto.CheckDate,
                CheckListId = dto.CheckListId,
                TemplateItemId = dto.TemplateItemId,
                Description = templateItemDto?.Description,
                DicItemId = templateItemDto?.DicItemId,
                DicItemName = templateItemDto?.DicItemName,
                Priority = templateItemDto?.Priority ?? 0,
                TemplateBlockId = templateItemDto?.TemplateBlockId,
                TemplateId = templateItemDto?.TemplateId
            };
        }

        public static Clients.Models.Item ToPreModel(this DataAccess.Dtos.TemplateItemDto templateItemDto)
        {
            if (templateItemDto == null)
            {
                return null;
            }

            return new Clients.Models.Item
            {
                TemplateItemId = templateItemDto.Id,
                Description = templateItemDto?.Description,
                DicItemId = templateItemDto?.DicItemId,
                DicItemName = templateItemDto?.DicItemName,
                Priority = templateItemDto?.Priority ?? 0,
                TemplateBlockId = templateItemDto?.TemplateBlockId,
                TemplateId = templateItemDto?.TemplateId
            };
        }
    }
}
