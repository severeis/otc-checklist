﻿using CheckList.Clients.Models.Input;
using CheckList.DataAccess.Dtos;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CheckList.Tests.CheckListService
{
    public class CheckListServiceTest
    {
        [Test]
        public async Task GetCheckListTest()
        {
            var checkListDto = new CheckListDto
            {
                Id = 1,
                CreateDate = new DateTime(2019, 03, 06),
                OrderId = 0,
                Items = new List<ItemDto>
                {
                    new ItemDto
                    {
                        CheckDate = null,
                        TemplateItemId = 1,

                    },
                    new ItemDto
                    {
                        CheckDate = new DateTime(2019, 03, 07),
                        TemplateItemId = 2
                    }
                },
                Template = new TemplateDto { Id = 1 }
            };

            var templateItemDto = new TemplateItemDto
            {
                Id = 1,
                TemplateBlockId = 1
            };

            var templateDto = new TemplateDto
            {
                Id = 1,
                CreateDate = new DateTime(2019, 03, 06),
                DicConditionId = 1,
                IsActive = true,
                Name = "Template1",
                TemplateBlocks = new List<TemplateBlockDto>
                {
                    new TemplateBlockDto
                    {
                        Id = 1,
                        DicBlockId = 1,
                        TemplateId = 1,
                        Priority = 0,
                        TemplateItems = new List<TemplateItemDto>
                        {
                            templateItemDto
                        }
                    }
                },
                TemplateItems = new List<TemplateItemDto>
                {
                    templateItemDto,
                    new TemplateItemDto
                    {
                        Id = 2
                    }
                }
            };

            var checkListRepoMock = new Mock<DataAccess.RepositoryInterfaces.ICheckListRepository>();
            checkListRepoMock.Setup(s => s.GetByOrderAsync(1, 1))
                .ReturnsAsync(checkListDto);

            var templateRepoMock = new Mock<DataAccess.RepositoryInterfaces.ITemplateRepository>();
            templateRepoMock.Setup(s => s.GetAsync(1))
                .ReturnsAsync(templateDto);

            var itemRepoMock = new Mock<DataAccess.RepositoryInterfaces.IItemRepository>();
            var optionsMock = new Mock<Microsoft.Extensions.Options.IOptions<Services.Configs.AppSettings>>();
            var conditionServiceMock = new Mock<Services.IConditionService>();
            conditionServiceMock.Setup(s => s.FindTenderConditionAsync(null, null, null))
                .ReturnsAsync(new TenderConditionDto { Id = 1, TemplateId = 1 });

            var service = new Services.CheckListService.CheckListService(checkListRepoMock.Object, templateRepoMock.Object, itemRepoMock.Object, optionsMock.Object, conditionServiceMock.Object);

            var result = await service.GetCheckListAsync(new GetCheckListParam
            {
                OrderId = 1
            });

            Assert.NotNull(result);
            Assert.AreEqual(1, result.Id);
            Assert.AreEqual("06.03.2019", result.CreateDate.ToString("dd.MM.yyyy"));
            Assert.AreEqual(0, result.OrderId);

            Assert.AreEqual(1, result.Items.Count, "Items");
            Assert.AreEqual(1, result.Blocks.Count, "Blocks");
        }

        [Test]
        public async Task CheckListApplyShowItemsLimit()
        {
            var checkListDto = new Clients.Models.CheckList
            {
                Id = 1,
                CreateDate = new DateTime(2019, 03, 06),
                OrderId = 0,
                TemplateId = 1,
                Items = new List<Clients.Models.Item>
                {
                    new Clients.Models.Item
                    {
                        CheckDate = null,
                        TemplateItemId = 1,

                    },
                    new Clients.Models.Item
                    {
                        CheckDate = new DateTime(2019, 03, 07),
                        TemplateItemId = 2
                    }
                },
                Blocks = new List<Clients.Models.Block>
                {
                    new Clients.Models.Block
                    {
                        Items = new List<Clients.Models.Item>
                        {
                            new Clients.Models.Item
                            {
                                CheckDate = null,
                                TemplateItemId = 1,

                            },
                            new Clients.Models.Item
                            {
                                CheckDate = new DateTime(2019, 03, 07),
                                TemplateItemId = 2
                            }
                        }
                    },
                    new Clients.Models.Block
                    {
                        Items = new List<Clients.Models.Item>
                        {
                            new Clients.Models.Item
                            {
                                CheckDate = null,
                                TemplateItemId = 1,

                            },
                            new Clients.Models.Item
                            {
                                CheckDate = new DateTime(2019, 03, 07),
                                TemplateItemId = 2
                            }
                        }
                    }
                }
            };

            var checkListRepoMock = new Mock<DataAccess.RepositoryInterfaces.ICheckListRepository>();
            var templateRepoMock = new Mock<DataAccess.RepositoryInterfaces.ITemplateRepository>();
            var itemRepoMock = new Mock<DataAccess.RepositoryInterfaces.IItemRepository>();
            var optionsMock = new Mock<Microsoft.Extensions.Options.IOptions<Services.Configs.AppSettings>>();
            var conditionServiceMock = new Mock<Services.IConditionService>();

            var service = new Services.CheckListService.CheckListService(checkListRepoMock.Object, templateRepoMock.Object, itemRepoMock.Object, optionsMock.Object, conditionServiceMock.Object);

            var result = service.ApplyShowItemsLimit(checkListDto, 3);

            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Items.Count);
            Assert.AreEqual(1, result.Blocks.Count);
            Assert.AreEqual(1, result.Blocks.First().Items.Count);
        }

        [Test]
        public async Task CheckListApplyShowItemsLimit1()
        {
            var checkListDto = new Clients.Models.CheckList
            {
                Id = 1,
                CreateDate = new DateTime(2019, 03, 06),
                OrderId = 0,
                TemplateId = 1,
                Items = new List<Clients.Models.Item>
                {
                    new Clients.Models.Item
                    {
                        CheckDate = null,
                        TemplateItemId = 1,

                    },
                    new Clients.Models.Item
                    {
                        CheckDate = new DateTime(2019, 03, 07),
                        TemplateItemId = 2
                    }
                },
                Blocks = new List<Clients.Models.Block>
                {
                    new Clients.Models.Block
                    {
                        Items = new List<Clients.Models.Item>
                        {
                            new Clients.Models.Item
                            {
                                CheckDate = null,
                                TemplateItemId = 1,

                            },
                            new Clients.Models.Item
                            {
                                CheckDate = new DateTime(2019, 03, 07),
                                TemplateItemId = 2
                            }
                        }
                    },
                    new Clients.Models.Block
                    {
                        Items = new List<Clients.Models.Item>
                        {
                            new Clients.Models.Item
                            {
                                CheckDate = null,
                                TemplateItemId = 1,

                            },
                            new Clients.Models.Item
                            {
                                CheckDate = new DateTime(2019, 03, 07),
                                TemplateItemId = 2
                            }
                        }
                    }
                }
            };

            var checkListRepoMock = new Mock<DataAccess.RepositoryInterfaces.ICheckListRepository>();
            var templateRepoMock = new Mock<DataAccess.RepositoryInterfaces.ITemplateRepository>();
            var itemRepoMock = new Mock<DataAccess.RepositoryInterfaces.IItemRepository>();
            var optionsMock = new Mock<Microsoft.Extensions.Options.IOptions<Services.Configs.AppSettings>>();
            var conditionServiceMock = new Mock<Services.IConditionService>();

            var service = new Services.CheckListService.CheckListService(checkListRepoMock.Object, templateRepoMock.Object, itemRepoMock.Object, optionsMock.Object, conditionServiceMock.Object);

            var result = service.ApplyShowItemsLimit(checkListDto, 5);

            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Items.Count);
            Assert.AreEqual(2, result.Blocks.Count);
            Assert.AreEqual(2, result.Blocks.First().Items.Count);
            Assert.AreEqual(1, result.Blocks.Last().Items.Count);
        }

        [Test]
        public async Task CheckListApplyShowItemsLimit2()
        {
            var checkListDto = new Clients.Models.CheckList
            {
                Id = 1,
                CreateDate = new DateTime(2019, 03, 06),
                OrderId = 0,
                TemplateId = 1,
                Items = new List<Clients.Models.Item>
                {
                    new Clients.Models.Item
                    {
                        CheckDate = null,
                        TemplateItemId = 1,

                    },
                    new Clients.Models.Item
                    {
                        CheckDate = new DateTime(2019, 03, 07),
                        TemplateItemId = 2
                    }
                },
                Blocks = new List<Clients.Models.Block>
                {
                    new Clients.Models.Block
                    {
                        Items = new List<Clients.Models.Item>
                        {
                            new Clients.Models.Item
                            {
                                CheckDate = null,
                                TemplateItemId = 1,

                            },
                            new Clients.Models.Item
                            {
                                CheckDate = new DateTime(2019, 03, 07),
                                TemplateItemId = 2
                            }
                        }
                    },
                    new Clients.Models.Block
                    {
                        Items = new List<Clients.Models.Item>
                        {
                            new Clients.Models.Item
                            {
                                CheckDate = null,
                                TemplateItemId = 1,

                            },
                            new Clients.Models.Item
                            {
                                CheckDate = new DateTime(2019, 03, 07),
                                TemplateItemId = 2
                            }
                        }
                    }
                }
            };

            var checkListRepoMock = new Mock<DataAccess.RepositoryInterfaces.ICheckListRepository>();
            var templateRepoMock = new Mock<DataAccess.RepositoryInterfaces.ITemplateRepository>();
            var itemRepoMock = new Mock<DataAccess.RepositoryInterfaces.IItemRepository>();
            var optionsMock = new Mock<Microsoft.Extensions.Options.IOptions<Services.Configs.AppSettings>>();
            var conditionServiceMock = new Mock<Services.IConditionService>();

            var service = new Services.CheckListService.CheckListService(checkListRepoMock.Object, templateRepoMock.Object, itemRepoMock.Object, optionsMock.Object, conditionServiceMock.Object);

            var result = service.ApplyShowItemsLimit(checkListDto, 7);

            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Items.Count);
            Assert.AreEqual(2, result.Blocks.Count);
            Assert.AreEqual(2, result.Blocks.First().Items.Count);
            Assert.AreEqual(2, result.Blocks.Last().Items.Count);
        }
    }
}
