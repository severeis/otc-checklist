﻿using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CheckList.Tests.CheckListService
{
    public class ConditionServiceTest
    {
        [Test]
        public void GetIncludedCondition()
        {
            var income = 1;
            var result = new[] { 1 };

            var tenderConditionServiceMock = new Mock<DataAccess.RepositoryInterfaces.ITenderConditionRepository>();

            var tradePlanRepoMock = new Mock<DataAccess.RepositoryInterfaces.ITradePlanConditionRepository>();

            var service = new Services.ConditionService.ConditionService(tenderConditionServiceMock.Object, tradePlanRepoMock.Object);
            var res = service.GetActiveConditions(income);

            Assert.AreEqual(result, res.ToArray());

            income = 2;
            result = new[] { 2 };

            res = service.GetActiveConditions(income);

            income = 3;
            result = new[] { 1, 2 };

            res = service.GetActiveConditions(income);

            income = 4;
            result = new[] { 4 };

            res = service.GetActiveConditions(income);

            Assert.AreEqual(result, res.ToArray());

            income = 5;
            result = new[] { 1, 4 };
            res = service.GetActiveConditions(income);

            Assert.AreEqual(result, res.ToArray());

            income = 6;
            result = new[] { 2, 4 };
            res = service.GetActiveConditions(income);

            Assert.AreEqual(result, res.ToArray());

            income = 7;
            result = new[] { 1, 2, 4 };
            res = service.GetActiveConditions(income);

            Assert.AreEqual(result, res.ToArray());

            income = 8;
            result = new[] { 8 };
            res = service.GetActiveConditions(income);

            Assert.AreEqual(result, res.ToArray());
        }

        /// <summary>
        /// Запрос в репозиторий возвращает один результат.
        /// </summary>
        [Test]
        public async Task FindTenderConditionTest()
        {
            var tenderConditions = new List<DataAccess.Dtos.TenderConditionDto>
            {
                new DataAccess.Dtos.TenderConditionDto
                {
                    Id = 15,
                    PurchaseTypeId = 1,
                    FederalLawTypeName = "44",
                    DicEtpId = 2
                }
            };

            var tenderConditionServiceMock = new Mock<DataAccess.RepositoryInterfaces.ITenderConditionRepository>();
            tenderConditionServiceMock.Setup(s => s.GetTenderContions(1, 44, 2))
                .ReturnsAsync(tenderConditions);

            var tradePlanRepoMock = new Mock<DataAccess.RepositoryInterfaces.ITradePlanConditionRepository>();

            var service = new Services.ConditionService.ConditionService(tenderConditionServiceMock.Object, tradePlanRepoMock.Object);

            var result = await service.FindTenderConditionAsync(1, 44, 2);

            Assert.IsNotNull(result);
            Assert.AreEqual(15, result.Id);
        }

        /// <summary>
        /// Запрос в репозиторий возвращает результат без способа закупки.
        /// </summary>
        [Test]
        public async Task FindTenderConditionTest1()
        {
            var tenderConditions = new List<DataAccess.Dtos.TenderConditionDto>
            {
                new DataAccess.Dtos.TenderConditionDto
                {
                    Id = 16,
                    PurchaseTypeId = (int?)null,
                    FederalLawTypeName = "44",
                    FederalLawTypeId = 44,
                    DicEtpId = 2
                }
            };

            var tenderConditionServiceMock = new Mock<DataAccess.RepositoryInterfaces.ITenderConditionRepository>();
            tenderConditionServiceMock.Setup(s => s.GetTenderContions(1, 44, null))
                .ReturnsAsync(tenderConditions);

            var tradePlanRepoMock = new Mock<DataAccess.RepositoryInterfaces.ITradePlanConditionRepository>();

            var service = new Services.ConditionService.ConditionService(tenderConditionServiceMock.Object, tradePlanRepoMock.Object);

            var result = await service.FindTenderConditionAsync(1, 44, 2);

            Assert.IsNotNull(result);
            Assert.AreEqual(16, result.Id);
        }
    }
}
