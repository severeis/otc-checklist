﻿using System;

namespace BaseWebAPI.Common.Domain
{
    /// <summary>
    /// Class ApplicationVersionBase.
    /// </summary>
    public abstract class ApplicationVersionBase
    {
        /// <summary>
        /// Gets or sets the modify date.
        /// </summary>
        /// <value>The modify date.</value>
        public virtual DateTime ModifyDate { get; set; }

        /// <summary>
        /// Gets or sets the row version.
        /// </summary>
        /// <value>The row version.</value>
        public virtual int RowVersion { get; set; }

        /// <summary>
        /// Gets or sets the major.
        /// </summary>
        /// <value>The major.</value>
        public virtual int Major { get; set; }

        /// <summary>
        /// Gets or sets the minor.
        /// </summary>
        /// <value>The minor.</value>
        public virtual int Minor { get; set; }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns><c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            var t = obj as ApplicationVersionBase;
            if (t == null)
                return false;
            if (Major == t.Major && Minor == t.Minor)
                return true;
            return false;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.</returns>
        public override int GetHashCode()
        {
            return (Major + "|" + Minor).GetHashCode();
        }  
    }
}