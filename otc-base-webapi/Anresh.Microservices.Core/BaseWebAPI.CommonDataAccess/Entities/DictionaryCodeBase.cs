﻿namespace BaseWebAPI.Common.Domain
{
    /// <summary>
    /// Базовый класс словаря с кодом.
    /// </summary>
    public abstract class DictionaryCodeBase : DictionaryBase
    {
        /// <summary>
        /// Код.
        /// </summary>
        public virtual string Code { get; set; }
    }
}
