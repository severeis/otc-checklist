﻿namespace BaseWebAPI.Common.Domain
{
    /// <summary>
    /// Class EntityBase.
    /// </summary>
    public abstract class EntityBase : EntityWithTypedIdBase<int>
    {
    }
}