﻿using BaseWebAPI.Common.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseWebAPI.CommonDataAccess.Entities
{
    /// <summary>
    /// Сущность с признаком удален.
    /// </summary>
    public abstract class EntityBaseWithDeleted : EntityBase
    {
        /// <summary>
        /// Удалена ли сущность.
        /// </summary>
        public virtual bool Deleted { get; set; }
    }
}
