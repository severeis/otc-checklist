﻿using System;

namespace BaseWebAPI.Common.Domain
{
    /// <summary>
    /// Class EntityUniqueBase.
    /// </summary>
    public abstract class EntityUniqueBase : EntityWithTypedIdBase<Guid>
    {
    }
}