//using SharpArch.Domain.DomainModel;

namespace BaseWebAPI.Common.Domain
{
    /// <summary>
    /// Class EntityWithTypedIdBase.
    /// </summary>
    /// <typeparam name="TId">The type of the t identifier.</typeparam>
    public abstract class EntityWithTypedIdBase<TId> : IEntityWithTypedIdBase<TId>
    {
        public virtual TId Id { get; set; }

        /// <summary>
        /// Gets or sets the row version.
        /// </summary>
        /// <value>The row version.</value>
        public virtual int RowVersion { get; set; }
    }
}