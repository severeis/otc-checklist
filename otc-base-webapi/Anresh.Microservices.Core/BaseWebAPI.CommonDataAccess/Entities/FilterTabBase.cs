﻿using BaseWebAPI.Common.Domain;
using BaseWebAPI.CommonDataAccess.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseWebAPI.CommonDataAccess.Entities
{
    /// <summary>
    /// Базовая сущность фильтра.
    /// </summary>
    public abstract class FilterTabBase : EntityBase
    {
        /// <summary>
        /// Идентификатор пользователя.
        /// </summary>
        public virtual int? CommonUserId { get; set; }

        /// <summary>
        /// Идентификатор организации.
        /// </summary>
        public virtual int? CommonOrganizationId { get; set; }

        /// <summary>
        /// Название фильтра.
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Тип фильтра.
        /// </summary>
        public virtual FilterTabType FilterTabType { get; set; }

        /// <summary>
        /// Порядковый номер таба в фильтре, наичная с 0.
        /// </summary>
        public virtual int TabOrder { get; set; }

        /// <summary>
        /// JSON обеъкта фильтра.
        /// </summary>
        public virtual string Json { get; set; }

        /// <summary>
        /// Системный.
        /// </summary>
        public virtual bool IsSystem { get; set; }

        /// <summary>
        /// Отображать кол-во найденных результатов.
        /// </summary>
        public virtual bool ShowCounter { get; set; }
    }
}
