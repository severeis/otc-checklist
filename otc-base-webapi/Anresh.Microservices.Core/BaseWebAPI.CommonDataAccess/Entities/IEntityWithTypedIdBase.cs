﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BaseWebAPI.Common.Domain
{
    public interface IEntityWithTypedIdBase<TId>
    {
        TId Id { get; }
    }
}
