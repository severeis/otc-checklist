﻿using BaseWebAPI.Common.Domain;
using BaseWebAPI.CommonDataAccess.Models.Enum;
using System;

namespace BaseWebAPI.CommonDataAccess.Entities
{
    /// <summary>
    /// Class SagaBase.
    /// </summary>
    public abstract class SagaBase : EntityBase
    {
        /// <summary>
        /// Gets or sets the creation date.
        /// </summary>
        /// <value>The creation date.</value>
        public virtual DateTime CreationDate { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>The state.</value>
        public virtual SagaState State { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        /// <value>The start date.</value>
        public virtual DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the last attempt date.
        /// </summary>
        /// <value>The last attempt date.</value>
        public virtual DateTime? LastAttemptDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        /// <value>The end date.</value>
        public virtual DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        /// <value>The error message.</value>
        public virtual string ErrorMessage { get; set; }
    }
}