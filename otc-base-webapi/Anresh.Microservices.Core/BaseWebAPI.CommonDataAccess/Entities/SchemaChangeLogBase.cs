﻿using System;

namespace BaseWebAPI.Common.Domain
{
    /// <summary>
    /// Class SchemaChangeLogBase.
    /// </summary>
    public abstract class SchemaChangeLogBase
    {
        /// <summary>
        /// Gets or sets the name of the script.
        /// </summary>
        /// <value>The name of the script.</value>
        public virtual string ScriptName { get; set; }

        /// <summary>
        /// Gets or sets the execute date.
        /// </summary>
        /// <value>The execute date.</value>
        public virtual DateTime ExecuteDate { get; set; } 
    }
}