﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseWebAPI.Common.Domain
{
    /// <summary>
    /// Class TinyIntEnumEntity.
    /// </summary>
    /// <typeparam name="TEnum">The type of the t enum.</typeparam>
    public class TinyIntEnumEntity<TEnum> : EntityBase, IIdentityNo, ITinyIntId
    {
        /// <summary>
        /// The _enm
        /// </summary>
        private TEnum _enm;

        /// <summary>
        /// Initializes a new instance of the <see cref="TinyIntEnumEntity{TEnum}"/> class.
        /// </summary>
        /// <param name="enm">The enm.</param>
        public TinyIntEnumEntity(TEnum enm)
        {
            this._enm = enm;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TinyIntEnumEntity{TEnum}"/> class.
        /// </summary>
        public TinyIntEnumEntity() { }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public new virtual byte Id
        {
            get { return (byte)Convert.ToInt32(_enm); }
            set { this._enm = (TEnum)Enum.Parse(typeof(TEnum), value.ToString()); }
        }

        /// <summary>
        /// Gets or sets the row version.
        /// </summary>
        /// <value>The row version.</value>
        public new virtual int RowVersion
        {
            get { return 1; }
            set { }
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public virtual string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>The value.</value>
        public virtual TEnum Value
        {
            get { return _enm; }
            set { }
        }

        /// <summary>
        /// Gets the specified en.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="en">The en.</param>
        /// <returns>T.</returns>
        public static T Get<T>(TEnum en) where T : TinyIntEnumEntity<TEnum>, new()
        {
            var entity = new T();
            entity.Id = (byte)Convert.ToInt32(en);
            return entity;
        }
    }
}
