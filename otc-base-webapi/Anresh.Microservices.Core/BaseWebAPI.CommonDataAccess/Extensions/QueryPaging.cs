﻿using BaseWebAPI.CommonDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseWebAPI.CommonDataAccess.Extensions
{
    /// <summary>
    /// QueryPaging.
    /// </summary>
    public static class QueryPaging
    {
        /// <summary>
        /// Постраничная фильтрация.
        /// </summary>
        /// <typeparam name="T">Тип сущности.</typeparam>
        /// <param name="query">Запрос.</param>
        /// <param name="filter">Фильтр.</param>
        /// <returns>IQueryable.</returns>
        public static IQueryable<T> Paging<T>(this IQueryable<T> query, BaseFilterModel filter)
        {
            if (filter.PageIndex > 0)
            {
                var skip = (filter.PageIndex - 1) * filter.PageSize;
                if (skip > 0)
                {
                    query = query.Skip(skip);
                }

                return query.Take(filter.PageSize);
            }

            if (filter.Skip > 0)
            {
                query = query.Skip(filter.Skip);
            }

            if (filter.PageSize > 0)
            {
                query = query.Take(filter.PageSize);
            }

            return query;
        }

        /// <summary>
        /// Строку в список int.
        /// </summary>
        /// <param name="str">Строка.</param>
        /// <returns>Список.</returns>
        public static List<int> ToIntList(this string str)
        {
            try
            {
                return (str ?? string.Empty).Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i.Trim())).ToList();
            }
            catch
            {
                return new List<int>();
            }
        }
    }
}
