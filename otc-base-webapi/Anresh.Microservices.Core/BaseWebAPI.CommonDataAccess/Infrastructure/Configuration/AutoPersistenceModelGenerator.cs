﻿using System;
using BaseWebAPI.Common.Domain;
using FluentNHibernate.Automapping;
using FluentNHibernate.Conventions;
using NHibernate.Envers;
using BaseWebAPI.CommonDataAccess.Entities;

namespace BaseWebAPI.Infrastructure.NHibernate.Configuration
{
	public class AutoPersistenceModelGenerator
	{
		#region IAutoPersistenceModelGenerator Members

        public static AutoPersistenceModel Generate<TEntityAssembly, TEntityMapAssembly>()
        {
            var model = GetModel<TEntityAssembly>();
            model = SetupConvetions(model);
            model = IgnoreBase(model);
            model = UseOverridesFromAssemblyOf<TEntityMapAssembly>(model);
            return model;
        }

        public static AutoPersistenceModel Generatev2<TEntityAssembly, TEntityMapAssembly>()
        {
            var model = GetModel<TEntityAssembly>();
            model = SetupConvetionsv2(model);
            model = IgnoreBase(model);
            model = UseOverridesFromAssemblyOf<TEntityMapAssembly>(model);
            return model;
        }

		#endregion

        #region Helpres

        protected static AutoPersistenceModel GetModel<TEntityAssembly>()
        {
            return AutoMap.AssemblyOf<TEntityAssembly>(new AutomappingConfiguration());
        }

        protected static AutoPersistenceModel SetupConvetions(AutoPersistenceModel model)
        {
            return model.Conventions.Setup(GetConventions());
        }

        protected static AutoPersistenceModel SetupConvetionsv2(AutoPersistenceModel model)
        {
            return model.Conventions.Setup(GetConventionsv2());
        }

        protected static AutoPersistenceModel UseOverridesFromAssemblyOf<TEntityMapAssembly>(AutoPersistenceModel model)
        {
            return model.UseOverridesFromAssemblyOf<TEntityMapAssembly>();
        }

        protected static AutoPersistenceModel IgnoreBase(AutoPersistenceModel model)
        {
            return model
                //.IgnoreBase<Entity>()
                //.IgnoreBase(typeof(EntityWithTypedId<>))
                .IgnoreBase<EntityBase>()
                .IgnoreBase<EntityBaseWithDeleted>()
                .IgnoreBase<DictionaryBase>()
                .IgnoreBase<DictionaryCodeBase>()
                //.IgnoreBase<SynchronizationBase>()
                .IgnoreBase<EntityUniqueBase>()
                //.IgnoreBase<SagaBase>()
                .IgnoreBase(typeof(EnumEntity<>))
                .IgnoreBase(typeof(SmallIntEnumEntity<>))
                .IgnoreBase(typeof(TinyIntEnumEntity<>))
                .IgnoreBase(typeof(EntityWithTypedIdBase<>))
                .IgnoreBase(typeof(DefaultRevisionEntity));
                //.IgnoreBase<FilterTabBase>();
        }

        private static Action<IConventionFinder> GetConventions()
		{
			return c =>
			{
                AddCommonConventions(c);
                c.Add<Conventions.ForeignKeyConvention>();
                c.Add<Conventions.HasManyConvention>();
                c.Add<Conventions.ManyToManyTableNameConvention>();
                c.Add<Conventions.HasManyToManyConvention>();
                c.Add<Conventions.ReferenceConvention>();
                c.Add<Conventions.PropertyConvention>();
            };
		}

        private static Action<IConventionFinder> GetConventionsv2()
        {
            return c =>
            {
                AddCommonConventions(c);
                c.Add<Conventions.v2.ForeignKeyConvention>();
                c.Add<Conventions.v2.HasManyConvention>();
                c.Add<Conventions.v2.ManyToManyTableNameConvention>();
                c.Add<Conventions.v2.HasManyToManyConvention>();
                c.Add<Conventions.v2.ReferenceConvention>();
                c.Add<Conventions.v2.PropertyConvention>();
            };
        }

        private static void AddCommonConventions(IConventionFinder conventionFinder)
        {
            conventionFinder.Add<Conventions.ClassConvention>();
            conventionFinder.Add<Conventions.PrimaryKeyConvention>();
            conventionFinder.Add<Conventions.TableNameConvention>();
            conventionFinder.Add<Conventions.EnumConvention>();
            conventionFinder.Add<Conventions.VersionConvention>();
        }

        #endregion Helpres
    }
}
