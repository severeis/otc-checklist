﻿using System.Collections.Generic;
using System.Linq;
using BaseWebAPI.Common.Domain;
using BaseWebAPI.Infrastructure.NHibernate.Conventions;
using FluentNHibernate;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Steps;
using FluentNHibernate.Conventions;
using NHibernate.Envers;

namespace BaseWebAPI.Infrastructure.NHibernate.Configuration
{
	public class AutomappingConfiguration : DefaultAutomappingConfiguration
	{
		public override bool ShouldMap(System.Type type)
		{
            return type.IsSubclassOf(typeof(SchemaChangeLogBase)) || type.IsSubclassOf(typeof(ApplicationVersionBase)) || type.IsSubclassOf(typeof(DefaultRevisionEntity)) ||
                   type.GetInterfaces().Any(
		               x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof (IEntityWithTypedIdBase<>));
		}

		public override bool ShouldMap(Member member)
		{
			return base.ShouldMap(member) && member.CanWrite;
		}

	    public override bool AbstractClassIsLayerSupertype(System.Type type)
	    {
            return /*type == typeof(EntityWithTypedId<>) || type == typeof(Entity) ||*/ type == typeof(EntityBase) ||
                   type == typeof(EntityWithTypedIdBase<>) ||
                   type == typeof(EntityUniqueBase) ||
                   type == typeof(DictionaryBase) ||
                   type == typeof(DictionaryCodeBase) ||
                   type == typeof(SchemaChangeLogBase) ||
                   //type == typeof (SagaBase) ||
                   //type == typeof(SynchronizationBase) ||
                   type == typeof(EnumEntity<>) ||
                   type == typeof(SmallIntEnumEntity<>) ||
                   type == typeof(TinyIntEnumEntity<>) ||
                   type == typeof(ApplicationVersionBase) ||
                   type == typeof(DefaultRevisionEntity);
                   //|| type == typeof (FilterTabBase);
        }

	    public override bool IsId(Member member)
		{
		    return member.Name == ConventionHelperBase.IDENTITY_COLUMN_NAME;
		}

        public override IEnumerable<IAutomappingStep> GetMappingSteps(FluentNHibernate.Automapping.AutoMapper mapper, IConventionFinder conventionFinder)
        {
            var steps = base.GetMappingSteps(mapper, conventionFinder).ToList();
            steps.Insert(1, new VersionStep(this));

            return steps;
        }
	}
}
