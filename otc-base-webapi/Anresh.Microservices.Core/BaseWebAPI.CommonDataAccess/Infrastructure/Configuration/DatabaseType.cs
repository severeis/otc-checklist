﻿namespace BaseWebAPI.Infrastructure.NHibernate.Configuration
{
    public enum DatabaseType
    {
        MsSqlServer,
        PostgreSql
    }
}