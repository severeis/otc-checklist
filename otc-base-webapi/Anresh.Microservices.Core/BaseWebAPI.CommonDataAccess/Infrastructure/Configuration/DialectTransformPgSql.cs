﻿namespace BaseWebAPI.Infrastructure.NHibernate.Configuration
{
    public class DialectTransformPgSql : ISqlDialectTransform
    {
        public string BeginQuoteIdentifer { get; set; }

        public string EndQuoteIdentifer { get; set; }

        public string VarCharQuoteIdentifer { get; set; }

        public string DefaultSchemaName { get; set; }

        public DatabaseType DatabaseType { get; set; }

        public string GetTopSqlString(int count)
        {
            return string.Empty;
        }

        public string GetLimitSqlString(int count)
        {
            return string.Format("LIMIT {0}", count);
        }

        public string CastIntToBoolSqlString(int value)
        {
            return string.Format("CAST( {0} AS BOOLEAN )", value);
        }

        public string CastDateToSqlString(string date)
        {
            return string.Format("CAST('{0}' AS timestamp without time zone )", date);
        }

        public string NVarCharSqlString(int count)
        {
            return string.Format("VARCHAR({0})", count);
        }

        public string VarCharMaxType()
        {
            return "text";
        }

        public string NVarCharMaxType()
        {
            return "text";
        }

        public string TinyintType()
        {
            return "smallint";
        }

        public string SmallDateTimeType()
        {
            return "timestamp without time zone";
        }

        public string DecimalType(int precision, int scale)
        {
            return string.Format("NUMERIC({0}, {1})", precision, scale);
        }

        public DialectTransformPgSql()
        {
            DatabaseType = DatabaseType.PostgreSql;

            BeginQuoteIdentifer = "\"";
            EndQuoteIdentifer = "\"";
            VarCharQuoteIdentifer = "'";

            DefaultSchemaName = "public";
        }

        public string QuoteDbObjectName(string objectName)
        {
            return BeginQuoteIdentifer + objectName.ToLower() + EndQuoteIdentifer;
        }

        public string CastExpressionToBool(string expression)
        {
            return string.Format("cast((case when {0} then 1 else 0 end) as boolean)",
                expression);
        }

        public string GetVariableSqlString(string variable)
        {
            return variable;
        }

        public string TrueValue()
        {
            return "TRUE";
        }

        public string FalseValue()
        {
            return "FALSE";
        }

        public string WithNoLockValue()
        {
            return string.Empty;
        }

        public string GetExternalDbName(string tableName, string dbName)
        {
            return "Foreign_" + tableName;
        }

        public string TableValuesToString(string tableName, string fieldName, string where)
        {
            return string.Format("select string_agg({0}, ',') from {1} where {2}",
                fieldName, tableName, where);
        }

        public string StringJoinOperator()
        {
            return "||";
        }
    }
}
