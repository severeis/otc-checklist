﻿using System;

namespace BaseWebAPI.Infrastructure.NHibernate.Configuration
{
    public class DialectTransformTSql: ISqlDialectTransform
    {
        public string DefaultSchemaName { get; set; }

        public string BeginQuoteIdentifer { get; set; }

        public string EndQuoteIdentifer { get; set; }

        public string VarCharQuoteIdentifer { get; set; }

        public DatabaseType DatabaseType { get; set; }

        public string GetTopSqlString(int count)
        {
            return string.Format("TOP ({0})", count);
        }

        public string GetLimitSqlString(int count)
        {
            return string.Empty;
        }

        public string CastIntToBoolSqlString(int value)
        {
            return value.ToString();
        }

        public string CastDateToSqlString(string date)
        {
            return string.Format("CAST('{0}' AS DATETIME)", date);
        }

        public DialectTransformTSql()
        {
            DatabaseType = DatabaseType.MsSqlServer;

            BeginQuoteIdentifer = "[";
            EndQuoteIdentifer = "]";
            VarCharQuoteIdentifer = "\"";

            DefaultSchemaName = "dbo";
        }

        public string QuoteDbObjectName(string objectName)
        {
            return BeginQuoteIdentifer + objectName + EndQuoteIdentifer;
        }

        public string CastExpressionToBool(string expression)
        {
            return string.Format("cast((case when {0} then 1 else 0 end) as bit)",
                expression);
        }

        public string NVarCharSqlString(int count)
        {
            return string.Format("NVARCHAR({0})", count);
        }

        public string VarCharMaxType()
        {
            return "varchar(MAX)";
        }

        public string NVarCharMaxType()
        {
            return "nvarchar(MAX)";
        }

        public string TinyintType()
        {
            return "tinyint";
        }

        public string SmallDateTimeType()
        {
            return "smalldatetime";
        }

        public string DecimalType(int precision, int scale)
        {
            return string.Format("DECIMAL({0}, {1})", precision, scale);
        }

        public string GetVariableSqlString(string variable)
        {
            return "@" + variable;
        }

        public string TrueValue()
        {
            return "1";
        }

        public string FalseValue()
        {
            return "0";
        }

        public string WithNoLockValue()
        {
            return "with(nolock)";
        }

        public string GetExternalDbName(string tableName, string dbName)
        {
            return BeginQuoteIdentifer + dbName + EndQuoteIdentifer + "."
                + DefaultSchemaName + "." + tableName;
        }

        public string TableValuesToString(string tableName, string fieldName, string where)
        {
            return string.Format("select {0} as 'data()' from {1} where {2} for XML path('')",
                fieldName, tableName, where);
        }

        public string StringJoinOperator()
        {
            return "+";
        }

        public DateTime AddDays(double value)
        {
            return DateTime.UtcNow;
        }
    }
}
