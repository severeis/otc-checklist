﻿using BaseWebAPI.CommonDataAccess.Infrastructure.Configuration.Functions;
using NHibernate;
using NHibernate.Dialect;
using NHibernate.Dialect.Function;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseWebAPI.CommonDataAccess.Infrastructure.Configuration
{
    public class CustomDialect : MsSql2008Dialect
    {
        public CustomDialect()
        {
            RegisterFunction(
                 "AddDays",
                 new SQLFunctionTemplate(
                      NHibernateUtil.DateTime,
                      "dateadd(dd,?2,?1)"
                      )
                 );

            //RegisterFunction("isnull", new SQLFunctionTemplate(NHibernateUtil.Object, "isnull(?1,?2)"));

            CustomProjections.Register();
        }
    }
}
