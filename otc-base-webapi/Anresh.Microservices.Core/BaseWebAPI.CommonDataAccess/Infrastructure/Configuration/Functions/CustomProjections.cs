﻿using NHibernate;
using NHibernate.Criterion;
using NHibernate.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BaseWebAPI.CommonDataAccess.Infrastructure.Configuration.Functions
{
    public static class CustomProjections
    {
        static CustomProjections()
        {
            ExpressionProcessor.RegisterCustomProjection(() => IsNull(null, ""), ProcessIsNull);
            ExpressionProcessor.RegisterCustomProjection(() => IsNull(null, 0), ProcessIsNull);
            ExpressionProcessor.RegisterCustomProjection(() => ConcatStr(null, "", ""), ProcessConcatStr);
        }

        public static void Register() { }

        public static T IsNull<T>(this T objectProperty, T replaceValueIfIsNull)
        {
            throw new Exception("Not to be used directly - use inside QueryOver expression");
        }

        public static T? IsNull<T>(this T? objectProperty, T replaceValueIfIsNull) where T : struct
        {
            throw new Exception("Not to be used directly - use inside QueryOver expression");
        }

        private static IProjection ProcessIsNull(MethodCallExpression mce)
        {
            var arg0 = ExpressionProcessor.FindMemberProjection(mce.Arguments[0]).AsProjection();
            var arg1 = ExpressionProcessor.FindMemberProjection(mce.Arguments[1]).AsProjection();
            return Projections.SqlFunction("coalesce", NHibernateUtil.Object, arg0, arg1);
        }

        public static string ConcatStr(this string objectProperty, string start, string end)
        {
            throw new Exception("Not to be used directly - use inside QueryOver expression");
        }

        private static IProjection ProcessConcatStr(MethodCallExpression mce)
        {
            var arg0 = ExpressionProcessor.FindMemberProjection(mce.Arguments[0]).AsProjection();
            var arg1 = ExpressionProcessor.FindMemberProjection(mce.Arguments[1]).AsProjection();
            var arg2 = ExpressionProcessor.FindMemberProjection(mce.Arguments[2]).AsProjection();
            return Projections.SqlFunction("concat", NHibernateUtil.String, arg1, arg0, arg2);
        }
    }

}
