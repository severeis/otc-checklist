﻿using NHibernate.Linq.Functions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseWebAPI.CommonDataAccess.Infrastructure.Configuration.Functions
{
    public class MyLinqtoHqlGeneratorsRegistry :
        DefaultLinqToHqlGeneratorsRegistry
    {
        public MyLinqtoHqlGeneratorsRegistry()
        {
            this.Merge(new AddDaysGenerator());
        }
    }
}
