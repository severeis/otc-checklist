﻿using System;

namespace BaseWebAPI.Infrastructure.NHibernate.Configuration
{
    /// <summary>
    /// Интерфейс для описания преобразований sql диалектов.
    /// Используется в нативных SQL-запросах в коде C#.
    /// </summary>
    public interface ISqlDialectTransform
    {
        /// <summary>
        /// Тип БД
        /// </summary>
        DatabaseType DatabaseType { get; set; }

        /// <summary>
        /// Имя схемы БД по-умолчанию
        /// </summary>
        string DefaultSchemaName { get; set; }

        /// <summary>
        /// Начальный спец. символ в имени объекта БД
        /// </summary>
        string BeginQuoteIdentifer { get; set; }

        /// <summary>
        /// Конечный спец. символ в имени объекта БД
        /// </summary>
        string EndQuoteIdentifer { get; set; }

        /// <summary>
        /// Спец. символ, обрамляющий строку
        /// </summary>
        string VarCharQuoteIdentifer { get; set; }

        /// <summary>
        /// Параметр TOP SQL-команды
        /// </summary>
        /// <param name="count">Кол-во записей</param>
        /// <returns></returns>
        string GetTopSqlString(int count);

        /// <summary>
        /// Параметр LIMIT SQL-команды
        /// </summary>
        /// <param name="count">Кол-во записей</param>
        /// <returns></returns>
        string GetLimitSqlString(int count);

        /// <summary>
        /// Преобразование int в bool
        /// Подразумевается, что для разных СУБД (и БД) bool может
        /// храниться в БД в разных типах данных. 
        /// </summary>
        /// <param name="value">Значение bool</param>
        /// <returns>SQL-строка с преобразованием</returns>
        string CastIntToBoolSqlString(int value);

        /// <summary>
        /// Преобразование строки с датой в правильный тип даты в СУБД
        /// </summary>
        /// <param name="date">Строка с датой</param>
        /// <returns></returns>
        string CastDateToSqlString(string date);

        /// <summary>
        /// Использование типа данных NVarChar
        /// </summary>
        /// <param name="count">Размер</param>
        /// <returns></returns>
        string NVarCharSqlString(int count);

        /// <summary>
        /// Тип данных VARCHAR(MAX)
        /// </summary>
        /// <param name="count">Размер</param>
        /// <returns></returns>
        string VarCharMaxType();

        /// <summary>
        /// Тип данных NVARCHAR(MAX)
        /// </summary>
        /// <param name="count">Размер</param>
        /// <returns></returns>
        string NVarCharMaxType();
        /// <summary>
        /// Тип данных Tinyint
        /// </summary>
        /// <returns></returns>
        string TinyintType();

        /// <summary>
        /// Тип данных SmallDateTime
        /// </summary>
        /// <returns></returns>
        string SmallDateTimeType();

        /// <summary>
        /// Тип данных Decimal
        /// </summary>
        /// <param name="precision"></param>
        /// <param name="scale"></param>
        /// <returns></returns>
        string DecimalType(int precision, int scale);

        /// <summary>
        /// Обрамление имени объекта БД (или столбца таблицы) в имя, принятое
        /// в конкретной СУБД. Заключается в кавычки для PostgreSql и в
        /// квадратные скобки для SQL Server
        /// </summary>
        /// <param name="objectName">Имя объекта</param>
        /// <returns></returns>
        string QuoteDbObjectName(string objectName);

        /// <summary>
        /// Построение sql-конструкции cast((case when then 1 else 0 end
        /// </summary>
        /// <param name="expression">Условие when</param>
        /// <returns></returns>
        string CastExpressionToBool(string expression);

        /// <summary>
        /// Приведение имени переменной sql-запроса к принятому
        /// в СУБД синтаксису
        /// </summary>
        /// <param name="variable">Имя переменной</param>
        /// <returns></returns>
        string GetVariableSqlString(string variable);

        /// <summary>
        /// Значение true, принятое в БД
        /// </summary>
        /// <returns></returns>
        string TrueValue();

        /// <summary>
        /// Значение false, принятое в БД
        /// </summary>
        /// <returns></returns>
        string FalseValue();

        /// <summary>
        /// Строка WithNoLock, принятоя в БД либо её отсутсвие
        /// </summary>
        /// <returns></returns>
        string WithNoLockValue();

        /// <summary>
        /// Получение имени внешней таблицы (cross db)
        /// </summary>
        /// <param name="tableName">Имя таблицы</param>
        /// <param name="dbName">Имя БД</param>
        /// <returns></returns>
        string GetExternalDbName(string tableName, string dbName);

        /// <summary>
        /// Построение sql-конструкции, которая формиует строку
        /// с значениями поля таблицы, разделённых символом ','
        /// </summary>
        /// <param name="tableName">Имя таблицы</param>
        /// <param name="fieldName">Имя поля</param>
        /// <param name="where">Условие выборки</param>
        /// <returns></returns>
        string TableValuesToString(string tableName, string fieldName, string where);

        /// <summary>
        /// Оператор объединения строк
        /// </summary>
        /// <returns></returns>
        string StringJoinOperator();
    }
}