﻿namespace BaseWebAPI.Infrastructure.NHibernate.Configuration
{
    /// <summary>
    /// Получение преобразований sql-диалекта для текущего типа СУБД
    /// </summary>
    public static class SqlDialectTransform
    {
        /// <summary>
        /// Тип БД
        /// </summary>
        public static DatabaseType DatabaseType
        {
            get { return Current.DatabaseType; }
        }

        /// <summary>
        /// Преобразования sql-диалекта для текущего типа БД
        /// </summary>
        public static ISqlDialectTransform Current { get; private set; }

        public static void InitializeDialectTransform(string sqlDialect)
        {
            Current = GetSqlDialectTransform(SqlDialectToDatabaseType(sqlDialect));
        }

        public static void InitializeDialectTransform(bool usePostgreSql)
        {
            Current = GetSqlDialectTransform(usePostgreSql
                ? DatabaseType.PostgreSql
                : DatabaseType.MsSqlServer);
        }

        private static DatabaseType SqlDialectToDatabaseType(string sqlDialect)
        {
            return sqlDialect.Contains("PostgreSQL")
                ? DatabaseType.PostgreSql
                : DatabaseType.MsSqlServer;
        }

        private static ISqlDialectTransform GetSqlDialectTransform(DatabaseType databaseType)
        {
            if (databaseType == DatabaseType.PostgreSql)
                return new DialectTransformPgSql();

            return new DialectTransformTSql();
        }

        public static bool IsPostgreSql
        {
            get { return DatabaseType == DatabaseType.PostgreSql; }    
        }

        public static bool IsMsSql
        {
            get { return DatabaseType == DatabaseType.MsSqlServer; }
        }
    }
}
