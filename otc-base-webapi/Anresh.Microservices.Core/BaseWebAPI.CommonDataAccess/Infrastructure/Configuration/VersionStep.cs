﻿using FluentNHibernate;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Steps;
using FluentNHibernate.MappingModel.ClassBased;

namespace BaseWebAPI.Infrastructure.NHibernate.Configuration
{
	public class VersionStep : IAutomappingStep
	{
		private readonly IAutomappingStep _versionStep;
		
		public VersionStep(IAutomappingConfiguration cfg)
		{
			_versionStep = new FluentNHibernate.Automapping.Steps.VersionStep(cfg);
		}
		
		public bool ShouldMap(Member member)
		{
		    return member.Name == Conventions.ConventionHelperBase.VERSION_COLUMN_NAME;
		}

		public void Map(ClassMappingBase classMap, Member member)
		{
			_versionStep.Map(classMap, member);
		}
	}
}
