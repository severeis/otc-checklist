﻿using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace BaseWebAPI.Infrastructure.NHibernate.Conventions
{
    public class ClassConvention : IClassConvention
    {
        public void Apply(IClassInstance instance)
        {
            //instance.Cache.ReadWrite();
            instance.DynamicUpdate();
            instance.OptimisticLock.Version();
        }
    }
}
