﻿using System;

namespace BaseWebAPI.Infrastructure.NHibernate.Conventions
{
    public static class ConventionHelper
    {
        public static string GenerateForeignKeyColumnName(string propertyName)
        {
            return string.Format("{0}_{1}", propertyName, ConventionHelperBase.IDENTITY_COLUMN_NAME); //Column_Id
        }
        
        public static string GenerateForeignKeyName(string typeName, string propertyName, string referenceTypeName)
        {
            return string.Format("FK_{0}_{1}__{2}", typeName, referenceTypeName,
                GenerateForeignKeyColumnName(propertyName));
        }

        public static string GenerateManyToManyTableName(string table1Name, string table2Name)
        {
            return string.Format("{0}_to_{1}", table1Name, table2Name);
        }

        public static string GenerateNameForSequencePrimaryKey(string tableName, string propertyName)
        {
            var sequenceName = string.Format("{0}_{1}_Seq", tableName, propertyName);
            return sequenceName;
        }
    }
}
