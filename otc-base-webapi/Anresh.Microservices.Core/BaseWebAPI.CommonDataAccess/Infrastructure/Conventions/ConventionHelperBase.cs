﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseWebAPI.Infrastructure.NHibernate.Conventions
{
    public static class ConventionHelperBase
    {
        public static readonly string IDENTITY_COLUMN_NAME = "Id";
        public static readonly string VERSION_COLUMN_NAME = "RowVersion";
        public static readonly string IDENTITY_INT_UNSAVED_VALUE = "0";
        public static readonly string IDENTITY_GUID_UNSAVED_VALUE = Guid.Empty.ToString();
    }
}
