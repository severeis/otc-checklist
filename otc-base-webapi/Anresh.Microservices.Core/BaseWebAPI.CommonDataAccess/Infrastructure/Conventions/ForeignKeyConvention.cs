﻿using System;
using FluentNHibernate;

namespace BaseWebAPI.Infrastructure.NHibernate.Conventions
{
	public class ForeignKeyConvention : FluentNHibernate.Conventions.ForeignKeyConvention
	{
		protected override string GetKeyName(Member property, Type type)
		{
		    return ConventionHelper.GenerateForeignKeyColumnName(property == null ? type.Name : property.Name);
		}
	}
}
