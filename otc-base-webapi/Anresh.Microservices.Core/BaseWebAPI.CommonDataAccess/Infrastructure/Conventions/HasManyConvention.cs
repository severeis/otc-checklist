﻿using FluentNHibernate.Conventions;

namespace BaseWebAPI.Infrastructure.NHibernate.Conventions
{
	public class HasManyConvention : IHasManyConvention
	{
		public void Apply(FluentNHibernate.Conventions.Instances.IOneToManyCollectionInstance instance)
		{
		    instance.Key.Column(ConventionHelper.GenerateForeignKeyColumnName(instance.EntityType.Name));
            instance.Cascade.All();
            instance.Inverse();
		}
	}
}
