﻿using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace BaseWebAPI.Infrastructure.NHibernate.Conventions
{
	public class HasManyToManyConvention : IHasManyToManyConvention
	{
		public void Apply(IManyToManyCollectionInstance instance)
		{
			instance.Cascade.SaveUpdate();
		    instance.Key.ForeignKey(ConventionHelper.GenerateForeignKeyName(GetManyToManyTableName(instance),
		                                                                    instance.EntityType.Name, instance.EntityType.Name));
		}

        private static string GetManyToManyTableName(IManyToManyCollectionInstance instance)
        {
            //var first = instance.HasExplicitTable ? instance.OtherSide.EntityType.Name : instance.EntityType.Name;
            //var second = instance.HasExplicitTable ? instance.EntityType.Name : instance.OtherSide.EntityType.Name;

            //return ConventionHelper.GenerateManyToManyTableName(first, second);

            return instance.TableName;
        }
	}
}
