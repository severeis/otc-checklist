﻿using FluentNHibernate.Conventions.Inspections;

namespace BaseWebAPI.Infrastructure.NHibernate.Conventions
{
	public class ManyToManyTableNameConvention : FluentNHibernate.Conventions.ManyToManyTableNameConvention
	{
		protected override string GetBiDirectionalTableName(IManyToManyCollectionInspector collection,
			IManyToManyCollectionInspector otherSide)
		{
		    return ConventionHelper.GenerateManyToManyTableName(collection.EntityType.Name, otherSide.EntityType.Name);
		}

		protected override string GetUniDirectionalTableName(IManyToManyCollectionInspector collection)
		{
            return ConventionHelper.GenerateManyToManyTableName(collection.EntityType.Name, collection.ChildType.Name);
		}
	}
}