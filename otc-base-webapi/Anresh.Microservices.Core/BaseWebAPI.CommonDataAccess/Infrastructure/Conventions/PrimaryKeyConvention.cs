﻿using System;
using System.Linq;
using BaseWebAPI.Common.Domain;
using FluentNHibernate.Conventions;

namespace BaseWebAPI.Infrastructure.NHibernate.Conventions
{
	public class PrimaryKeyConvention : IIdConvention
	{
		public void Apply(FluentNHibernate.Conventions.Instances.IIdentityInstance instance)
		{
            if (IsSchemaChangeLog(instance))
                return;

            instance.Column(ConventionHelperBase.IDENTITY_COLUMN_NAME);

            if (IsEntityUniqueBase(instance))
            {
                instance.UnsavedValue(ConventionHelperBase.IDENTITY_GUID_UNSAVED_VALUE);
                instance.GeneratedBy.Guid();
            }
            else
            {
                instance.UnsavedValue(ConventionHelperBase.IDENTITY_INT_UNSAVED_VALUE);

                if (IsIdentityNo(instance))
                    instance.GeneratedBy.Assigned();
                else
                {
                    instance.GeneratedBy.Identity();

#if USE_POSTGRESQL
                    instance.GeneratedBy.Sequence(
                        ConventionHelper.GenerateNameForSequencePrimaryKey(
                            instance.EntityType.Name,
                            instance.Name));
#endif
                }

                if (IsITinyIntId(instance))
                    instance.CustomType(typeof(byte));

                if (IsISmallIntId(instance))
                    instance.CustomType(typeof(short));
            }
		}

        #region Helpers

        private bool IsIdentityNo(FluentNHibernate.Conventions.Instances.IIdentityInstance instance)
        {
            return IsInterface(instance, typeof(IIdentityNo));
        }

        private bool IsITinyIntId(FluentNHibernate.Conventions.Instances.IIdentityInstance instance)
        {
            return IsInterface(instance, typeof(ITinyIntId));
        }

        private bool IsISmallIntId(FluentNHibernate.Conventions.Instances.IIdentityInstance instance)
        {
            return IsInterface(instance, typeof(ISmallIntId));
        }

        private bool IsInterface(FluentNHibernate.Conventions.Instances.IIdentityInstance instance, Type interfaceType)
        {
            return instance.EntityType.GetInterfaces().Contains(interfaceType);
        }

        private bool IsSchemaChangeLog(FluentNHibernate.Conventions.Instances.IIdentityInstance instance)
        {
            return IsIsSubclassOf<SchemaChangeLogBase>(instance);
        }

        private bool IsEntityUniqueBase(FluentNHibernate.Conventions.Instances.IIdentityInstance instance)
        {
            return IsIsSubclassOf<EntityUniqueBase>(instance);
        }

        private bool IsIsSubclassOf<TClass>(FluentNHibernate.Conventions.Instances.IIdentityInstance instance)
        {
            return instance.EntityType.IsSubclassOf(typeof(TClass));
        }

        #endregion Helpers
    }
}
