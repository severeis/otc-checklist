﻿using BaseWebAPI.CommonDataAccess.Entities;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseWebAPI.Infrastructure.NHibernate.Conventions
{
    public class PropertyConvention : IPropertyConvention
    {
        public void Apply(IPropertyInstance instance)
        {
            if (IsSubclassOf<EntityBaseWithDeleted>(instance) && instance.Property.Name == "Deleted")
            {
                instance.Not.Nullable();
            }
        }

        private bool IsSubclassOf<TClass>(IPropertyInstance instance)
        {
            return instance.EntityType.IsSubclassOf(typeof(TClass));
        }
    }
}
