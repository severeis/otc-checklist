﻿using FluentNHibernate.Conventions;

namespace BaseWebAPI.Infrastructure.NHibernate.Conventions
{
	public class ReferenceConvention : IReferenceConvention
	{
		public void Apply(FluentNHibernate.Conventions.Instances.IManyToOneInstance instance)
		{
		    instance.Column(ConventionHelper.GenerateForeignKeyColumnName(instance.Property.Name));
		    instance.ForeignKey(ConventionHelper.GenerateForeignKeyName(instance.Property.DeclaringType.Name,
		                                                                instance.Property.Name, instance.Class.Name));
		}
	}
}
