﻿using FluentNHibernate.Conventions;

namespace BaseWebAPI.Infrastructure.NHibernate.Conventions
{
	public class TableNameConvention : IClassConvention
	{
		public void Apply(FluentNHibernate.Conventions.Instances.IClassInstance instance)
		{
			instance.Table(instance.EntityType.Name);
		}
	}
}
