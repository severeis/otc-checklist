﻿using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace BaseWebAPI.Infrastructure.NHibernate.Conventions
{
    public class VersionConvention : IVersionConvention
    {
        public void Apply(IVersionInstance instance)
        {
            instance.Column(ConventionHelperBase.VERSION_COLUMN_NAME);
            instance.Not.Nullable();
            instance.UnsavedValue("0");
        }
    }
}
