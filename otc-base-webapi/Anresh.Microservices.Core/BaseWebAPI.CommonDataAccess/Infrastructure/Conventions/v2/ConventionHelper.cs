﻿using System;
using BaseWebAPI.Infrastructure.NHibernate.Conventions;

namespace BaseWebAPI.Infrastructure.NHibernate.Conventions.v2
{
    public static class ConventionHelper
    {
        public static string GenerateForeignKeyColumnName(string propertyName)
        {
            return string.Format("{0}{1}", propertyName, ConventionHelperBase.IDENTITY_COLUMN_NAME); //ColumnId
        }

        public static string GenerateForeignKeyName(string typeName, string propertyName, string referenceTypeName)
        {
            return string.Format("FK_{0}_{1}__{2}", typeName, referenceTypeName,
                GenerateForeignKeyColumnName(propertyName));
        }

        public static string GenerateManyToManyTableName(string table1Name, string table2Name)
        {
            return string.Format("{0}To{1}", table1Name, table2Name);
        }
    }
}
