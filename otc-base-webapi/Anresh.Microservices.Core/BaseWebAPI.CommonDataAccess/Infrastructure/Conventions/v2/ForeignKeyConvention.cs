﻿using System;
using FluentNHibernate;

namespace BaseWebAPI.Infrastructure.NHibernate.Conventions.v2
{
	public class ForeignKeyConvention : FluentNHibernate.Conventions.ForeignKeyConvention
	{
		protected override string GetKeyName(Member property, Type type)
		{
		    return ConventionHelper.GenerateForeignKeyColumnName(property == null ? type.Name : property.Name);
		}
	}
}
