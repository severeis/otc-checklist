﻿using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace BaseWebAPI.Infrastructure.NHibernate.Conventions.v2
{
	public class HasManyToManyConvention : IHasManyToManyConvention
	{
		public void Apply(IManyToManyCollectionInstance instance)
		{
            instance.Cascade.SaveUpdate();
            instance.Key.ForeignKey(ConventionHelper.GenerateForeignKeyName(GetManyToManyTableName(instance),
                                                                            instance.EntityType.Name, instance.EntityType.Name));
            instance.Relationship.ForeignKey(ConventionHelper.GenerateForeignKeyName(GetManyToManyTableName(instance),
                                                                            instance.Relationship.StringIdentifierForModel, instance.Relationship.StringIdentifierForModel));

            instance.AsSet();
		}

        private static string GetManyToManyTableName(IManyToManyCollectionInstance instance)
        {
            return instance.TableName;
        }
	}
}
