﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;
using BaseWebAPI.CommonDataAccess.Entities;

namespace BaseWebAPI.Infrastructure.NHibernate.Conventions.v2
{
    public class PropertyConvention : IPropertyConvention
    {
        public void Apply(IPropertyInstance instance)
        {
            if (instance.Property.PropertyType == typeof(string))
            {
                instance.CustomType("AnsiString(200)");
            }

            if (IsSubclassOf<EntityBaseWithDeleted>(instance) && instance.Property.Name == "Deleted")
            {
                instance.Not.Nullable();
                instance.Default("0");
            }
        }

        private bool IsSubclassOf<TClass>(IPropertyInstance instance)
        {
            return instance.EntityType.IsSubclassOf(typeof(TClass));
        }
    }
}
