﻿using System;
using System.Reflection;
using BaseWebAPI.Infrastructure.NHibernate.Configuration;
using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using NConfiguration = NHibernate.Cfg.Configuration;
using NHibernate.Cfg;
using BaseWebAPI.CommonDataAccess.Infrastructure.Configuration.Functions;
using BaseWebAPI.CommonDataAccess.Infrastructure.Configuration;

namespace BaseWebAPI.Infrastructure.NHibernate
{
    public static class NHibernateInitializer
    {
        public static NConfiguration CreateConfiguration(
            string cfgFile, 
            string[] mappingAssemblies, 
            AutoPersistenceModel autoPersistenceModel, 
            INamingStrategy namingStrategy = null, 
            Action<NConfiguration> alterConfiguration = null)
        {
            var cfg = new NConfiguration();
            cfg.Configure(cfgFile);

            if(namingStrategy != null)
            {
                cfg.SetNamingStrategy(namingStrategy);
            }

            var sqlDialect = cfg.Properties.ContainsKey("dialect")
                                ? cfg.Properties["dialect"]
                                : "NHibernate.Dialect.MsSql2008Dialect";

            if (!sqlDialect.Contains("PostgreSQL"))
            {
                cfg.DataBaseIntegration(x => x.Dialect<CustomDialect>());
                cfg.LinqToHqlGeneratorsRegistry<MyLinqtoHqlGeneratorsRegistry>();
            }

            SqlDialectTransform.InitializeDialectTransform(sqlDialect);
            FluentConfiguration fluentConfiguration = Fluently.Configure(cfg);

            if (alterConfiguration != null)
            {
                fluentConfiguration.ExposeConfiguration(alterConfiguration);
            }
            
            fluentConfiguration.Mappings(m =>
            {
                foreach (var mappingAssembly in mappingAssemblies)
                {
                    var assembly =
                        Assembly.LoadFrom(MakeLoadReadyAssemblyName(mappingAssembly));

                    m.HbmMappings.AddFromAssembly(assembly);
                    m.FluentMappings.AddFromAssembly(assembly)
                        .Conventions.AddAssembly(assembly);
                }

                if (autoPersistenceModel != null)
                {
                    m.AutoMappings.Add(autoPersistenceModel);
                }
            });

            return fluentConfiguration.BuildConfiguration();
        }

        private static string MakeLoadReadyAssemblyName(string assemblyName)
        {
            return (assemblyName.IndexOf(".dll") == -1)
                ? assemblyName.Trim() + ".dll"
                : assemblyName.Trim();

        } 
    }
}