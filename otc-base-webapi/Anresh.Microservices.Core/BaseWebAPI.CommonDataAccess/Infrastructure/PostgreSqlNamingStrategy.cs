﻿using NHibernate.Cfg;

namespace BaseWebAPI.Infrastructure.NHibernate
{
    public class PostgreSqlNamingStrategy : INamingStrategy
    {
        public string ClassToTableName(string className)
        {
            return className;
        }

        public string PropertyToColumnName(string propertyName)
        {
            return propertyName;
        }

        public string TableName(string tableName)
        {
            return DoubleQuoteAndLower(tableName);
        }

        public string ColumnName(string columnName)
        {
            return columnName;
        }

        public string PropertyToTableName(string className,
            string propertyName)
        {
            return propertyName;
        }

        public string LogicalColumnName(string columnName,
            string propertyName)
        {
            return string.IsNullOrWhiteSpace(columnName)
                ? propertyName
                : columnName;
        }

        private static string DoubleQuote(string raw)
        {
            raw = raw.Replace("`", "");
            raw = raw.Replace("[", "");
            raw = raw.Replace("]", "");
            raw = raw.Replace("\"", "");

            return string.Format("\"{0}\"", raw);
        }

        private static string DoubleQuoteAndLower(string raw)
        {
            var result = DoubleQuote(raw);
            return result.ToLower();
        }
    }
}
