﻿using BaseWebAPI.CommonDataAccess.Entities;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseWebAPI.CommonDataAccess.Mappings
{
    public abstract class FilterTabBaseMap<T> : IAutoMappingOverride<T>
        where T : FilterTabBase
    {
        public virtual void Override(AutoMapping<T> mapping)
        {
            mapping.Map(c => c.CommonOrganizationId).Nullable();
            mapping.Map(c => c.CommonUserId).Nullable();
            mapping.Map(c => c.FilterTabType).Not.Nullable();
            mapping.Map(c => c.TabOrder).Not.Nullable();
            mapping.Map(c => c.Name).Not.Nullable().Length(200);
            mapping.Map(c => c.Json).Not.Nullable().Length(8000);
            mapping.Map(c => c.IsSystem).Not.Nullable();
            mapping.Map(c => c.ShowCounter).Not.Nullable();
        }
    }
}
