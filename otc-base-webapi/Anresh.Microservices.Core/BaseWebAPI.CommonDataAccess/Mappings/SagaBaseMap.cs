﻿using BaseWebAPI.CommonDataAccess.Entities;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace BaseWebAPI.CommonDataAccess.Mappings
{
    public abstract class SagaBaseMap<T> : IAutoMappingOverride<T>
        where T : SagaBase
    {
        public virtual void Override(AutoMapping<T> mapping)
        {
            mapping.Map(c => c.CreationDate).Not.Nullable();
            mapping.Map(c => c.ErrorMessage).Nullable().Length(10000);
            mapping.Map(c => c.LastAttemptDate).Nullable();
            mapping.Map(c => c.StartDate).Nullable();
            mapping.Map(c => c.EndDate).Nullable();
            mapping.Map(c => c.State).Not.Nullable();
        }
    }
}