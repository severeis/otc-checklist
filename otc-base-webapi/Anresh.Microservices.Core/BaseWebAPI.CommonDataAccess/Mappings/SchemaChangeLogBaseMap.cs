﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace BaseWebAPI.Common.Domain.Data.Mapping
{
    public class SchemaChangeLogBaseMap<T> : IAutoMappingOverride<T>
        where T : SchemaChangeLogBase
    {
        public void Override(AutoMapping<T> mapping)
        {
            mapping.Id(c => c.ScriptName).Length(200);
            mapping.Map(c => c.ExecuteDate).Not.Nullable();
        }
    }
}