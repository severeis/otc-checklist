﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseWebAPI.CommonDataAccess.Models
{
    /// <summary>
    /// Базовая модель фильтра.
    /// </summary>
    public class BaseFilterModel
    {
        /// <summary>
        /// Количество элементов на странице.
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// Номер страницы.
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// Количество элементов, которое нужно пропустить.
        /// </summary>
        public int Skip { get; set; }
    }
}
