﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseWebAPI.CommonDataAccess.Models.Enum
{
    public enum FilterTabType
    {
        /// <summary>
        /// Заявки.
        /// </summary>
        FinRequests = 1,

        /// <summary>
        /// Предложения.
        /// </summary>
        FinOffers = 2,

        /// <summary>
        /// Выданные кредиты/займы.
        /// </summary>
        Credits = 3,

        /// <summary>
        /// Шаблоны автоинвестирования.
        /// </summary>
        AutoinvestmentTemplates = 4,

        /// <summary>
        /// Полученные займы.
        /// </summary>
        BorrowerCredits = 5,
    }
}
