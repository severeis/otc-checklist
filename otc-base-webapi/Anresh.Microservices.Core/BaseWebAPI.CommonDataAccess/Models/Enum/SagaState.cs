﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseWebAPI.CommonDataAccess.Models.Enum
{
    /// <summary>
    /// Представляет состояние саги
    /// </summary>
    public enum SagaState
    {
        /// <summary>
        /// Новый.
        /// </summary>
        New = 1,

        /// <summary>
        /// В работе.
        /// </summary>
        InProcess = 2,

        /// <summary>
        /// Ожидает завершения других БП.
        /// </summary>
        Waiting = 3,

        /// <summary>
        /// Завершена.
        /// </summary>
        Completed = 5,

        /// <summary>
        /// Завершена с ошибкой.
        /// </summary>
        CompletedWithError = 6,

        /// <summary>
        /// Принудительно завершена.
        /// </summary>
        ForceComplete = 7,

        /// <summary>
        /// Отправка на повторную обработку, например получить Id сущности с ООС.
        /// </summary>
        DoubleProcessState = 8,

        /// <summary>
        /// Ошибка.
        /// </summary>
        Error = 100,

        /// <summary>
        /// Удалена.
        /// </summary>
        Deleted = 101
    }
}