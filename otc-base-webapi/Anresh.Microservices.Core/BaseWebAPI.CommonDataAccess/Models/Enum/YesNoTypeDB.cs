﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseWebAPI.CommonDataAccess.Models.Enum
{
    /// <summary>
    /// Да/нет.
    /// </summary>
    public enum YesNoTypeDB
    {
        /// <summary>
        /// Нет
        /// </summary>
        No = 1,

        /// <summary>
        /// Да
        /// </summary>
        Yes = 2,
    }
}
