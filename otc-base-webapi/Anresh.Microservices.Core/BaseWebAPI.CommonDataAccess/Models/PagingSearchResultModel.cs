﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseWebAPI.CommonDataAccess.Models
{
    /// <summary>
    /// Результат поиска элеметов в коллекции. 
    /// </summary>
    /// <typeparam name="T">Тип элеметов в коллекции.</typeparam>
    [Serializable]
    public class PagingSearchResultModel<T>
    {
        #region Constructors

        public PagingSearchResultModel()
        {
            Items = new T[0];
            TotalRows = 0;
        }

        public PagingSearchResultModel(T[] items, int totalRows)
        {
            if (items == null)
                throw new ArgumentNullException("items");

            if (totalRows < 0)
                throw new ArgumentOutOfRangeException("totalRows", "Количество строк должно быть больше нуля");

            Items = items;
            TotalRows = totalRows;
        }

        #endregion

        /// <summary>
        /// Результат поиска.
        /// </summary>
        public T[] Items { get; protected set; }

        /// <summary>
        /// Общее количество строк в БД.
        /// </summary>
        public int TotalRows { get; private set; }
    }
}
