﻿namespace Anresh.Microservices.Clients.Common
{
    /// <summary>
    /// Настройки приложения.
    /// </summary>
    public class CommonSettings
    {
        /// <summary>
        /// Время, после которого токен будет удален из кэша, если с ним никто не обращался.
        /// </summary>
        public string TokenSlidingExpiration { get; set; }

        /// <summary>
        /// Название куки с токеном.
        /// </summary>
        public string TokenCookieName { get; set; }

        /// <summary>
        /// Адрес сайта.
        /// </summary>
        public string SiteOrigin { get; set; }

        /// <summary>
        /// Адрес сервиса для получения токена.
        /// </summary>
        public string CommonTokenApi { get; set; }

        /// <summary>
        /// Адрес Витрины.
        /// </summary>
        public string TradeRegistryApi { get; set; }
    }
}
