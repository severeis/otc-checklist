﻿using System.Threading.Tasks;

using Anresh.Microservices.Clients.Common.Models.Token;

namespace Anresh.Microservices.Clients.Common
{
    /// <summary>
    /// Клиент для работы с токенами.
    /// </summary>
    public interface ITokenClient
    {
        /// <summary>
        /// Получить информацию о токене.
        /// </summary>
        /// <param name="token">Токен.</param>
        /// <returns>Информация.</returns>
        Task<TokenInfo> GetInfoAsync(string token);

        /// <summary>
        /// Возвращает токен для пользователя.
        /// </summary>
        /// <param name="userName">Имя пользователя.</param>
        /// <returns>Токен.</returns>
        Task<string> GetTokenAsync(string userName);
    }
}
