﻿using Clients.Common.Models.TradeRegistry;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Clients.Common
{
    /// <summary>
    /// Клиент для работы с Витриной.
    /// </summary>
    public interface ITradeRegistryClient
    {
        /// <summary>
        /// Получить словари для поисковых запросов.
        /// </summary>
        /// <returns></returns>
        Task<MonitoringDictionaries> GetMonitoringDictionaries();
    }
}
