﻿using Microsoft.IdentityModel.Tokens;
using System;

namespace Anresh.Microservices.Clients.Common.Models.Token
{
    /// <summary>
    /// Информация о токене.
    /// </summary>
    public class TokenInfo : SecurityToken
    {
        /// <summary>
        /// Дата выдачи.
        /// </summary>
        public DateTime IssueUtc { get; set; }

        /// <summary>
        /// Срок действия.
        /// </summary>
        public DateTime ExpireUtc { get; set; }

        /// <summary>
        /// Идентификатор пользователя.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Идентификатор сотрудника.
        /// </summary>
        public int EmployeeId { get; set; }

        /// <summary>
        /// Идентификатор организации.
        /// </summary>
        public int OrganizationId { get; set; }

        /// <summary>
        /// Платформа.
        /// </summary>
        public int PlatformId { get; set; }

        /// <summary>
        /// Отпечаток сертификата.
        /// </summary>
        public string Thumbprint { get; set; }

        /// <summary>
        /// Признак оператора.
        /// </summary>
        public bool IsOperator { get; set; }

        public override string Id => this.UserId.ToString();

        public override string Issuer => throw new NotImplementedException();

        public override SecurityKey SecurityKey => throw new NotImplementedException();

        public override SecurityKey SigningKey { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public override DateTime ValidFrom => throw new NotImplementedException();

        public override DateTime ValidTo => this.ExpireUtc;
    }
}
