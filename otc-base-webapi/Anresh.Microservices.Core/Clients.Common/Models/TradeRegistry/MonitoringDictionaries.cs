﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Clients.Common.Models.TradeRegistry
{
    /// <summary>
    /// Словари для страницы мониторинга.
    /// </summary>
    public class MonitoringDictionaries
    {
        public Dictionary<int, string> CurrencyCodes { get; set; }

        public List<Search2TreeViewModel> Marketplaces { get; set; }

        public Dictionary<int, string> PurchaseMethods { get; set; }
    }
}
