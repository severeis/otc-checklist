﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Clients.Common.Models.TradeRegistry
{
    /// <summary>
    /// Модель древовидного справочника.
    /// </summary>
    public class Search2TreeViewModel
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Имя.
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// Потомки.
        /// </summary>
        public Search2TreeViewModel[] children { get; set; }
    }
}
