﻿using System.Threading.Tasks;

using Anresh.Microservices.Clients.Common.Models.Token;
using Anresh.Microservices.Infrastructure.Clients;
using Anresh.Microservices.Infrastructure.Clients.Cfg;
using Anresh.Microservices.Infrastructure.Clients.Logging;
using Microsoft.Extensions.Options;

namespace Anresh.Microservices.Clients.Common
{
    /// <summary>
    /// Клиент для работы с токенами.
    /// </summary>
    public class TokenClient : WebApiClientBase, ITokenClient
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="httpClientProvider">Поставщик HTTP клиентов.</param>
        /// <param name="chain">Поставщик идентификатора цепочки вызовов.</param>
        /// <param name="log">Логгер.</param>
        public TokenClient(IHttpClientProvider httpClientProvider, IChainIdProvider chain, IClientLog log, IOptions<CommonSettings> commonSettings)
            : base(commonSettings.Value.CommonTokenApi, httpClientProvider, chain, log)
        {
        }

        /// <summary>
        /// Возвращает информацию о токене.
        /// </summary>
        /// <param name="token">Токен.</param>
        /// <returns>Информация.</returns>
        public async Task<TokenInfo> GetInfoAsync(string token)
        {
            return await PostAsync<TokenInfo>("api/token/info", new { token }, null).ConfigureAwait(false);
        }

        /// <summary>
        /// Возвращает токен для пользователя.
        /// </summary>
        /// <param name="userName">Имя пользователя.</param>
        /// <returns>Токен.</returns>
        public async Task<string> GetTokenAsync(string userName)
        {
            return await GetAsync<string>("api/gettokenbyuser", new { userName }, null).ConfigureAwait(false);
        }
    }
}
