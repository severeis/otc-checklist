﻿using System.Threading.Tasks;
using Anresh.Microservices.Clients.Common;
using Anresh.Microservices.Clients.Common.Models.Token;
using Anresh.Microservices.Infrastructure.Clients;
using Anresh.Microservices.Infrastructure.Clients.Cfg;
using Anresh.Microservices.Infrastructure.Clients.Logging;
using Clients.Common.Models.TradeRegistry;
using Microsoft.Extensions.Options;

namespace Clients.Common
{
    /// <inhritdoc />
    public class TradeRegistryClient : WebApiClientBase, ITradeRegistryClient
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="httpClientProvider">Поставщик HTTP клиентов.</param>
        /// <param name="chain">Поставщик идентификатора цепочки вызовов.</param>
        /// <param name="log">Логгер.</param>
        public TradeRegistryClient(IHttpClientProvider httpClientProvider, IChainIdProvider chain, IClientLog log, IOptions<CommonSettings> commonSettings)
            : base(commonSettings.Value.TradeRegistryApi, httpClientProvider, chain, log)
        {
        }

        /// <inhritdoc />
        public async Task<MonitoringDictionaries> GetMonitoringDictionaries()
        {
            return await GetAsync<MonitoringDictionaries>("api/order/GetMonitoringDictionaries", null, null).ConfigureAwait(false);
        }
    }
}
