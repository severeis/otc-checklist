﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Http;

namespace Anresh.Microservices.Infrastructure.Clients.Cfg
{
    /// <summary>
    /// Конфигуратор Http клиентов, использующий AppSettings.config.
    /// </summary>
    public class AppSettingsClientConfigurator : IHttpClientConfigurator
    {
        /// <summary>
        /// Имя настройки базового адреса клиента.
        /// </summary>
        private const string BaseAddressSettingName = "base-address";

        /// <summary>
        /// Имя настройки таймаута.
        /// </summary>
        private const string TimeoutSettingName = "timeout-seconds";

        /// <summary>
        /// Настройки приложения.
        /// </summary>
        //private AppSettings _appSettings;

        //public AppSettingsClientConfigurator()
        //{

        //}

        /// <summary>
        /// Конфигурирует HTTP клиент.
        /// </summary>
        /// <param name="url">Имя клиента.</param>
        /// <param name="client">Клиент.</param>
        public void Configure(string url, HttpClient client)
        {
            // TODO: validate and throw custom exceptions

            var baseAddress = new Uri(url);

            client.BaseAddress = baseAddress;

            var servicePoint = ServicePointManager.FindServicePoint(baseAddress);

            servicePoint.Expect100Continue = true;

            var timeoutSecondsString = string.Empty;// GetSettingValue(name, TimeoutSettingName);

            int timeoutSeconds;

            if (int.TryParse(timeoutSecondsString, out timeoutSeconds) && timeoutSeconds > 0)
            {
                var timeout = TimeSpan.FromTicks(timeoutSeconds * TimeSpan.TicksPerSecond);

                client.Timeout = timeout;
            }
        }

        /// <summary>
        /// Возвращает значение настройки.
        /// </summary>
        /// <param name="clientName">Имя клиента.</param>
        /// <param name="settingName">Название настройки.</param>
        /// <returns>Значение настройки.</returns>
        //private string GetSettingValue(string clientName, string settingName)
        //{
        //    return ConfigurationManager.AppSettings[string.Format("{0}:{1}", clientName, settingName)];
        //}
        private object GetPropValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }
    }
}
