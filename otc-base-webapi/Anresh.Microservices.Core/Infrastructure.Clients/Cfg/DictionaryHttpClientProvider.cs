﻿using System.Collections.Generic;
using System.Net.Http;

namespace Anresh.Microservices.Infrastructure.Clients.Cfg
{
    /// <summary>
    /// Поставщик HTTP клиентов, реализованный с помощью Dictionary.
    /// </summary>
    public class DictionaryHttpClientProvider : IHttpClientProvider
    {
        /// <summary>
        /// Объект для блокировки словаря клиентов.
        /// </summary>
        private readonly object _clientsLock = new object();

        /// <summary>
        /// Словарь клиентов.
        /// </summary>
        private readonly Dictionary<string, HttpClient> _clients = new Dictionary<string, HttpClient>();

        /// <summary>
        /// Конфигуратор HTTP клиентов.
        /// </summary>
        private readonly IHttpClientConfigurator _configurator;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="configurator">Конфигуратор HTTP клиентов.</param>
        public DictionaryHttpClientProvider(IHttpClientConfigurator configurator)
        {
            _configurator = configurator;
        }

        /// <summary>
        /// Возвращает или создает нового Http клиента.
        /// </summary>
        /// <param name="url">Имя клиента.</param>
        /// <returns>Клиент.</returns>
        public HttpClient GetOrCreate(string url)
        {
            lock (_clientsLock)
            {
                HttpClient client;

                if (!_clients.TryGetValue(url, out client))
                {
                    var handler = new HttpClientHandler
                    {
                        UseCookies = false
                    };

                    client = new HttpClient(handler);

                    _configurator.Configure(url, client);

                    _clients[url] = client;
                }

                return client;
            }
        }
    }
}
