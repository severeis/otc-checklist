﻿using System.Net.Http;

namespace Anresh.Microservices.Infrastructure.Clients.Cfg
{
    /// <summary>
    /// Конфигуратор HTTP клиентов.
    /// </summary>
    public interface IHttpClientConfigurator
    {
        /// <summary>
        /// Конфигурирует HTTP клиент.
        /// </summary>
        /// <param name="url">Имя клиента.</param>
        /// <param name="client">Клиент.</param>
        void Configure(string url, HttpClient client);
    }
}
