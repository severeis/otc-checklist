﻿using System.Net.Http;

namespace Anresh.Microservices.Infrastructure.Clients.Cfg
{
    /// <summary>
    /// Поставщик HTTP клиентов.
    /// </summary>
    public interface IHttpClientProvider
    {
        /// <summary>
        /// Возвращает или создает нового Http клиента.
        /// </summary>
        /// <param name="name">Имя клиента.</param>
        /// <returns>Клиент.</returns>
        HttpClient GetOrCreate(string name);
    }
}
