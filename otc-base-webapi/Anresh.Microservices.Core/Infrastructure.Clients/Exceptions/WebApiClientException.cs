﻿using System;
using System.Net;

namespace Anresh.Microservices.Infrastructure.Clients.Exceptions
{
    /// <summary>
    /// Ошибка клиента.
    /// </summary>
    public class WebApiClientException : ApplicationException
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="responseStatus">Статус.</param>
        /// <param name="message">Сообщение.</param>
        public WebApiClientException(HttpStatusCode responseStatus, string message)
            : base(message)
        {
            ResponseStatus = responseStatus;
        }

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="responseStatus">Статус.</param>
        /// <param name="message">Сообщение.</param>
        /// <param name="responseBody">Тело ответа.</param>
        public WebApiClientException(HttpStatusCode responseStatus, string message, string responseBody)
            : base(message)
        {
            ResponseStatus = responseStatus;
            ResponseBody = responseBody;
        }

        /// <summary>
        /// Статус.
        /// </summary>
        public HttpStatusCode ResponseStatus { get; private set; }

        /// <summary>
        /// Тело ответа.
        /// </summary>
        public string ResponseBody { get; private set; }
    }
}
