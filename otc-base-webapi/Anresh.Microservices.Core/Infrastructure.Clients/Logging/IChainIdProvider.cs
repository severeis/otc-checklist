﻿using System;

namespace Anresh.Microservices.Infrastructure.Clients.Logging
{
    /// <summary>
    /// Поставщик идентификатора цепочки вызовов.
    /// </summary>
    public interface IChainIdProvider
    {
        /// <summary>
        /// Возвращает идентификатор цепочки вызовов.
        /// </summary>
        string ChainId { get; }
    }
}
