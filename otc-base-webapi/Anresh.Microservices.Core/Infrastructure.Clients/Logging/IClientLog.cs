﻿using System;

namespace Anresh.Microservices.Infrastructure.Clients.Logging
{
    /// <summary>
    /// Логгер клиента.
    /// </summary>
    public interface IClientLog
    {
        /// <summary>
        /// Логирует успешный вызов.
        /// </summary>
        /// <param name="utcTimestamp">Метка времени.</param>
        /// <param name="clientName">Имя клиента.</param>
        /// <param name="uri">Uri метода клиента.</param>
        /// <param name="chainId">Идентификатор цепочки вызовов.</param>
        /// <param name="elapsedMilliseconds">Время операции.</param>
        void Success(DateTime utcTimestamp, string clientName, string uri, string chainId, long elapsedMilliseconds);

        /// <summary>
        /// Логирует вызов с ошибкой.
        /// </summary>
        /// <param name="utcTimestamp">Метка времени.</param>
        /// <param name="clientName">Имя клиента.</param>
        /// <param name="uri">Uri метода клиента.</param>
        /// <param name="chainId">Идентификатор цепочки вызовов.</param>
        /// <param name="elapsedMilliseconds">Время операции.</param>
        /// <param name="error">Ошибка.</param>
        void Error(DateTime utcTimestamp, string clientName, string uri, string chainId, long elapsedMilliseconds, Exception error);
    }
}
