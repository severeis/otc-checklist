﻿using System;
using System.Globalization;

using Microsoft.Extensions.Logging;

namespace Anresh.Microservices.Infrastructure.Clients.Logging
{
    /// <summary>
    /// Заглушка логгера клиента.
    /// </summary>
    public class Log4netClientLog : IClientLog
    {
        private readonly CultureInfo _cultureInfo = new CultureInfo(string.Empty);
        private readonly ILogger<Log4netClientLog> _logger;

        public Log4netClientLog(ILogger<Log4netClientLog> logger)
        {
            _cultureInfo.NumberFormat.NumberGroupSeparator = " ";
            _logger = logger;
        }

        /// <summary>
        /// Логирует успешный вызов (на самом деле нет).
        /// </summary>
        /// <param name="utcTimestamp">Метка времени.</param>
        /// <param name="clientName">Имя клиента.</param>
        /// <param name="uri">Uri метода клиента.</param>
        /// <param name="chainId">Идентификатор цепочки вызовов.</param>
        /// <param name="elapsedMilliseconds">Время операции.</param>
        public void Success(DateTime utcTimestamp, string clientName, string uri, string chainId, long elapsedMilliseconds)
        {
            var logMessage = string.Join(Environment.NewLine,
                                         GetHeader("SUCCEEDED", utcTimestamp, clientName, uri, chainId, elapsedMilliseconds));

            _logger.LogInformation(logMessage);
        }

        /// <summary>
        /// Логирует вызов с ошибкой (на самом деле нет).
        /// </summary>
        /// <param name="utcTimestamp">Метка времени.</param>
        /// <param name="clientName">Имя клиента.</param>
        /// <param name="uri">Uri метода клиента.</param>
        /// <param name="chainId">Идентификатор цепочки вызовов.</param>
        /// <param name="elapsedMilliseconds">Время операции.</param>
        /// <param name="error">Ошибка.</param>
        public void Error(DateTime utcTimestamp, string clientName, string uri, string chainId, long elapsedMilliseconds, Exception error)
        {
            var logMessage = string.Join(Environment.NewLine,
                                         GetHeader("FAILED", utcTimestamp, clientName, uri, chainId, elapsedMilliseconds),
                                         error);

            _logger.LogError(logMessage);
        }

        /// <summary>
        /// Возвращает заголовок сообщения в лог.
        /// </summary>
        /// <param name="caption">Заголовок.</param>
        /// <param name="utcTimestamp">Метка времени.</param>
        /// <param name="clientName">Имя клиента.</param>
        /// <param name="uri">Uri метода клиента.</param>
        /// <param name="chainId">Идентификатор цепочки вызовов.</param>
        /// <param name="elapsedMilliseconds">Время операции.</param>
        /// <returns>Заголовок.</returns>
        private string GetHeader(string caption, DateTime utcTimestamp, string clientName, string uri, string chainId, long elapsedMilliseconds)
        {
            return string.Format("{0}: {1}: chain {2}: ***{3}[{4}]***: {5} ms",
                                 caption,
                                 utcTimestamp.ToString("s"),
                                 chainId,
                                 clientName,
                                 uri,
                                 elapsedMilliseconds.ToString("n0", _cultureInfo));
        }
    }
}
