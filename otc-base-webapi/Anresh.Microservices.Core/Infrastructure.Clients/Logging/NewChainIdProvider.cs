﻿using System;

namespace Anresh.Microservices.Infrastructure.Clients.Logging
{
    /// <summary>
    /// Поставщик нового идентификатора цепочки вызовов.
    /// </summary>
    public class NewChainIdProvider : IChainIdProvider
    {
        /// <summary>
        /// Идентификатор цепочки вызовов.
        /// </summary>
        private readonly string _chainId = Guid.NewGuid().ToString();

        /// <summary>
        /// Возвращает идентификатор цепочки вызовов.
        /// </summary>
        public string ChainId
        {
            get { return _chainId; }
        }
    }
}
