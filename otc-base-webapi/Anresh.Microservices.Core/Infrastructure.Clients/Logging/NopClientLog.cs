﻿using System;

namespace Anresh.Microservices.Infrastructure.Clients.Logging
{
    /// <summary>
    /// Заглушка логгера клиента.
    /// </summary>
    public class NopClientLog : IClientLog
    {
        /// <summary>
        /// Логирует успешный вызов (на самом деле нет).
        /// </summary>
        /// <param name="utcTimestamp">Метка времени.</param>
        /// <param name="clientName">Имя клиента.</param>
        /// <param name="uri">Uri метода клиента.</param>
        /// <param name="chainId">Идентификатор цепочки вызовов.</param>
        /// <param name="elapsedMilliseconds">Время операции.</param>
        public void Success(DateTime utcTimestamp, string clientName, string uri, string chainId, long elapsedMilliseconds)
        {
        }

        /// <summary>
        /// Логирует вызов с ошибкой (на самом деле нет).
        /// </summary>
        /// <param name="utcTimestamp">Метка времени.</param>
        /// <param name="clientName">Имя клиента.</param>
        /// <param name="uri">Uri метода клиента.</param>
        /// <param name="chainId">Идентификатор цепочки вызовов.</param>
        /// <param name="elapsedMilliseconds">Время операции.</param>
        /// <param name="error">Ошибка.</param>
        public void Error(DateTime utcTimestamp, string clientName, string uri, string chainId, long elapsedMilliseconds, Exception error)
        {
        }
    }
}
