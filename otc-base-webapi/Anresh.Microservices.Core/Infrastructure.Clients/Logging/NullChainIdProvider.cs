﻿namespace Anresh.Microservices.Infrastructure.Clients.Logging
{
    /// <summary>
    /// Заглушка поставщика идентификатора цепочки вызовов.
    /// </summary>
    public class NullChainIdProvider : IChainIdProvider
    {
        /// <summary>
        /// Всегда возвращате null.
        /// </summary>
        public string ChainId
        {
            get { return null; }
        }
    }
}
