﻿namespace Anresh.Microservices.Infrastructure.Clients.Models
{
    /// <summary>
    /// Параметры получения разметки HTML со скриптами и стилями.
    /// </summary>
    public class GetPartialHtmlParam
    {
        /// <summary>
        /// Uri для получения разметки HTML.
        /// </summary>
        public string HtmlRequestUri { get; set; }

        /// <summary>
        /// Аргументы для получения разметки HTML.
        /// </summary>
        public object HtmlRequestArgs { get; set; }

        /// <summary>
        /// Uri для получения скриптов.
        /// </summary>
        public string ScriptsRequestUri { get; set; }

        /// <summary>
        /// Uri для получения стилей.
        /// </summary>
        public string StylesRequestUri { get; set; }
    }
}
