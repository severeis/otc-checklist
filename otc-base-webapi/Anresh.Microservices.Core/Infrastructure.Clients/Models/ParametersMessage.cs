﻿using System;

namespace Anresh.Microservices.Infrastructure.Clients.Models
{
    /// <summary>
    /// Сообщение с параметрами.
    /// </summary>
    /// <typeparam name="TParameters">Тип параметров.</typeparam>
    public abstract class ParametersMessage<TParameters>
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="eventName">Наименование события.</param>
        /// <param name="param">Параметры.</param>
        public ParametersMessage(string eventName, TParameters param)
        {
            EventDate = DateTime.UtcNow;
            EventName = eventName;
            Parameters = param;
        }

        /// <summary>
        /// Дата/время события.
        /// </summary>
        public DateTime EventDate { get; set; }

        /// <summary>
        /// Наименование события.
        /// </summary>
        public string EventName { get; set; }

        /// <summary>
        /// Идентификатор цепочки вызовов.
        /// </summary>
        public string ChainId { get; set; }

        /// <summary>
        /// Параметры.
        /// </summary>
        public TParameters Parameters { get; set; }
    }
}
