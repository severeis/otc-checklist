﻿namespace Anresh.Microservices.Infrastructure.Clients.Models
{
    /// <summary>
    /// Разметка HTML со скриптами и стилями.
    /// </summary>
    public class PartialHtml
    {
        /// <summary>
        /// Разметка HTML.
        /// </summary>
        public string Html { get; set; }

        /// <summary>
        /// Скрипты.
        /// </summary>
        public string Scripts { get; set; }

        /// <summary>
        /// Стили.
        /// </summary>
        public string Styles { get; set; }
    }
}
