﻿namespace Anresh.Microservices.Infrastructure.Clients.Models
{
    /// <summary>
    /// Файл.
    /// </summary>
    public class WebApiFile
    {
        /// <summary>
        /// Имя файла.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Содержимое.
        /// </summary>
        public byte[] Content { get; set; }
    }
}
