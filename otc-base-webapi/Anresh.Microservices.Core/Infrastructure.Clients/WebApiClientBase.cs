﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Anresh.Microservices.Infrastructure.Clients.Cfg;
using Anresh.Microservices.Infrastructure.Clients.Exceptions;
using Anresh.Microservices.Infrastructure.Clients.Logging;
using Anresh.Microservices.Infrastructure.Clients.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Anresh.Microservices.Infrastructure.Clients
{
    /// <summary>
    /// Базовый Http-клиент.
    /// </summary>
    public abstract class WebApiClientBase
    {
        /// <summary>
        /// Media type для JSON.
        /// </summary>
        private const string JsonMediaType = "application/json";

        /// <summary>
        /// Имя заголовка HTTP для передачи идентификатор цепочки вызовов.
        /// </summary>
        private const string ChainIdHeaderName = "X-Anresh-ChainId";

        /// <summary>
        /// Имя.
        /// </summary>
        private readonly string _name;

        /// <summary>
        /// HTTP клиент.
        /// </summary>
        private readonly HttpClient _httpClient;

        /// <summary>
        /// Поставщик идентификатора цепочки вызовов.
        /// </summary>
        private readonly IChainIdProvider _chain;

        /// <summary>
        /// Логгер.
        /// </summary>
        protected readonly IClientLog _log;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="name">Имя.</param>
        /// <param name="httpClientProvider">Поставщик HTTP клиентов.</param>
        /// <param name="chain">Поставщик идентификатора цепочки вызовов.</param>
        /// <param name="log">Логгер.</param>
        public WebApiClientBase(string name, IHttpClientProvider httpClientProvider, IChainIdProvider chain, IClientLog log)
        {
            _name = name;
            _httpClient = httpClientProvider.GetOrCreate(name);
            _chain = chain ?? new NullChainIdProvider();
            _log = log ?? new NopClientLog();
        }

        /// <summary>
        /// Выполняет запросы для получения разметки HTML со скриптами и стилями.
        /// </summary>
        /// <param name="param">Параметры.</param>
        /// <param name="token">Токен.</param>
        /// <returns>Разметка со скриптами и стилями.</returns>
        protected async Task<PartialHtml> GetPartialAsync(GetPartialHtmlParam param, string token)
        {
            var html = GetStringAsync(param.HtmlRequestUri, param.HtmlRequestArgs, token);

            var scripts = param.ScriptsRequestUri != null
                ? GetStringAsync(param.ScriptsRequestUri, null, token)
                : Task.FromResult<string>(null);

            var styles = param.StylesRequestUri != null
                ? GetStringAsync(param.StylesRequestUri, null, token)
                : Task.FromResult<string>(null);

            return new PartialHtml
            {
                Html = await html.ConfigureAwait(false),
                Scripts = await scripts.ConfigureAwait(false),
                Styles = await styles.ConfigureAwait(false)
            };
        }

        /// <summary>
        /// Выполняет GET запрос.
        /// </summary>
        /// <typeparam name="TResult">Тип результата.</typeparam>
        /// <param name="uri">Uri метода.</param>
        /// <param name="queryArgs">Аргументы.</param>
        /// <param name="token">Токен.</param>
        /// <returns>Результат.</returns>
        protected async Task<TResult> GetAsync<TResult>(string uri, object queryArgs, string token)
        {
            return await InvokeAsync(uri, () => CreateGetRequest(uri, queryArgs), HandleJsonResponse<TResult>, token).ConfigureAwait(false);
        }

        /// <summary>
        /// Выполняет GET запрос.
        /// </summary>
        /// <param name="uri">Uri метода.</param>
        /// <param name="queryArgs">Аргументы.</param>
        /// <param name="token">Токен.</param>
        /// <returns>Результат.</returns>
        protected async Task<string> GetStringAsync(string uri, object queryArgs, string token)
        {
            return await InvokeAsync(uri, () => CreateGetRequest(uri, queryArgs), HandleStringResponse, token).ConfigureAwait(false);
        }

        /// <summary>
        /// Выполняет GET запрос.
        /// </summary>
        /// <param name="uri">Uri метода.</param>
        /// <param name="queryArgs">Аргументы.</param>
        /// <param name="token">Токен.</param>
        /// <param name="cookies">Куки.</param>
        /// <returns>Результат.</returns>
        protected async Task<string> GetStringAsync(string uri, object queryArgs, string token, IRequestCookieCollection cookies)
        {
            return await InvokeAsync(uri, () => CreateGetRequest(uri, queryArgs, cookies), HandleStringResponse, token).ConfigureAwait(false);
        }

        /// <summary>
        /// Выполняет GET запрос.
        /// </summary>
        /// <param name="uri">Uri метода.</param>
        /// <param name="queryArgs">Аргументы.</param>
        /// <param name="token">Токен.</param>
        /// <returns>Результат.</returns>
        protected async Task<byte[]> GetBytesAsync(string uri, object queryArgs, string token)
        {
            return await InvokeAsync(uri, () => CreateGetRequest(uri, queryArgs), HandleBytesResponse, token).ConfigureAwait(false);
        }

        /// <summary>
        /// Выполняет GET запрос.
        /// </summary>
        /// <param name="uri">Uri метода.</param>
        /// <param name="queryArgs">Аргументы.</param>
        /// <param name="token">Токен.</param>
        /// <returns>Результат.</returns>
        protected async Task<WebApiFile> GetFileAsync(string uri, object queryArgs, string token)
        {
            return await InvokeAsync(uri, () => CreateGetRequest(uri, queryArgs), HandleFileResponse, token).ConfigureAwait(false);
        }

        /// <summary>
        /// Выполняет POST запрос.
        /// </summary>
        /// <typeparam name="TResult">Тип результата.</typeparam>
        /// <param name="uri">Uri метода.</param>
        /// <param name="param">Параметры.</param>
        /// <param name="token">Токен.</param>
        /// <returns>Результат.</returns>
        protected async Task<TResult> PostAsync<TResult>(string uri, object param, string token)
        {
            return await InvokeAsync(uri, () => CreateJsonPostRequest(uri, param), HandleJsonResponse<TResult>, token).ConfigureAwait(false);
        }

        /// <summary>
        /// Выполняет POST запрос.
        /// </summary>
        /// <param name="uri">Uri метода.</param>
        /// <param name="param">Параметры.</param>
        /// <param name="token">Токен.</param>
        /// <returns>Результат.</returns>
        protected async Task<string> PostAsync(string uri, object param, string token)
        {
            return await InvokeAsync(uri, () => CreateJsonPostRequest(uri, param), HandleStringResponse, token).ConfigureAwait(false);
        }

        /// <summary>
        /// Выполняет POST запрос.
        /// </summary>
        /// <typeparam name="TResult">Тип результата.</typeparam>
        /// <param name="uri">Uri метода.</param>
        /// <param name="token">Токен.</param>
        /// <returns>Результат.</returns>
        protected async Task<TResult> PostAsync<TResult>(string uri, string token)
        {
            return await InvokeAsync(uri, () => CreateEmptyPostRequest(uri), HandleJsonResponse<TResult>, token).ConfigureAwait(false);
        }

        /// <summary>
        /// Выполняет POST запрос.
        /// </summary>
        /// <param name="uri">Uri метода.</param>
        /// <param name="token">Токен.</param>
        /// <returns>Результат.</returns>
        protected async Task<string> PostAsync(string uri, string token)
        {
            return await InvokeAsync(uri, () => CreateEmptyPostRequest(uri), HandleStringResponse, token).ConfigureAwait(false);
        }

        /// <summary>
        /// Выполняет POST запрос.
        /// </summary>
        /// <param name="uri">Uri метода.</param>
        /// <param name="param">Параметры.</param>
        /// <param name="token">Токен.</param>
        /// <returns>Результат.</returns>
        protected async Task<WebApiFile> PostForFileAsync(string uri, object param, string token)
        {
            return await InvokeAsync(uri, () => CreateJsonPostRequest(uri, param), HandleFileResponse, token).ConfigureAwait(false);
        }

        /// <summary>
        /// Выполняет POST запрос.
        /// </summary>
        /// <param name="uri">Uri метода.</param>
        /// <param name="param">Параметры.</param>
        /// <param name="token">Токен.</param>
        /// <returns>Задача.</returns>
        protected async Task PostNoResultAsync(string uri, object param, string token)
        {
            await InvokeAsync(uri, () => CreateJsonPostRequest(uri, param), HandleNoContentResponse, token).ConfigureAwait(false);
        }

        /// <summary>
        /// Выполняет POST запрос.
        /// </summary>
        /// <param name="uri">Uri метода.</param>
        /// <param name="token">Токен.</param>
        /// <returns>Задача.</returns>
        protected async Task PostNoResultAsync(string uri, string token)
        {
            await InvokeAsync(uri, () => CreateEmptyPostRequest(uri), HandleNoContentResponse, token).ConfigureAwait(false);
        }

        /// <summary>
        /// Создает GET запрос.
        /// </summary>
        /// <param name="uri">Uri метода.</param>
        /// <param name="queryArgs">Аргументы.</param>
        /// <returns>Запрос.</returns>
        private HttpRequestMessage CreateGetRequest(string uri, object queryArgs)
        {
            var requestUri = uri + ToQueryString(queryArgs);

            return new HttpRequestMessage
            {
                RequestUri = new Uri(_httpClient.BaseAddress, requestUri),
                Method = HttpMethod.Get
            };
        }

        /// <summary>
        /// Создает GET запрос.
        /// </summary>
        /// <param name="uri">Uri метода.</param>
        /// <param name="queryArgs">Аргументы.</param>
        /// <param name="cookies">Куки.</param>
        /// <returns>Запрос.</returns>
        private HttpRequestMessage CreateGetRequest(string uri, object queryArgs, IRequestCookieCollection cookies)
        {
            var requestUri = uri + ToQueryString(queryArgs);

            var message = new HttpRequestMessage
            {
                RequestUri = new Uri(_httpClient.BaseAddress, requestUri),
                Method = HttpMethod.Get
            };

            var cookiesHeaderValue = string.Join("; ", cookies.Keys
                                                              .Select(name => string.Format("{0}={1}", name, cookies[name])));

            message.Headers.Add("Cookie", cookiesHeaderValue);

            return message;
        }

        /// <summary>
        /// Создает POST запрос с JSON телом.
        /// </summary>
        /// <param name="uri">Uri метода.</param>
        /// <param name="param">Параметры.</param>
        /// <returns>Запрос.</returns>
        private HttpRequestMessage CreateJsonPostRequest(string uri, object param)
        {
            var paramJson = JsonConvert.SerializeObject(param);

            var content = new StringContent(paramJson, Encoding.UTF8, JsonMediaType);

            return new HttpRequestMessage
            {
                RequestUri = new Uri(_httpClient.BaseAddress, uri),
                Method = HttpMethod.Post,
                Content = content
            };
        }

        /// <summary>
        /// Создает POST запрос без тела.
        /// </summary>
        /// <param name="uri">Uri метода.</param>
        /// <returns>Запрос.</returns>
        private HttpRequestMessage CreateEmptyPostRequest(string uri)
        {
            return new HttpRequestMessage
            {
                RequestUri = new Uri(_httpClient.BaseAddress, uri),
                Method = HttpMethod.Post
            };
        }

        /// <summary>
        /// Обрабатывает ответ с JSON телом.
        /// </summary>
        /// <typeparam name="TResult">Тип результата.</typeparam>
        /// <param name="response">Ответ.</param>
        /// <returns>Результат.</returns>
        private async Task<TResult> HandleJsonResponse<TResult>(HttpResponseMessage response)
        {
            await ValidateResponseStatus(response, HttpStatusCode.OK);

            var resultJson = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

            var result = JsonConvert.DeserializeObject<TResult>(resultJson);

            return result;
        }

        /// <summary>
        /// Обрабатывает ответ со строкой в теле.
        /// </summary>
        /// <param name="response">Ответ.</param>
        /// <returns>Строка.</returns>
        private async Task<string> HandleStringResponse(HttpResponseMessage response)
        {
            await ValidateResponseStatus(response, HttpStatusCode.OK);

            var result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

            return result;
        }

        /// <summary>
        /// Обрабатывает ответ с массивом байтов в теле.
        /// </summary>
        /// <param name="response">Ответ.</param>
        /// <returns>Строка.</returns>
        private async Task<byte[]> HandleBytesResponse(HttpResponseMessage response)
        {
            await ValidateResponseStatus(response, HttpStatusCode.OK);

            var result = await response.Content.ReadAsByteArrayAsync().ConfigureAwait(false);

            return result;
        }

        /// <summary>
        /// Обрабатывает ответ с файлом в теле.
        /// </summary>
        /// <param name="response">Ответ.</param>
        /// <returns>Файл.</returns>
        private async Task<WebApiFile> HandleFileResponse(HttpResponseMessage response)
        {
            await ValidateResponseStatus(response, HttpStatusCode.OK);

            var fileName = response.Content.Headers.ContentDisposition != null
                ? response.Content.Headers.ContentDisposition.FileName
                : null;

            var content = await response.Content.ReadAsByteArrayAsync().ConfigureAwait(false);

            return new WebApiFile
            {
                FileName = fileName,
                Content = content
            };
        }

        /// <summary>
        /// Обрабатывает ответ без тела.
        /// </summary>
        /// <param name="response">Ответ.</param>
        /// <returns>Задача.</returns>
        private async Task HandleNoContentResponse(HttpResponseMessage response)
        {
            await ValidateResponseStatus(response, HttpStatusCode.NoContent, HttpStatusCode.OK);
        }

        /// <summary>
        /// Проверяет статус ответа.
        /// </summary>
        /// <param name="response">Ответ.</param>
        /// <param name="validStatusCodes">Корректные статусы.</param>
        /// <returns>Задача.</returns>
        private async Task ValidateResponseStatus(HttpResponseMessage response, params HttpStatusCode[] validStatusCodes)
        {
            if (response.StatusCode != HttpStatusCode.NoContent &&
                response.StatusCode != HttpStatusCode.OK)
            {
                var responseBody = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

                var message = string.Format("некорректный HTTP статус ответа {0}, ожидался [{1}]",
                                            response.StatusCode,
                                            string.Join(", ", validStatusCodes));

                throw new WebApiClientException(response.StatusCode, message, responseBody);
            }
        }

        /// <summary>
        /// Выполняет запрос.
        /// </summary>
        /// <param name="uri">Uri метода.</param>
        /// <param name="createRequest">Функция, создающая запрос.</param>
        /// <param name="handleResponse">Функция, обрабатывающая ответ.</param>
        /// <param name="token">Токен.</param>
        /// <returns>Задача.</returns>
        private async Task InvokeAsync(string uri, Func<HttpRequestMessage> createRequest, Func<HttpResponseMessage, Task> handleResponse, string token)
        {
            await InvokeAsync(uri, createRequest, async response => { await handleResponse(response).ConfigureAwait(false); return true; }, token).ConfigureAwait(false);
        }

        /// <summary>
        /// Выполняет запрос.
        /// </summary>
        /// <typeparam name="TResult">Тип результата.</typeparam>
        /// <param name="uri">Uri метода.</param>
        /// <param name="createRequest">Функция, создающая запрос.</param>
        /// <param name="handleResponse">Функция, обрабатывающая ответ.</param>
        /// <param name="token">Токен.</param>
        /// <returns>Результат.</returns>
        private async Task<TResult> InvokeAsync<TResult>(string uri, Func<HttpRequestMessage> createRequest, Func<HttpResponseMessage, Task<TResult>> handleResponse, string token)
        {
            var chainId = _chain != null ? _chain.ChainId : null;

            var isSucceeded = true;

            var utcTimestamp = DateTime.UtcNow;

            var stopwatch = Stopwatch.StartNew();

            try
            {
                using (var request = createRequest())
                {
                    request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    if (!string.IsNullOrEmpty(token))
                    {
                        request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    }

                    if (chainId != null)
                    {
                        request.Headers.Add(ChainIdHeaderName, chainId.ToString());
                    }

                    using (var response = await _httpClient.SendAsync(request).ConfigureAwait(false))
                    {
                        var result = await handleResponse(response).ConfigureAwait(false);

                        return result;
                    }
                }
            }
            catch (Exception e)
            {
                stopwatch.Stop();

                isSucceeded = false;

                _log.Error(utcTimestamp, _name, uri, chainId, stopwatch.ElapsedMilliseconds, e);

                throw;
            }
            finally
            {
                if (isSucceeded)
                {
                    stopwatch.Stop();

                    _log.Success(utcTimestamp, _name, uri, chainId, stopwatch.ElapsedMilliseconds);
                }
            }
        }

        /// <summary>
        /// Возвращает значение настройки.
        /// </summary>
        /// <param name="settingName">Название настройки.</param>
        /// <returns>Значение настройки.</returns>
        //private string GetSettingValue(string settingName)
        //{
        //    return ConfigurationManager.AppSettings[string.Format("{0}:{1}", _name, settingName)];
        //}

        /// <summary>
        /// Преобразовать параметры в строку query.
        /// </summary>
        /// <param name="args">Параметры.</param>
        /// <returns>Строка query.</returns>
        private string ToQueryString(object args)
        {
            if (args == null)
            {
                return null;
            }

            return "?" + ToQueryString(args, string.Empty);
        }

        /// <summary>
        /// Преобразовать параметры в строку query.
        /// </summary>
        /// <param name="args">Параметры.</param>
        /// <param name="prefix">Префикс имен.</param>
        /// <returns>Строка query.</returns>
        private string ToQueryString(object args, string prefix)
        {
            if (args == null)
            {
                return null;
            }

            var queryArgs = args.GetType()
                                .GetProperties()
                                .Select(p => ToQueryArgument(args, p, prefix))
                                .Where(p => p != null)
                                .ToList();

            return string.Join("&", queryArgs);
        }

        /// <summary>
        /// Преобразует поле в аргумент query.
        /// </summary>
        /// <param name="args">Параметры.</param>
        /// <param name="property">Поле.</param>
        /// <param name="prefix">Префикс имен.</param>
        /// <returns>Аргумент query.</returns>
        private string ToQueryArgument(object args, PropertyInfo property, string prefix)
        {
            var name = property.Name.ToLower();

            var value = property.GetValue(args);

            if (property.PropertyType.IsConstructedGenericType &&
                property.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
            {
                var listToQueryArguments = typeof(WebApiClientBase).GetMethod("ListToQueryArguments", BindingFlags.NonPublic | BindingFlags.Instance)
                                                                   .MakeGenericMethod(property.PropertyType.GenericTypeArguments);

                return (string) listToQueryArguments.Invoke(this, new object[] { HttpUtility.UrlEncode(prefix + name), value });
            }

            if (property.PropertyType.IsClass &&
                property.DeclaringType.Assembly == property.PropertyType.Assembly)
            {
                return ToQueryString(value, name + ".");
            }

            return
                HttpUtility.UrlEncode(prefix + name) +
                "=" +
                (value != null ? HttpUtility.UrlEncode(ToQueryArgumentValue(value, property.PropertyType)) : null);
        }

        /// <summary>
        /// Преобразует список в аргументы query.
        /// </summary>
        /// <typeparam name="T">Тип элемента списка.</typeparam>
        /// <param name="name">Имя аргумента.</param>
        /// <param name="values">Список.</param>
        /// <returns>Аргументы query.</returns>
        private string ListToQueryArguments<T>(string name, List<T> values)
        {
            return string.Join("&", values.Select(value => HttpUtility.UrlEncode(name) + "=" + HttpUtility.UrlEncode(ToQueryArgumentValue(value, typeof(T)))));
        }

        /// <summary>
        /// Преобразует значение в аргумент query.
        /// </summary>
        /// <param name="value">Значение.</param>
        /// <param name="type">Тип значения.</param>
        /// <returns>Аргумент query.</returns>
        private string ToQueryArgumentValue(object value, Type type)
        {
            if (type == typeof(DateTime))
            {
                return ((DateTime) value).ToString("s");
            }

            if (type == typeof(decimal))
            {
                return ((decimal)value).ToString(CultureInfo.InvariantCulture);
            }

            if(value == null)
            {
                return string.Empty;
            }

            return value.ToString();
        }
    }
}
