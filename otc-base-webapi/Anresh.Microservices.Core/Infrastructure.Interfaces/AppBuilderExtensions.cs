﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

using Anresh.Microservices.Clients.Common;
using Anresh.Microservices.Infrastructure.Clients.Cfg;
using Anresh.Microservices.Infrastructure.Clients.Logging;
using Anresh.Microservices.Infrastructure.Interfaces.Authentication;

using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication.JwtBearer;

using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Anresh.Microservices.Infrastructure.Interfaces
{
    public static class AppBuilderExtensions
    {
        public readonly static string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
        private const string DOCUMENTATION_FOLDER_PATH = "Documentation";
        private const string DOCUMENTATION_FILE_NAME_TEMPLATE = "*.xml";

        /// <summary>
        /// Настраивает микросервис.
        /// </summary>
        /// <param name="app">Приложение.</param>
        /// <param name="container">Контейнер.</param>
        public static void UseMicroservice(this IServiceCollection services, IConfigurationSection configurationSection)
        {
            services.Configure<CommonSettings>(configurationSection);

            services.AddTransient<ITokenClient, TokenClient>();
            services.AddSingleton<IHttpClientProvider, DictionaryHttpClientProvider>();
            services.AddSingleton<IHttpClientConfigurator, AppSettingsClientConfigurator>();
            services.AddSingleton<IClientLog, Log4netClientLog>();

            services.AddSingleton<ISecurityTokenValidator, OtcTokenValidator>();

            if (!services.Contains(new ServiceDescriptor(typeof(IChainIdProvider), typeof(NewChainIdProvider), ServiceLifetime.Scoped)))
            {
                services.AddScoped<IChainIdProvider, NewChainIdProvider>();
            }
            
            var sp = services.BuildServiceProvider();
            var commonSettings = sp.GetService<IOptions<CommonSettings>>().Value;

            var corsOrigins = commonSettings.SiteOrigin?.Split(",");

            services.AddCors(options =>
            {
                options.AddPolicy(MyAllowSpecificOrigins,
                    builder =>
                    {
                        builder.WithOrigins(corsOrigins);
                        builder.AllowAnyHeader();
                    });
            });

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.SecurityTokenValidators.Add(sp.GetService<ISecurityTokenValidator>());
                });
        }

        public static void AddSwagger(this IServiceCollection services)
        {
            services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();

            services.AddApiVersioning(opts => opts.ReportApiVersions = true);
            services.AddVersionedApiExplorer(opts =>
            {
                // add the versioned api explorer, which also adds IApiVersionDescriptionProvider service
                // note: the specified format code will format the version as "'v'major[.minor][-status]"
                opts.GroupNameFormat = "'v'VVV";

                // note: this option is only necessary when versioning by url segment. the SubstitutionFormat
                // can also be used to control the format of the API version in route templates
                opts.SubstituteApiVersionInUrl = true;
            });

            services.AddSwaggerGen(o =>
            {
                var documentationFolder = Path.Combine(AppContext.BaseDirectory, AppBuilderExtensions.DOCUMENTATION_FOLDER_PATH);
                if (Directory.Exists(documentationFolder))
                {
                    Directory.GetFiles(documentationFolder, AppBuilderExtensions.DOCUMENTATION_FILE_NAME_TEMPLATE).ToList()
                        .ForEach(docFile => o.IncludeXmlComments(docFile));
                }

                o.AddSecurityDefinition("Bearer", new ApiKeyScheme()
                {
                    Description = "Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });

                o.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
                {
                    { "Bearer", Enumerable.Empty<string>() }
                });
            });
        }

        public static void UseSwagger(this IApplicationBuilder app, IApiVersionDescriptionProvider provider)
        {
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                foreach (var description in provider.ApiVersionDescriptions)
                {
                    c.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json", description.GroupName.ToUpperInvariant());
                }
            });
        }
    }
}