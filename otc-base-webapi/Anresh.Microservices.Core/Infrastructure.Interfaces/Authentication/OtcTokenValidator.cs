﻿using Anresh.Microservices.Clients.Common;
using Anresh.Microservices.Clients.Common.Models.Token;
using Anresh.Microservices.Infrastructure.Services.Helpers;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Runtime.Caching;
using System.Security.Claims;

namespace Anresh.Microservices.Infrastructure.Interfaces.Authentication
{
    /// <summary>
    /// OtcTokenValidator.
    /// </summary>
    public class OtcTokenValidator : ISecurityTokenValidator
    {
        /// <summary>
        /// Имя кэша токенов.
        /// </summary>
        private const string OtcTokenFromCookieCacheName = "OtcTokenFromCookieCache";

        /// <summary>
        /// Кэш токенов.
        /// </summary>
        private MemoryCache _tokenCache = new MemoryCache(OtcTokenFromCookieCacheName);

        /// <summary>
        /// Настройки приложения.
        /// </summary>
        private CommonSettings _appSettings;

        /// <summary>
        /// Клиент для работы с токенами.
        /// </summary>
        private ITokenClient _tokenClient;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="options">Настройки приложения.</param>
        /// <param name="tokenClient">Клиент для работы с токенами.</param>
        public OtcTokenValidator(IOptions<CommonSettings> options,
            ITokenClient tokenClient)
        {
            _appSettings = options.Value;
            _tokenClient = tokenClient;
        }

        public bool CanValidateToken => true;

        public int MaximumTokenSizeInBytes { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public bool CanReadToken(string securityToken)
        {
            return true;
        }

        public ClaimsPrincipal ValidateToken(string securityToken, TokenValidationParameters validationParameters, out SecurityToken validatedToken)
        {
            validatedToken = (TokenInfo)_tokenCache.Get(securityToken);

            if (validatedToken == null)
            {
                validatedToken = _tokenClient.GetInfoAsync(securityToken).Result;
            }

            var now = DateTime.UtcNow;

            if (validatedToken == null || validatedToken.ValidTo < now)
            {
                return new ClaimsPrincipal();
            }

            var expiration = now + _appSettings.TokenSlidingExpiration.ToTimeSpanFromSeconds();

            if (expiration > validatedToken.ValidTo)
            {
                expiration = validatedToken.ValidTo;
            }

            _tokenCache.Set(securityToken, validatedToken, expiration);

            var tokenInfo = validatedToken as TokenInfo;

            var identity = AuthHelper.CreateIdentity(securityToken, tokenInfo.UserId, tokenInfo.OrganizationId, tokenInfo.Thumbprint);

            return new ClaimsPrincipal(identity);
        }
    }
}
