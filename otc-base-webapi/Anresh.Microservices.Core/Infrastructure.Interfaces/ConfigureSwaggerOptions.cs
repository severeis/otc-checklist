﻿using System;

using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc.ApiExplorer;

using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Anresh.Microservices.Infrastructure.Interfaces
{
    internal sealed class ConfigureSwaggerOptions : IConfigureOptions<SwaggerGenOptions>
    {
        readonly IApiVersionDescriptionProvider _provider;

        public ConfigureSwaggerOptions(IApiVersionDescriptionProvider provider) => this._provider = provider;

        public void Configure(SwaggerGenOptions options)
        {
            foreach (var description in this._provider.ApiVersionDescriptions)
            {
                options.SwaggerGeneratorOptions.SwaggerDocs.Add(description.GroupName, ConfigureSwaggerOptions.CreateInfoForApiVersion(description));
            }
        }

        private static Info CreateInfoForApiVersion(ApiVersionDescription description)
        {
            var info = new Info()
            {
                Title = "Microservice WebApi",
                Version = description.ApiVersion.ToString(),
                Description = "Microservice WebApi." + (description.IsDeprecated ? " This API version has been deprecated." : string.Empty)
            };

            return info;
        }
    }
}