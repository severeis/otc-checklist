﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading;

using Anresh.Microservices.Infrastructure.Services.Models;
using Anresh.Microservices.Infrastructure.Services.Models.Sign;

namespace Anresh.Microservices.Infrastructure.Services.Helpers
{
    /// <summary>
    /// Helper для работы с аутентификацией.
    /// </summary>
    public static class AuthHelper
    {
        /// <summary>
        /// Тип аутентификации.
        /// </summary>
        public const string AuthenticationType = "OtcTokenAuth";

        /// <summary>
        /// Ключ контекста для хранения токена.
        /// </summary>
        public static string OtcTokenKey
        {
            get { return "OtcToken"; }
        }

        /// <summary>
        /// Текущий пользователь.
        /// </summary>
        public static ClaimsIdentity CurrentIdentity
        {
            get
            {
                var principal = Thread.CurrentPrincipal;

                if (principal == null)
                {
                    return null;
                }

                return principal.Identity as ClaimsIdentity;
            }
        }

        /// <summary>
        /// Текущий токен.
        /// </summary>
        public static string CurrentToken
        {
            get { return GetClaimValue(OtcClaimTypes.Token); }
        }

        /// <summary>
        /// Идентификатор текущего пользователя.
        /// </summary>
        public static int? CurrentUserId
        {
            get { return GetClaimValue(OtcClaimTypes.UserId, ModelExtensions.TryToInteger); }
        }

        /// <summary>
        /// Идентификатор текущей организации.
        /// </summary>
        public static int? CurrentOrganizationId
        {
            get { return GetClaimValue(OtcClaimTypes.OrganizationId, ModelExtensions.TryToInteger); }
        }

        /// <summary>
        /// Отпечаток сертификата.
        /// </summary>
        public static string CurrentThumbprint
        {
            get { return GetClaimValue(OtcClaimTypes.Thumbprint); }
        }

        /// <summary>
        /// Информация о подписанте.
        /// </summary>
        public static SignUserInfo CurrentSignUserInfo
        {
            get
            {
                var userId = CurrentUserId;
                var organizationId = CurrentOrganizationId;
                var thumbprint = CurrentThumbprint;

                if (!userId.HasValue || !organizationId.HasValue || thumbprint == null)
                {
                    return null;
                }

                return new SignUserInfo
                {
                    UserId = userId.Value,
                    OrganizationId = organizationId.Value,
                    Thumbprint = thumbprint
                };
            }
        }

        /// <summary>
        /// Создает и возвращает identity.
        /// </summary>
        /// <param name="token">Токен.</param>
        /// <param name="userId">Идентификатор пользователя.</param>
        /// <param name="organizationId">Идентификатор организации.</param>
        /// <param name="thumbprint">Отпечаток сертификата.</param>
        /// <returns>Identity.</returns>
        public static ClaimsIdentity CreateIdentity(string token, int userId, int organizationId, string thumbprint)
        {
            var claims = new[]
            {
                new Claim(OtcClaimTypes.Token, token),
                new Claim(OtcClaimTypes.UserId, userId.ToString()),
                new Claim(OtcClaimTypes.OrganizationId, organizationId.ToString()),
                new Claim(OtcClaimTypes.Thumbprint, thumbprint)
            };

            return new ClaimsIdentity(claims, AuthenticationType);
        }

        /// <summary>
        /// Возвращает значение claim-а.
        /// </summary>
        /// <typeparam name="T">Тип значения.</typeparam>
        /// <param name="claimType">Тип claim-а.</param>
        /// <param name="map">Функция преобразования.</param>
        /// <returns>Значение.</returns>
        private static T GetClaimValue<T>(string claimType, Func<string, T> map)
        {
            return map(GetClaimValue(claimType));
        }

        /// <summary>
        /// Возвращает значение claim-а.
        /// </summary>
        /// <param name="claimType">Тип claim-а.</param>
        /// <returns>Значение.</returns>
        private static string GetClaimValue(string claimType)
        {
            var identity = CurrentIdentity;

            if (identity == null || identity.Claims == null)
            {
                return null;
            }

            var claim = identity.Claims.FirstOrDefault(c => c.Type == claimType);

            if (claim == null)
            {
                return null;
            }

            return claim.Value;
        }
    }
}
