﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Services.Helpers
{
    namespace BaseWebAPI.CommonServices.Helpers
    {
        public static class JsonObjectExtensions
        {
            private static JsonSerializerSettings GetJsonSettings(bool typed, bool ignoreNullValues = true)
            {
                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = ignoreNullValues ? NullValueHandling.Ignore : NullValueHandling.Include,
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                };
                if (typed)
                {
                    settings.TypeNameHandling = TypeNameHandling.Objects;
                }

                settings.Converters.Add(new StringEnumConverter());
                settings.Converters.Add(new IsoDateTimeConverterLocalized());
                return settings;
            }

            public static void ToJson(this object value, TextWriter textWriter, bool ignoreNullValues = true)
            {
                JsonSerializerSettings settings = GetJsonSettings(false, ignoreNullValues);
                JsonSerializer.Create(settings).Serialize(textWriter, value);
            }

            public static void ToTypedJson(this object value, TextWriter textWriter)
            {
                JsonSerializerSettings settings = GetJsonSettings(true);
                JsonSerializer.Create(settings).Serialize(textWriter, value);
            }

            public static string ToJson(this object value, bool ignoreNullValues = true)
            {
                var stringWriter = new StringWriter();
                value.ToJson(stringWriter, ignoreNullValues);
                return stringWriter.ToString();
            }

            public static string ToJsonOrDefault(this object value, string defaultValue)
            {
                if (value == null)
                {
                    return defaultValue;
                }

                var stringWriter = new StringWriter();
                value.ToJson(stringWriter, true);
                return stringWriter.ToString();
            }

            public static string ToJsonNullAsEmpty(this object value)
            {
                if (value == null)
                {
                    return string.Empty;
                }

                return value.ToJson();
            }

            public static string ToTypedJson(this object value)
            {
                var stringWriter = new StringWriter();
                value.ToTypedJson(stringWriter);
                return stringWriter.ToString();
            }

            public static T FromJson<T>(this TextReader @textReader) where T : class
            {
                JsonSerializerSettings settings = GetJsonSettings(false);
                object obj = JsonSerializer.Create(settings).Deserialize(textReader, typeof(T));
                return obj as T;
            }

            public static T FromTypedJson<T>(this TextReader @textReader) where T : class
            {
                JsonSerializerSettings settings = GetJsonSettings(true);
                object obj = JsonSerializer.Create(settings).Deserialize(textReader, typeof(T));
                return obj as T;
            }

            public static T FromJson<T>(this string @value) where T : class
            {
                var stringReader = new StringReader(value);
                var obj = stringReader.FromJson<T>();
                return obj;
            }

            public static T FromJson<T>(this string @value, T defaultValue) where T : class
            {
                if (string.IsNullOrEmpty(@value))
                {
                    return defaultValue;
                }

                var stringReader = new StringReader(value);
                var obj = stringReader.FromJson<T>();
                return obj;
            }

            public static T FromTypedJson<T>(this string @value) where T : class
            {
                var stringReader = new StringReader(value);
                var obj = stringReader.FromTypedJson<T>();
                return obj;
            }

            public static T FromJsonToValueType<T>(this string @value)
            {
                var stringReader = new StringReader(value);
                var settings = GetJsonSettings(false);
                var obj = JsonSerializer.Create(settings).Deserialize(stringReader, typeof(T));

                return (T)obj;
            }

            private class IsoDateTimeConverterLocalized : IsoDateTimeConverter
            {
                public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
                {
                    try
                    {
                        return base.ReadJson(reader, objectType, existingValue, serializer);
                    }
                    catch (FormatException e)
                    {
                        throw new FormatException("Ошибка при вводе даты, введенные данные не являются корректной датой.", e);
                    }
                }
            }
        }
    }
}
