﻿using System;
using System.Globalization;

namespace Anresh.Microservices.Infrastructure.Services.Helpers
{
    /// <summary>
    /// Расширения моделей.
    /// </summary>
    public static class ModelExtensions
    {
        /// <summary>
        /// Преобразует строку в булевское значение.
        /// </summary>
        /// <param name="value">Строка.</param>
        /// <returns>Булевское значение.</returns>
        public static bool ToBoolean(this string value)
        {
            bool boolValue;

            return bool.TryParse(value, out boolValue) && boolValue;
        }

        /// <summary>
        /// Преобразует строку в число.
        /// </summary>
        /// <param name="value">Строка.</param>
        /// <returns>Число.</returns>
        public static int? TryToInteger(this string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            int intValue;

            return int.TryParse(value, out intValue)
                ? intValue
                : (int?)null;
        }

        /// <summary>
        /// Преобразует строку в число.
        /// </summary>
        /// <param name="value">Строка.</param>
        /// <returns>Число.</returns>
        public static int ToInteger(this string value)
        {
            return int.Parse(value);
        }

        /// <summary>
        /// Преобразует строку в число.
        /// </summary>
        /// <param name="value">Строка.</param>
        /// <returns>Число.</returns>
        public static decimal? TryToDecimal(this string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            decimal result;

            var style = NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands | NumberStyles.Float;
            var culture = CultureInfo.CreateSpecificCulture("ru-RU");

            if (!decimal.TryParse(value.ReplaceDotToComma(), style, culture, out result))
            {
                return null;
            }

            return result;
        }

        /// <summary>
        /// Преобразует строку в число.
        /// </summary>
        /// <param name="value">Строка.</param>
        /// <returns>Число.</returns>
        public static decimal ToDecimal(this string value)
        {
            var style = NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands | NumberStyles.Float;
            var culture = CultureInfo.CreateSpecificCulture("ru-RU");

            return decimal.Parse(value.ReplaceDotToComma(), style, culture);
        }

        /// <summary>
        /// Преобразует строку в число.
        /// </summary>
        /// <param name="value">Строка.</param>
        /// <returns>Число.</returns>
        public static decimal ToDecimalInvariant(this string value)
        {
            return decimal.Parse(value, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Преобразует строку с секундами в интервал.
        /// </summary>
        /// <param name="value">Строка с секундами.</param>
        /// <returns>Интервал.</returns>
        public static TimeSpan ToTimeSpanFromSeconds(this string value)
        {
            return TimeSpan.FromTicks(int.Parse(value) * TimeSpan.TicksPerSecond);
        }

        /// <summary>
        /// Заменят точки на запятые.
        /// </summary>
        /// <param name="value">Строка.</param>
        /// <returns>Модифицированная строка.</returns>
        public static string ReplaceDotToComma(this string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            return value.Replace(".", ",");
        }
    }
}
