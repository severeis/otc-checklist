﻿namespace Anresh.Microservices.Infrastructure.Services.Models
{
    /// <summary>
    /// Типы Claim-ов Otc.
    /// </summary>
    public static class OtcClaimTypes
    {
        /// <summary>
        /// Токен.
        /// </summary>
        public const string Token = "OtcToken";

        /// <summary>
        /// Идентификатор пользователя.
        /// </summary>
        public const string UserId = "OtcUserId";

        /// <summary>
        /// Идентификатор организации.
        /// </summary>
        public const string OrganizationId = "OtcOrganizationId";

        /// <summary>
        /// Отпечаток сертификата.
        /// </summary>
        public const string Thumbprint = "OtcThumbprint";
    }
}
