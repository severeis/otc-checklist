﻿using System;
using System.Collections.Generic;

namespace Anresh.Microservices.Infrastructure.Services.Models.Sign
{
    /// <summary>
    /// Сервисная модель ЭЦП.
    /// </summary>
    public class DigitalSign
    {
        /// <summary>
        /// Простая электронная подпись.
        /// </summary>
        public DigitalSignatureType SignatureType { get; set; }

        /// <summary>
        /// Данные для подписания.
        /// </summary>
        public byte[] Data { get; set; }

        /// <summary>
        /// Подпись.
        /// </summary>
        public string Sign { get; set; }

        /// <summary>
        /// Подписи файлов.
        /// </summary>
        public Dictionary<Guid, string> FileSign { get; set; }
    }
}
