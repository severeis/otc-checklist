﻿namespace Anresh.Microservices.Infrastructure.Services.Models.Sign
{
    /// <summary>
    /// Тип цифровой подписи.
    /// </summary>
    public enum DigitalSignatureType
    {
        /// <summary>
        /// Неизвестно.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Квалифицированная электронная подпись.
        /// </summary>
        Qualified = 1,

        /// <summary>
        /// Простая электронная подпись.
        /// </summary>
        Simple = 2
    }
}
