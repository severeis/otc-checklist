﻿using System;

namespace Anresh.Microservices.Infrastructure.Services.Models.Sign
{
    /// <summary>
    /// Цифровая подпись файла.
    /// </summary>
    [Serializable]
    public class FileDigitalSignatureInfo
    {
        /// <summary>
        /// Идентификатор файла.
        /// </summary>
        public Guid Guid { get; set; }

        /// <summary>
        /// Хэш файла по алгоритму ГОСТ Р 34.11-94.
        /// </summary>
        public string Hash { get; set; }

        /// <summary>
        /// Хэш файла по алгоритму SHA1.
        /// </summary>
        public string HashSHA1 { get; set; }

        /// <summary>
        /// Открепленная цифровая подпись.
        /// </summary>
        public string DigitalSign { get; set; }
    }
}
