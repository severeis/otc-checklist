﻿namespace Anresh.Microservices.Infrastructure.Services.Models.Sign
{
    /// <summary>
    /// Модель представления данных для подписания.
    /// </summary>
    public class SignData
    {
        /// <summary>
        /// Простая электронная подпись.
        /// </summary>
        public DigitalSignatureType SignatureType { get; set; }

        /// <summary>
        /// Подпись открепленная.
        /// </summary>
        public bool SignIsDetached { get; set; }

        /// <summary>
        /// Тестовая подпись.
        /// </summary>
        public bool DigitalSignatureInTestMode { get; set; }

        /// <summary>
        /// Фильтр ГОСТ.
        /// </summary>
        public string FilterGOSTValue { get; set; }

        /// <summary>
        /// Хендлер для загрузки файлов.
        /// </summary>
        public string FileDownloadHandler { get; set; }

        /// <summary>
        /// Хеш выбранного сертификата.
        /// </summary>
        public string SelectedSertHash { get; set; }

        /// <summary>
        /// Отпечаток сертификата, которым надо подписывать.
        /// </summary>
        public string Thumbprint { get; set; }

        /// <summary>
        /// Данные для подписи.
        /// </summary>
        public string ContentForSign { get; set; }

        /// <summary>
        /// Цифровая подпись.
        /// </summary>
        public string DigitalSign { get; set; }

        /// <summary>
        /// Цифровая подпись документов.
        /// </summary>
        public string FileDigitalSigns { get; set; }

        /// <summary>
        /// Подтверждение добавления подписи.
        /// </summary>
        public string AddSignConfirm { get; set; }

        /// <summary>
        /// Определяет требуется ли фильтровать доступные сертификаты
        /// перед показом их пользователю на предмет их соответствия
        /// стандарту ГОСТ Р 34.10-2001.
        /// </summary>
        public bool IsFilterGOST { get; set; }

        /// <summary>
        /// Исходные данные для подписи.
        /// </summary>
        public string ContentForSignOriginal { get; set; }
    }
}
