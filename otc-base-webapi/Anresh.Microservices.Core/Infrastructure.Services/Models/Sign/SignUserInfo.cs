﻿namespace Anresh.Microservices.Infrastructure.Services.Models.Sign
{
    /// <summary>
    /// Информация о пользователе.
    /// </summary>
    public class SignUserInfo
    {
        /// <summary>
        /// Идентификатор пользователя.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Идентификатор организации.
        /// </summary>
        public int OrganizationId { get; set; }

        /// <summary>
        /// Отпечаток сертификата.
        /// </summary>
        public string Thumbprint { get; set; }
    }
}
